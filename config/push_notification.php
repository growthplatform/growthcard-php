<?php

return [
    'apple' => [
        'sandbox' => [
            'url' => 'ssl://gateway.sandbox.push.apple.com:2195',
            'pem_file' => base_path('apple_pem/sandbox') . '/growthcard.pem',
            'passphrase' => ''
        ], 'production' => [
            'url' => 'ssl://gateway.push.apple.com:2195',
            'pem_file' => base_path('apple_pem/production') . '/growthcard.pem',
            'passphrase' => ''
        ]
    ],
    'gcm' => [
        'default' => [
            'url' => 'https://android.googleapis.com/gcm/send',
            'google_api_key' => 'AIzaSyBpKMuv5tQD6Czz_GqSvRj5SxOYlzNBC6Q',
        ]
    ],
];