<?php

return [
        'OK' => 200,
        'unauthorized' => 401,
        'user_deleted' => 604,
        'login_another_device' => 603,
        'parameter_missing' => 412,
        'not_found' => 404,
        'empty_json'=>601,
        'invalid_json'=>602,
        'internal_server_error'=>500,
        'manager_delete_confirm'=>605,
        'tech_error'=>606,
];
