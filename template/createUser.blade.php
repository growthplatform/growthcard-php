<!-- CREATE USER -->
<div class="modal fade" id="createUserModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add a new user</h4>
      </div>
      <div class="modal-body">
        	<form class="clearfix">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
            	<div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="First name">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Last name">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <input type="Email" class="form-control" placeholder="Email">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <input type="password" class="form-control" placeholder="Password">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                	<input type="text" class="form-control" placeholder="Company name">
                </div>
                <div class="form-group">
                	<select class="selectpicker" data-width="100%">
                        <option>option 1</option>
                        <option>option 2</option>
                        <option>obtion 3</option>
                    </select>
                </div>
                <div class="form-group">
                	<a href="#" class="create-add-link" data-toggle="modal" data-target="#newJobModal"><span class="add-sign">+</span> Create a new Job</a>
                </div>
                <div class="form-group">
                	<input type="text" class="form-control" placeholder="Department">
                </div>
                <div>
                	<a href="#" class="create-add-link"><span class="add-sign">+</span> Add new Department</a>
                </div>
            </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success" disabled>Add User</button>
      </div>
    </div>
  </div>
</div>