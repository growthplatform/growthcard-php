<!-- UPLOAD CSV FILE -->
<div class="modal fade" id="uploadCsvModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        	
            <div id="upload-csv" class="avatar-photo-wrap avatar-drop">
                        <p>
                            Drag & Drop .csv file here </br>or
                            <span class="btn btn-file">Browse Computer
                                <input type="file" class="csv-upload-file">
                            </span>
                        </p>
                    </div>
      </div>
   </div>
  </div>
</div>