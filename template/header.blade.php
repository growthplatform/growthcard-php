<?php /* Header html of page */ ?>
<header class="navbar navbar-default navbar-fixed-top landing-header" id="site-header">
	 <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#site-header-collapse" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar top-bar"></span>
        <span class="icon-bar middle-bar"></span>
        <span class="icon-bar bottom-bar"></span>
      </button>
      <a class="navbar-brand" href="#">
        <img alt="Brand" src="images/logo.png" class="pull-left" width="55" height="47">
      </a>
      <span class="company-name ellipsis">
        Company Name
      </span>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="site-header-collapse">
    	
      <ul class="nav navbar-nav navbar-right">
      	<li class="active"><a href="#">Teams</a></li>
      	<li><a href="#">Users</a></li>
        <li><a href="#">Effective Actions</a></li>
        <li class="dropdown profile">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img src="images/user-img.jpg" class="img-circle" width="36" height="36" alt=""><span>Fox Marks</span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Separated link</a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</header>
