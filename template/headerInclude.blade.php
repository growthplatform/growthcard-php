<?php /* Some common CSS and head section of page */ ?>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="images/icons/favicon.ico">
    <link rel="apple-touch-icon" href="images/icons/favicon.png">
    
    <!--FONTS-->
    <link href='https://fonts.googleapis.com/css?family=Rubik:300,400,500,700,900' rel='stylesheet' type='text/css'>
    <link type='text/css' rel='stylesheet' href='styles/font-awesome.min.css' >
    <link type="text/css" rel="stylesheet" href="styles/jquery-ui.css">
    <link type="text/css" rel="stylesheet" href="styles/bootstrap.css">
    <link type="text/css" rel="stylesheet" href="styles/bootstrap-select.css">
    <link type="text/css" rel="stylesheet" href="styles/jquery.mCustomScrollbar.css">
    <link type="text/css" rel="stylesheet" href="styles/common-layout.css">
    <link type="text/css" rel="stylesheet" href="styles/style.css">
    <link type="text/css" rel="stylesheet" href="styles/style-responsive.css">
    
    												