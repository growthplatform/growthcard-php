-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 29, 2016 at 03:53 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dummy_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `devices`
--

CREATE TABLE IF NOT EXISTS `devices` (
  `dev_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `device_type` varchar(100) CHARACTER SET latin1 NOT NULL,
  `device_id` varchar(255) CHARACTER SET latin1 NOT NULL,
  `user_token` varchar(255) NOT NULL,
  `ip_address` varchar(100) CHARACTER SET latin1 NOT NULL,
  `device_os` varchar(100) CHARACTER SET latin1 NOT NULL,
  `application_version` varchar(100) CHARACTER SET latin1 NOT NULL,
  `device_token` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`dev_id`),
  UNIQUE KEY `device_id` (`device_id`),
  KEY `idx_user` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `devices`
--

INSERT INTO `devices` (`dev_id`, `user_id`, `device_type`, `device_id`, `user_token`, `ip_address`, `device_os`, `application_version`, `device_token`, `created_at`, `updated_at`) VALUES
(5, 2, 'ios', 'ndfbdfg', '', '', '', '', NULL, '2016-01-07 06:24:25', '2016-01-07 06:24:25'),
(7, 13, 'ios', 'ndfbdfgdefe', '', '', '', '', NULL, '2016-01-07 06:24:25', '2016-01-07 06:24:25'),
(10, 1, 'ios', 'teyrbkhrwtyurtdfwdf', 'd055bf311fdb99765d0d8288dc9761fc', '', '', '', 'dgefydtefsdsddsds', '2016-01-29 04:45:47', '2016-01-29 04:45:47');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'param', '', 'paramgir@gmail.com', '$2y$10$SNa7BfIXnd2v6JuyUVUTJe9YkhvQakJyJOwxWkCJC/lZ3gxFyhQHK', '8sH9MUclX2lfMlsP5SPUcbWbIOMNp449giczTJUGwWZ9AIJNZD4ygVWKNr6y', '2016-01-28 10:10:12', '2016-01-29 02:14:53', NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
