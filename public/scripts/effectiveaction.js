//EFFECTIVE-ACTION-PAGE-JS
$(document).ready(function () {
    $('.editTheJob').click(function () {
        $('.jobName_error').html('');
    });
    $('.createNewJob').click(function () {
        $('.jobName_error').html('');
        $('#jobName').val('');
    });
    $(document).on('focus', ':input', function () {
        $(this).attr('autocomplete', 'off');
    });
});

$('#newJobModal').on('show.bs.modal', function () {
    $('#addEffectiveModal').css('z-index', 1039);
});

$('#newJobModal').on('hidden.bs.modal', function () {
    $('#addEffectiveModal').css('z-index', 1041);
});

$('#newJobModal').on('show.bs.modal', function () {
    $('#createEffectiveModal').css('z-index', 1039);
});

$('#newJobModal').on('hidden.bs.modal', function () {
    $('#createEffectiveModal').css('z-index', 1041);
});

$('#newJobModal').on('show.bs.modal', function () {
    $('#editEffectiveModal').css('z-index', 1039);
});

$('#newJobModal').on('hidden.bs.modal', function () {
    $('#editEffectiveModal').css('z-index', 1041);
});

//GET-USER-DATA
function get_user_data(job) {
    var jobId = "#" + job;
    $.ajax({
        url: 'designation',
        method: 'GET',
        dataType: 'json',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            if (response.statusCode == 200) {
                var html = '';
                $.each(response.result, function (key, val) {
                    html += "<option value ='" + val.designationId + "'>" + val.designationTitle + "</option>";
                });
                $(jobId).html(html).selectpicker('refresh');
            } else {
                alert("There might be some error occured.");
            }
        }
    });
}

//DELETE-EFFECTIVE-ACTION
function deletes(id) {
    var r = confirm('Do you want to Delete ?');
    if (r == true) {
        $.ajax({
            type: "POST",
            url: 'delete-effective-action',
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {id: id},
            success: function (response) {
                if (response.statusCode == 200) {
                    alert('Effective action successfully deleted.');
                    location.reload();
                } else {
                    alert("There might be some error occured.");
                }
            },
            error: function (response) {
                var error = jQuery.parseJSON(response.responseText);
                if (error.statusCode == 412) {
                    alert(error.message);
                } else {
                    alert("There might be some error occured.");
                }
            },
        });
    }
}

function get_edit_effective_action(jobid, id, effectiveactionname, jobname)
{
    $("#editEffectiveModal").modal();
    $("#editEffectiveModal input[name=effectiveAction]").val(effectiveactionname);
    $("#editEffectiveModal input[name=Jobtitle]").val(jobname);
    $("#editEffectiveModal input[name=id]").val(id);
    $("#editEffectiveModal input[name=jobid]").val(jobid);
}

function get_add_effective_action(id, jobname)
{
    $("#addEffectiveModal").modal();
    $("#addEffectiveModal input[name=Jobtitle]").val(id);
    $("#addEffectiveModal input[name=JobName]").val(jobname);
}

function get_edit_job(id, jobname)
{
    $("#editJobModal").modal();
    $("#editJobModal input[name=id]").val(id);
    $("#editJobModal input[name=jobName]").val(jobname);

}

function openJobPopup(flag) {
    $("#flag").val(flag);
    $("#newJobModal").modal('show');
}