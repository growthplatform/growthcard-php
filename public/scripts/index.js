$(function () {
    $(document).on('focus', ':input', function () {
        $(this).attr('autocomplete', 'off');
    });
    //on the time of adding manager in mobile view
    $('#mobilemanager').click(function () {
        $('#assignUserModal').attr('sourcebutton', 'selectmanager');
        var teamcounter = $(this).attr('teamcounter');
        var previousteamcounter = $(this).attr('previousteamcounter');
        $('#assignUserModal').attr('teamcounter', teamcounter);
        $('#assignUserModal').attr('previousteamcounter', previousteamcounter);
        $('#usertypelabel').attr('rel', '1');             //type of user
        $('#usertypelabel').html('Assign Manager');       //user type lable
        var previousmanagerid = $(this).attr('areamanager');
        $('#innermobileassignUser').attr('areamanager', previousmanagerid);
        $('#innermobileassignUser').attr('savedmanagerid', $(this).attr('savedmanagerid'));
    });

    //assign user in mobile view
    $('body').on('click', '.mobileassignUser', function () {
        $('#assignUserModal').attr('sourcebutton', 'assignuser');
        var teamcounter = $(this).attr('teamcounter');
        var previousteamcounter = $(this).attr('previousteamcounter');
        $('#assignUserModal').attr('teamcounter', teamcounter);
        $('#assignUserModal').attr('previousteamcounter', previousteamcounter);
        $('.mobileusercb').prop('checked', false); // unchecks it
        var managerid = $(this).attr('rel');
        $('#usertypelabel').attr('rel', '2');
        $('#usertypelabel').attr('managerid', managerid);
        $('#usertypelabel').html('Assign Subordinate');
        $("body").find(".mobile-assign-user").attr("disabled", true);
        $("body").find("#actiondropdown_" + managerid + '_' + teamcounter).removeAttr("disabled");
    });

    //select user in mobile view popup
    $('.mobileusercb').click(function () {
        var usertype = parseInt($('#usertypelabel').attr('rel'));
        if (usertype === 1) {
            var teammanagerid = parseInt($(this).attr('rel'));
            var newManager = true;
            $('.teamMainDiv_class').each(function () {
                var newteammanagerid = parseInt($(this).attr('teammanagerid'));
                if (newteammanagerid === teammanagerid) {
                }
            });
            var teanmanagername = $('#user-name_' + teammanagerid).text();
            if (newManager === false) {
            } else {
                if (!$(this).is(':checked')) {
                    $(this).prop('checked', false); // Checks it
                } else {
                    $('.mobileusercb').prop('checked', false); // Checks it
                    $(this).prop('checked', true); // Checks it
                }
            }
        } else {
            var teammanagerid = parseInt($('#usertypelabel').attr('managerid'));
            var newsubordinateid = parseInt($(this).attr('rel'));
            var teanmanagername = $('#user-name_' + teammanagerid).text();
            var managerofthisteam = false;
            if (teammanagerid === newsubordinateid) {
                $(this).prop('checked', false); // Checks it
                alert('You have already already selected ' + teanmanagername + ' as a manager for this team');
                managerofthisteam = true;
            } else {
                var newSubordinate = true;
                $('.subordinate_class').each(function () {
                    var subordinateid = parseInt($(this).attr('rel'));
                    if (newsubordinateid === subordinateid) {
                        newSubordinate = false;
                    }
                });
                if (newSubordinate === false) {
                    var newsubordinatename = $('#user-name_' + newsubordinateid).text();
                    if (managerofthisteam === true) {
                        $(this).prop('checked', false); // Checks it
                        alert('You have already selected ' + newsubordinatename + ' as a manager for this team');
                    } else {
                        $(this).prop('checked', false); // Checks it
                        alert('You have already assigned ' + newsubordinatename + ' as a subordinate. Pleae assign someone other');
                    }
                } else {
                    if (!$(this).is(':checked')) {
                        $(this).prop('checked', false); // Checks it
                    } else {
                        $(this).prop('checked', true); // Checks it
                    }
                }
            }
        }
    });

    //"Assign user in mobile view popup"
    $('#innermobileassignUser').click(function () {
        var objid = $(this).attr('areamanager');
        disableCreateNewTeam(objid);
        var checkedValuesArr = $('.mobileusercb:checked').map(function () {
            return $.trim($(this).attr('rel'));
        }).get();

        var usertype = parseInt($('#usertypelabel').attr('rel'));
        var teamcounter = $('#assignUserModal').attr('teamcounter');

        if (usertype === 1) {     //for manager
            if (checkedValuesArr.length > 0) {
                for (var j in checkedValuesArr) {
                    var teammanagerid = checkedValuesArr[j];
                    var newManager = true;
                    $('.teamMainDiv_class').each(function () {
                        var newteammanagerid = parseInt($(this).attr('teammanagerid'));
                        if (newteammanagerid === teammanagerid) {
                            newManager = false;
                        }
                    });

                    $('#deleteteam_' + teammanagerid + '_' + teamcounter).hide();
                    $('#changemanager_' + teammanagerid + '_' + teamcounter).hide();
                    var profile_image = $('#profile_image_' + teammanagerid).attr('src');
                    var name = $('#user-name_' + teammanagerid).text();
                    var designation = $('#user-profile_' + teammanagerid).text();
                    var department = $('#user-department_' + teammanagerid).text();
                    var previousmanagerid = parseInt($(this).attr('areamanager'));
                    var savedmanagerid = parseInt($(this).attr('savedmanagerid'));
                    if (teamcounter == '0') {
                        var tmpteamcounter = $('body').find(".team-managerclass_" + teammanagerid).length + 1;
                        teamcounter = 'teamtmp' + tmpteamcounter;
                    }
                    var teamManager = getTeamManager(teammanagerid, profile_image, name, designation, savedmanagerid, teamcounter);

                    var previussubordinateids = '';
                    var previussubordinateidlength = 0;
                    var newchildcontent = '';
                    var oldteamid = 0;
                    var managernotreplaceable = true;
                    if (previousmanagerid > 0) {
                        changeteamstatus = "0"
                        previussubordinateids = $.trim($('#myteamslist_' + previousmanagerid + '_' + teamcounter).attr('rel'));
                        if (previussubordinateids != '') {
                            var previussubordinateidsarr = previussubordinateids.split(',');
                            previussubordinateidlength = previussubordinateidsarr.length;
                            for (var l in previussubordinateidsarr) {
                                if (parseInt(teammanagerid) === parseInt(previussubordinateidsarr[l])) {
                                    managernotreplaceable = false;
                                    break;
                                }
                            }
                            if (managernotreplaceable === true) {
                                if (previussubordinateidlength > 0) {
                                    for (var k in previussubordinateidsarr) {
                                        var name = $.trim($('#name_' + previussubordinateidsarr[k]).text());
                                        var email = $.trim($('#email_' + previussubordinateidsarr[k]).text());
                                        var disignation_department = $.trim($('#disignation_department_' + previussubordinateidsarr[k]).text());
                                        var disignation_department_arr = disignation_department.split(',');
                                        var disignation = $.trim(disignation_department_arr[0]);
                                        var department = $.trim(disignation_department_arr[1]);
                                        var existinguserstatus = $.trim($('#deletelink_' + previussubordinateidsarr[k]).attr('existinguserstatus'));
                                        var profile_image = $.trim($('#profile_image_' + previussubordinateidsarr[k]).attr('src'));
                                        newchildcontent += getchildcontent(previussubordinateidsarr[k], teammanagerid, name, email, disignation, department, profile_image, existinguserstatus, teamcounter);
                                    }
                                }
                            }
                        }

                        if (managernotreplaceable === true) {
                            oldteamid = parseInt($('#doneTeam_' + previousmanagerid + '_' + teamcounter).attr('rel2'));
                            $('#teams-list-wrapper_' + previousmanagerid + '_' + teamcounter).remove();
                        }
                    }
                }

                if (managernotreplaceable === true) {
                    $('.noteamdiv').remove();
                    $(this).parents("body").find("#team-list-ctn").prepend('<div id="teams-list-wrapper_' + teammanagerid + '_' + teamcounter + '" class="teams-list-wrapper">' + teamManager + '</div>');


                    $('#team-creation-ctn').attr('rel', teammanagerid);
                    $('#changemanager_' + teammanagerid + '_' + teamcounter).attr('rel', 1);
                    if (oldteamid > 0) {
                        $('#doneTeam_' + teammanagerid + '_' + teamcounter).attr('rel2', oldteamid);
                        $('#doneTeam_' + teammanagerid + '_' + teamcounter).attr('oldmanagerid', previousmanagerid);
                    }
                    $(this).attr('areamanager', teammanagerid);
                }
                $('#mobilecreateTeam').attr('tempteamcount', teammanagerid);
                if (managernotreplaceable === true) {
                    $('#myteamslist_' + teammanagerid + '_' + teamcounter).attr('rel', previussubordinateids);
                    if (previussubordinateidlength > 0) {
                        $('#myteamslist_' + teammanagerid + '_' + teamcounter).after('<ul class="item-list-ctn ui-helper-reset clearfix" id="managersubordinate_' + teammanagerid + '_' + teamcounter + '">' + newchildcontent + '</ul>');
                    }
                    $('#doneTeam_' + teammanagerid + '_' + teamcounter).show();
                    $('#assignUserModal').modal('hide');
                    $('#team-creation-ctn').css('display', 'none');
                } else {
                    alert('You have already assigned this user as subordinate of this team');
                }

            } else {
                alert('Please select manager');
            }
        } else {
            if (checkedValuesArr.length > 0) {  //for subordinate
                var teammanagerid = $('#usertypelabel').attr('managerid');
                if (teamcounter == '0') {
                    var tmpteamcounter = $('body').find(".team-managerclass_" + teammanagerid).length + 1;
                    teamcounter = 'teamtmp' + tmpteamcounter;
                }
                var fieldlength = $('#managersubordinate_' + teammanagerid + '_' + teamcounter).length;
                var childcontent = '';
                var exitingsubordinateids = $.trim($('#myteamslist_' + teammanagerid + '_' + teamcounter).attr('rel'));
                var exitingsubordinateidsarr = [];
                if (exitingsubordinateids != '') {
                    exitingsubordinateidsarr = exitingsubordinateids.split(',');
                }
                for (var j in checkedValuesArr) {
                    var subordinateid = checkedValuesArr[j];
                    var name = $('#user-name_' + subordinateid).text();
                    var email = $('#user-email_' + subordinateid).text();
                    var designation = $('#user-profile_' + subordinateid).text();
                    var department = $('#user-department_' + subordinateid).text();
                    var profile_image = $('#profile_image_' + subordinateid).attr('src');
                    var existinguserstatus = 0;
                    childcontent += getchildcontent(subordinateid, teammanagerid, name, email, designation, department, profile_image, existinguserstatus, teamcounter);
                    exitingsubordinateidsarr.push(subordinateid);
                }

                if (fieldlength > 0) {
                    $('#managersubordinate_' + teammanagerid + '_' + teamcounter).prepend(childcontent);
                } else {
                    $('#myteamslist_' + teammanagerid + '_' + teamcounter).after('<ul class="item-list-ctn ui-helper-reset clearfix" id="managersubordinate_' + teammanagerid + '_' + teamcounter + '">' + childcontent + '</ul>');
                }
                $('#myteamslist_' + teammanagerid + '_' + teamcounter).attr('rel', exitingsubordinateidsarr.toString());
                $('#innermobilecanceluser').click();
                $('#changemanager_' + teammanagerid + '_' + teamcounter).hide();
                $('#deleteteam_' + teammanagerid + '_' + teamcounter).hide();
                $('#doneTeam_' + teammanagerid + '_' + teamcounter).show();
            } else {
                alert('Please select subordinate');
            }
        }
        $('#usertypelabel').attr('managerid', 0);
    });

    $('#createTeam, #uploadCsv').click(function () {
        $('#team-drop-area').attr('teamcounter', '0');
        $('#team-drop-area').attr('previousteamcounter', '0');
        $('#mobilemanager').attr('teamcounter', '0');
        $('#mobilemanager').attr('previousteamcounter', '0');
        var relVal = parseInt($('#createTeam').attr('rel'));

        if (relVal === 0) {
            $('#pageMessageDiv').html('Please complete team first by clicking done button').removeClass().addClass("selected-file error");
            $('.teamMainDiv_Message').html('');
        }

    });

    //popups
    $("body").on("click", 'a[data-action]', function (event) {
        event.preventDefault();
        var showDiv = $(this).attr("data-action");
        $(showDiv).show();

    });

    //for dragging
    $(".sidebar-ul .sidebar-item").draggable({
        revert: "invalid",
        containment: "document",
        helper: "clone",
        cursor: "move",
        appendTo: 'body',
        zIndex: 5,
    });

    //dragging manager
    $("#team-drop-area").droppable({
        tolerance: "fit",
        accept: ".sidebar-item",
        activeClass: "ui-state-hover",
        hoverClass: "ui-state-active",
        drop: function (event, ui) {
            $('.teamMainDiv_Message').html('');

            var tag = ui.draggable;
            var teammanagerid = parseInt(tag.find(".user-name").attr('rel'));
            disableCreateNewTeam(teammanagerid);
            var newManager = true;
            

            var teanmanagername = tag.find(".user-name").text();
            if (newManager !== false) {
                var previousmanagerid = parseInt($(this).attr('areamanager'));
                var teamcounter = $(this).attr('teamcounter');

                if (teamcounter == '0') {
                    var tmpteamcounter = $('body').find(".team-managerclass_" + teammanagerid).length + 1;
                    teamcounter = 'teamtmp' + tmpteamcounter;
                }

                var savedmanagerid = parseInt($(this).attr('savedmanagerid'));

                var teamManager = getTeamManager(teammanagerid, tag.find(".profile_image").attr('src'), tag.find(".user-name").text(), tag.find(".user-profile").text(), savedmanagerid, teamcounter);
                $(this).addClass("ui-state-highlight");
                $(this).parents("#team-creation-ctn").hide();
                var previussubordinateids = '';
                var previussubordinateidlength = 0;
                var newchildcontent = '';
                var oldteamid = 0;
                var managernotreplaceable = true;
                if (previousmanagerid > 0) {
                    changeteamstatus = "0"
                    previussubordinateids = $.trim($('#myteamslist_' + previousmanagerid + '_' + teamcounter).attr('rel'));
                    if (previussubordinateids != '') {
                        var previussubordinateidsarr = previussubordinateids.split(',');
                        previussubordinateidlength = previussubordinateidsarr.length;
                        for (var l in previussubordinateidsarr) {
                            if (parseInt(teammanagerid) === parseInt(previussubordinateidsarr[l])) {
                                managernotreplaceable = false;
                                break;
                            }
                        }

                        if (managernotreplaceable === true) {
                            if (previussubordinateidlength > 0) {
                                for (var k in previussubordinateidsarr) {
                                    var name = $.trim($('#name_' + previussubordinateidsarr[k]).text());
                                    var email = $.trim($('#email_' + previussubordinateidsarr[k]).text());
                                    var disignation_department = $.trim($('#disignation_department_' + previussubordinateidsarr[k]).text());
                                    var disignation_department_arr = disignation_department.split(',');
                                    var disignation = $.trim(disignation_department_arr[0]);
                                    var department = $.trim(disignation_department_arr[1]);
                                    var existinguserstatus = $.trim($('#deletelink_' + previussubordinateidsarr[k]).attr('existinguserstatus'));
                                    var profile_image = $.trim($('#profile_image_' + previussubordinateidsarr[k]).attr('src'));
                                    newchildcontent += getchildcontent(previussubordinateidsarr[k], teammanagerid, name, email, disignation, department, profile_image, existinguserstatus, teamcounter);
                                }
                            }
                        }
                    }
                    if (managernotreplaceable === true) {
                        var previousteamcounter = $(this).attr('previousteamcounter');
                        oldteamid = parseInt($('#doneTeam_' + previousmanagerid + '_' + teamcounter).attr('rel2'));
                        $('#teams-list-wrapper_' + previousmanagerid + '_' + previousteamcounter).remove();
                    }
                }

                if (managernotreplaceable === true) {
                    $('.noteamdiv').remove();

                    $(this).parents("body").find("#team-list-ctn").prepend('<div id="teams-list-wrapper_' + teammanagerid + '_' + teamcounter + '" class="teams-list-wrapper">' + teamManager + '</div>');


                    $("button#deleteteam_" + teammanagerid + '_' + teamcounter).hide();
                    $('#team-creation-ctn').attr('rel', teammanagerid);

                    if (oldteamid > 0) {
                        $('#changemanager_' + teammanagerid + '_' + teamcounter).attr('rel', 1);
                        $('#doneTeam_' + teammanagerid + '_' + teamcounter).attr('rel2', oldteamid);
                        $('#doneTeam_' + teammanagerid + '_' + teamcounter).attr('oldmanagerid', previousmanagerid);
                    }
                }
                //for subordinae of particular manager
                $("#user-drop-area_" + teammanagerid + "_" + teamcounter).droppable({
                    tolerance: "fit",
                    accept: function (draggable) {
                        return $(this).find("#subordinate_" + draggable.attr("id")).length == 0;
                        $(this).find("#subordinate_" + draggable.attr("id")).length == 0;

                    },
                    activeClass: "ui-state-hover",
                    hoverClass: "ui-state-active",
                    drop: function (event, ui) {
                        var $list = $("ul.item-list-ctn", "#user-drop-area_" + teammanagerid + "_" + teamcounter).length ?
                                $("ul.item-list-ctn", "#user-drop-area_" + teammanagerid + "_" + teamcounter) :
                                $("<ul id='tmp_managersubordinate_" + teammanagerid + "_" + teamcounter + "' class='item-list-ctn ui-helper-reset clearfix'/>").appendTo("#user-drop-area_" + teammanagerid + "_" + teamcounter);
                        var tag = ui.draggable;
                        var newsubordinateid = parseInt(tag.attr("rel"));
                        var managerofthisteam = false;
                        if (teammanagerid === newsubordinateid) {
                            alert('You have already already selected ' + teanmanagername + ' as a manager for this team');
                            managerofthisteam = true;
                        } else {
                            var newSubordinate = true;
                            $('.subordinate_class').each(function () {
                                var subordinateid = parseInt($(this).attr('rel'));
                                if (newsubordinateid === subordinateid) {
                                    newSubordinate = false;
                                }
                            });
                            if (newSubordinate === false) {
                                var newsubordinatename = tag.find(".user-name").text();
                                if (managerofthisteam === true) {
                                    alert('You have already selected ' + newsubordinatename + ' as a manager for this team');
                                } else {
                                    alert('You have already assigned ' + newsubordinatename + ' as a subordinate. Pleae assign someone other');
                                }
                            } else {
                                var existinguserstatus = 0;
                                var dropItem = getchildcontent(tag.attr("rel"), teammanagerid, tag.find(".user-name").text(), tag.find(".user-email").text(), tag.find(".user-profile").text(), tag.find(".user-department").text(), tag.find(".profile_image").attr('src'), existinguserstatus, teamcounter);
                                $('#teammessage_' + teammanagerid + '_' + teamcounter).html('');
                                $list.prepend(dropItem);
                                var teamlist = $.trim($('#myteamslist_' + teammanagerid + '_' + teamcounter).attr('rel'));
                                if (teamlist != '') {
                                    var teamlistarr = teamlist.split(',');
                                    teamlistarr.push(tag.attr("rel"));
                                    $('#myteamslist_' + teammanagerid + '_' + teamcounter).attr('rel', teamlistarr.toString());
                                } else {
                                    $('#myteamslist_' + teammanagerid + '_' + teamcounter).attr('rel', tag.attr("rel"));
                                }
                            }
                        }
                    }
                });

                $('#createTeam').attr('tempteamcount', teammanagerid);
                $('#mobilecreateTeam').attr('tempteamcount', teammanagerid);

                if (managernotreplaceable === true) {
                    $('#myteamslist_' + teammanagerid + '_' + teamcounter).attr('rel', previussubordinateidsarr);
                    if (previussubordinateidlength > 0) {
                        $('#myteamslist_' + teammanagerid + '_' + teamcounter).after('<ul class="item-list-ctn ui-helper-reset clearfix" id="managersubordinate_' + teammanagerid + '_' + teamcounter + '">' + newchildcontent + '</ul>');
                    }
                } else {
                    alert('You have already assigned this user as subordinate of this team');
                }
            }
        }
    })

    //cancel of add manager div
    $('#cancelteamcreationdiv').click(function () {
        $('#team-drop-area').attr('areamanager', 0);
        $('#team-creation-ctn').css('display', 'none');
        $("body").find(".mobile-assign-user").removeAttr("disabled");
        $("body").find(".assign-user").removeAttr("disabled");
    });

    //mobile view cancel or cross button
    $('#innermobilecanceluser, #crossclose').click(function () {
        var teammanagerid = $(this).attr('areamanager');
        $('#usertypelabel').attr('managerid', 0);
        $('.mobileusercb').prop('checked', false);
        $('#innermobileassignUser').attr('areamanager', 0);
        $('#innermobilecanceluser').attr('areamanager', 0);
        var teamcreatestatus = parseInt($('#mobilecreateTeam').attr('rel'));
        if (teamcreatestatus === 1) {
            $('#deleteteam_' + teammanagerid).show();
        }
        $('#changemanager_' + teammanagerid).show();
        var sourcebutton = $('#assignUserModal').attr('sourcebutton');
        if (sourcebutton == 'assignuser') {
            $("body").find(".mobile-assign-user").removeAttr("disabled");
        }

    });

    //add user in existing team
    $(".existinguser-drop-area").droppable({
        tolerance: "fit",
        accept: function (draggable) {
            return $(this).find("#subordinate_" + draggable.attr("id")).length == 0;
            $(this).find("#subordinate_" + draggable.attr("id")).length == 0;

        },
        activeClass: "ui-state-hover",
        hoverClass: "ui-state-active",
        drop: function (event, ui) {
            var teammanagerid = parseInt($(this).attr('rel'));
            var teamcounter = parseInt($(this).attr('rel2'));
            var teanmanagername = $('#user-name_' + teammanagerid).html();
            var $list = $("ul.item-list-ctn", "#user-drop-area_" + teammanagerid + '_' + teamcounter).length ?
                    $("ul.item-list-ctn", "#user-drop-area_" + teammanagerid + '_' + teamcounter) :
                    $("<ul id='tmp_managersubordinate_" + teammanagerid + "_" + teamcounter + "' class='item-list-ctn ui-helper-reset clearfix'/>").appendTo("#user-drop-area_" + teammanagerid + '_' + teamcounter);
            var tag = ui.draggable;

            var newsubordinateid = parseInt(tag.attr("rel"));
            var managerofthisteam = false;
            if (teammanagerid === newsubordinateid) {
                alert('You have already selected ' + teanmanagername + ' as a manager for this team');
                managerofthisteam = true;
            } else {
                var newSubordinate = true;
                $('.subordinate_class').each(function () {
                    var subordinateid = parseInt($(this).attr('rel'));
                    if (newsubordinateid === subordinateid) {
                        newSubordinate = false;
                    }
                });
                if (newSubordinate === false) {
                    var newsubordinatename = tag.find(".user-name").text();
                    if (managerofthisteam === true) {
                        alert('You have already selected ' + teanmanagername + ' as a manager for this team');
                    } else {
                        alert('You have already assigned ' + newsubordinatename + ' as a subordinate. Pleae assign someone other');
                    }

                } else {
                    var existinguserstatus = 0;
                    var dropItem = getchildcontent(tag.attr("rel"), teammanagerid, tag.find(".user-name").text(), tag.find(".user-email").text(), tag.find(".user-profile").text(), tag.find(".user-department").text(), tag.find(".profile_image").attr('src'), existinguserstatus, teamcounter);
                    $('#teammessage_' + teammanagerid + '_' + teamcounter).html('');
                    $list.prepend(dropItem);

                    var teamlist = $.trim($('#myteamslist_' + teammanagerid + '_' + teamcounter).attr('rel'));
                    if (teamlist != '') {
                        var teamlistarr = teamlist.split(',');
                        teamlistarr.push(tag.attr("rel"));
                        $('#myteamslist_' + teammanagerid + '_' + teamcounter).attr('rel', teamlistarr.toString());
                    } else {
                        $('#myteamslist_' + teammanagerid + '_' + teamcounter).attr('rel', tag.attr("rel"));

                    }
                }
            }
        }
    })

    //Assign user in web view
    $("#team-list-ctn").on("click", "button.assign-user", function () {
        var relVal = $(this).attr('rel');
        var teamcounter = $(this).attr('teamcounter');
        var $this = $(this);
        $("#myteamschildlist_" + relVal + '_' + teamcounter).show();
        $this.hide();
        $('#doneTeam_' + relVal + '_' + teamcounter).show();
        $("body").find(".assign-user").attr("disabled", true);
    });

    //done team
    $("#team-list-ctn").on("click", "button.done-user", function () {
        var newteam = parseInt($(this).attr('newteam'));
        var relVal = $(this).attr('rel');
        var teamcounter = $(this).attr('teamcounter');
        var elelength = parseInt($('#tmp_managersubordinate_' + relVal + '_' + teamcounter).length);
        if (elelength > 0) {
            var childcontent = $('#tmp_managersubordinate_' + relVal + '_' + teamcounter).html();
            $('#tmp_managersubordinate_' + relVal + '_' + teamcounter).remove();
            if (newteam === 0) {
                if ($('#managersubordinate_' + relVal + '_' + teamcounter).length === 0) {
                    var tmpHtml = '<ul class="item-list-ctn ui-helper-reset clearfix" id="managersubordinate_' + relVal + '_' + teamcounter + '">' + childcontent + '</ul>';
                    $('#teamMainDiv_' + relVal + '_' + teamcounter).append(tmpHtml);
                } else {
                    $('#managersubordinate_' + relVal + '_' + teamcounter).prepend(childcontent);
                }
            } else {
                var tmpHtml = '<ul class="item-list-ctn ui-helper-reset clearfix" id="managersubordinate_' + relVal + '_' + teamcounter + '">' + childcontent + '</ul>';
                $('#teamMainDiv_' + relVal + '_' + teamcounter).append(tmpHtml);
            }
        }
        var $this = $(this);
        //$this.parents(".teams-list-wrapper").find(".user-creation-ctn").hide();
        $('#myteamschildlist_' + relVal + '_' + teamcounter).hide();
        $this.hide();
        $('#assignUser_' + relVal + '_' + teamcounter).show();
    });

});

//delete user
function deleteuser(userid, teammanagerid, role, teamcounter) {
    var saveduserstatus = parseInt($('#deletelink_' + userid).attr('existinguserstatus'));
    if (saveduserstatus > 0) {
        var deleteuserstatus = confirm("Do you want to delete the user from this team?");
        if (deleteuserstatus === true) {
            $.ajax({
                url: 'delete-subordinate',
                method: 'POST',
                data: {'userid': userid, 'role': role},
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                beforeSend: function () {
                    $('#deletelink_' + userid).removeAttr('onclick');
                    $('#teamMainDiv_Message_' + teammanagerid + '_' + teamcounter).html('processing...').removeClass().addClass("error");
                    ;
                },
                success: function (result) {
                    $('#pageMessageDiv').html('');
                    var deletestatus = parseInt(result);
                    if (deletestatus === 1) {
                        $('#subordinate_' + userid).remove();

                        updateteamlist(userid, teammanagerid, teamcounter);

                        if ($('.subordinate_class_' + teammanagerid + '_' + teamcounter).length === 0) {
                            $('#managersubordinate_' + teammanagerid + '_' + teamcounter).remove();
                        } else {
                        }
                        $('#teamMainDiv_Message_' + teammanagerid + '_' + teamcounter).removeClass().addClass("success teamMainDiv_Message").html('User has been deleted successfully');
                    } else {
                        $('#teamMainDiv_Message_' + teammanagerid + '_' + teamcounter).html('There is some problem while deleting this user. Please try again after some time.');
                    }
                }
            });
        }
    } else {
        //var removeuserstatus = confirm("Do you want to remove the user from the list of user of this team?");
        //if(removeuserstatus === true){
        updateteamlist(userid, teammanagerid, teamcounter);
        $('#subordinate_' + userid).remove();
        $('#teammessage_' + teammanagerid + '_' + teamcounter).html('User has been removed from the list successfully');
        //}
    }
}

//disable to create new team if other team is in creation/updation state
function disableCreateNewTeam(objid) {
    $('#createTeam').removeAttr("data-action");
    $('#mobilecreateTeam').removeAttr("data-action");
    $('#uploadCsv').removeAttr("data-target");
    $('#createTeam').attr("rel", '0');
    $('#mobilecreateTeam').attr("rel", '0');
    $('#innermobileassignUser').attr('areamanager', objid);
    $('#innermobilecanceluser').attr('areamanager', objid);
    $('#crossclose').attr('areamanager', objid);
}

//enable new team after done
function enableCreateNewTeam(teammanagerid, teamcounter) {
    $('.teamMainDiv_Message').html('');
    var oldmanagerid = $('#doneTeam_' + teammanagerid + '_' + teamcounter).attr('oldmanagerid');
    var savedmanagerid = $('#doneTeam_' + teammanagerid + '_' + teamcounter).attr('savedmanagerid');
    $('#createTeam').attr("data-action", "#team-creation-ctn");
    $('#mobilecreateTeam').attr("data-action", "#team-creation-ctn");
    $('#uploadCsv').attr("data-target", "#uploadCsvModal");
    $('#createTeam').attr("rel", '1');
    $('#mobilecreateTeam').attr("rel", '1');
    var teamsubordinateid = $("#myteamslist_" + teammanagerid + '_' + teamcounter).attr('rel');
    var teamid = parseInt($('#doneTeam_' + teammanagerid + '_' + teamcounter).attr('rel2'));

    $.ajax({
        url: 'create-team',
        method: 'POST',
        data: {'teammanagerid': teammanagerid, 'teamsubordinateid': teamsubordinateid, 'teamid': teamid, 'oldmanagerid': savedmanagerid},
        dataType: 'json',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function () {
            $('#teamMainDiv_Message_' + teammanagerid + '_' + teamcounter).html('processing...').removeClass().addClass("error");
        },
        success: function (result) {
            $('#pageMessageDiv').html('');
            $('#team-drop-area').attr('areamanager', 0);

            $('a#deleteteam_' + teammanagerid + '_' + teamcounter).show();
            $('button#deleteteam_' + teammanagerid + '_' + teamcounter).show();

            $('a#changemanager_' + teammanagerid + '_' + teamcounter).attr('rel', 2);
            $('button#changemanager_' + teammanagerid + '_' + teamcounter).attr('rel', 2);

            $('a#changemanager_' + teammanagerid + '_' + teamcounter).attr('teamid', result);
            $('button#changemanager_' + teammanagerid + '_' + teamcounter).attr('teamid', result);

            $('a#doneTeam_' + teammanagerid + '_' + teamcounter).attr('rel2', result);
            $('button#doneTeam_' + teammanagerid + '_' + teamcounter).attr('rel2', result);

            $('a#changemanager_' + teammanagerid + '_' + teamcounter).show();
            $('button#changemanager_' + teammanagerid + '_' + teamcounter).show();

            $('a#deleteteam_' + teammanagerid + '_' + teamcounter).show();
            $('button#deleteteam_' + teammanagerid + '_' + teamcounter).show();

            $('a#deleteteam_' + teammanagerid + '_' + teamcounter).attr('teamid', result);
            $('button#deleteteam_' + teammanagerid + '_' + teamcounter).attr('teamid', result);
            var subordinateArr = teamsubordinateid.split(',');
            for (len = 0; len < subordinateArr.length; len++) {
                $('#deletelink_' + subordinateArr[len]).attr('existinguserstatus', 1);
            }

            var teamcreatestatus = parseInt(result);
            if (teamcreatestatus > 0) {
                $('.noteamdiv').remove();
                if (teamid > 0) {
                    $('#teamMainDiv_Message_' + teammanagerid + '_' + teamcounter).removeClass().addClass("success teamMainDiv_Message").html('Team updated successfully');
                } else {
                    $('#teamMainDiv_Message_' + teammanagerid + '_' + teamcounter).removeClass().addClass("success teamMainDiv_Message").html('Team created successfully');
                }
                $('#doneTeam_' + teammanagerid + '_' + teamcounter).attr('oldmanagerid', 0);
                $('#doneTeam_' + teammanagerid + '_' + teamcounter).attr('savedmanagerid', teammanagerid);
                $('button#changemanager_' + teammanagerid + '_' + teamcounter).attr('savedmanagerid', teammanagerid);

            } else {
                $('#teamMainDiv_Message_' + teammanagerid + '_' + teamcounter).removeClass().addClass("selected-file error teamMainDiv_Message").html('There is some problem while creating team. Please try again after some time');
            }
            //Write your code here
            $('#teammessage_' + teammanagerid + '_' + teamcounter).html('Drag &amp; Drop a User from left Sidebar to Assign');
            $("body").find(".mobile-assign-user").removeAttr("disabled");
            $("body").find(".assign-user").removeAttr("disabled");
        }
    });
}

//change manager
function changemanager(teammanagerid, teamid, savedmanagerid, teamcounter) {
    //alert('teamcounter:'+teamcounter); return false;
    $('.mobileusercb').prop('checked', false); // unchecks it
    $("body").find(".mobile-assign-user").attr("disabled", true);
    $("body").find("#actiondropdown_" + teammanagerid + '_' + teamcounter).removeAttr("disabled");

    $("body").find(".assign-user").attr("disabled", true);
    $("body").find("#assignUser_" + teammanagerid + '_' + teamcounter).removeAttr("disabled");

    $('#mobilemanager').text('Select Manager');
    $('#team-drop-area').attr('areamanager', teammanagerid);
    $('#team-drop-area').attr('savedmanagerid', savedmanagerid);
    $('#team-drop-area').attr('teamcounter', teamcounter);
    $('#team-drop-area').attr('previousteamcounter', teamcounter);

    $('#mobilemanager').attr('areamanager', teammanagerid);
    $('#mobilemanager').attr('savedmanagerid', savedmanagerid);
    $('#mobilemanager').attr('teamcounter', teamcounter);
    $('#mobilemanager').attr('previousteamcounter', teamcounter);

    var teamcreatestatus = parseInt($('#changemanager_' + teammanagerid + '_' + teamcounter).attr('rel'));
    if (teamcreatestatus === 0) {
        $('#team-creation-ctn').css('display', 'block');
    } else if (teamcreatestatus === 1) {
        $('#team-creation-ctn').css('display', 'block');
    } else if (teamcreatestatus === 2) {
        $('#team-creation-ctn').css('display', 'block');
    }
}

//delete team
function deleteteam(teammanagerid, teamid, teamcounter) {
    $('.teamMainDiv_Message').html('');
    var deleteteamstatus = confirm("Do you want to delete the team?");
    if (deleteteamstatus === true) {
        $.ajax({
            url: 'delete-team',
            method: 'POST',
            data: {'teammanagerid': teammanagerid, 'teamid': teamid},
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend: function () {
                $('#pageMessageDiv').html('processing...').removeClass().addClass("error");
                ;
            },
            success: function (result) {
                var returnStatus = parseInt(result);
                if (returnStatus === 1) {
                    $('#teams-list-wrapper_' + teammanagerid + '_' + teamcounter).remove();
                    $('#pageMessageDiv').removeClass().addClass("success").html('Team deleted successfully');
                    var teamcount = $('.teams-list-wrapper').length;
                    if (teamcount === 0) {
                        var noteamdiv = '<div id="noteamdiv" class="dash-empty-ctn noteamdiv"><div class="display-table text-center"><span class="display-cell dash-empty-text">No teams appear to be here yet.<a style="cursor:pointer;" id="addteam" data-action="#team-creation-ctn">Add</a> one right now</span></div></div>';
                        $('#team-list-ctn').prepend(noteamdiv);
                    }
                } else if (returnStatus === 0) {
                    $('#pageMessageDiv').removeClass().addClass("error").html('There is some technical problem. Please try again after some times');
                }
            }
        });
    }
}

//get subordinae content
function getchildcontent(subordinateid, teammanagerid, name, email, designation, department, profile_image, existinguserstatus, teamcounter) {
    var content = '<li id="subordinate_' + subordinateid + '" class="clearfix item-list-inner subordinate_class subordinate_class_' + teammanagerid + '_' + teamcounter + ' managersubordinate_' + teammanagerid + '_' + teamcounter + ' teamclass_' + subordinateid + '" rel="' + subordinateid + '">' +
            '<ul class="list-unstyled item-row clearfix ">' +
            '<li class="col-xs-12 col-sm-4 item-info">' +
            '<a class="item-left"><img id="profile_image_' + subordinateid + '" src="' + profile_image + '" class="img-circle profile_image" width="35" height="35" alt=""></a>' +
            '<div class="item-right">' +
            '<div class="pull-right visible-xs">' +
            '<a rel="' + teammanagerid + '" existinguserstatus="' + existinguserstatus + '" style="cursor:pointer;" id="deletelink_' + subordinateid + '" teamcounter = "' + teamcounter + '" onclick="deleteuser(' + subordinateid + ', ' + teammanagerid + ', 2, $(this).attr(\'teamcounter\'));" ><i class="ic-delete ic-16"></i></a>' +
            '</div>' +
            '<div class="item-right">' +
            '<h5 class="name"><span rel="popover" id="name_' + subordinateid + '">' + name + '</span></h5>' +
            '<p class="designation hidden-xs" id="disignation_department_' + subordinateid + '">' + designation + '<span class="visible-xs">, ' + department + '</span></p>' +
            '<p class="designation visible-xs">' + designation + '<span class="">, ' + department + '</span></p>' +
            '</adiv>' +
            '</div>' +
            '</li>' +
            '<li class="col-md-3 col-sm-2 department hidden-xs">' + department + '</li>' +
            '<li class="col-sm-4 email hidden-xs" id="email_' + subordinateid + '">' + email + '</li>' +
            '<li class="col-md-1 col-xs-2 col-sm-2 hidden-xs">' +
            '<div class="pull-right">' +
            '<a rel="' + teammanagerid + '" existinguserstatus="' + existinguserstatus + '" style="cursor:pointer;" id="deletelink_' + subordinateid + '" teamcounter = "' + teamcounter + '" onclick="deleteuser(' + subordinateid + ', ' + teammanagerid + ', 2, $(this).attr(\'teamcounter\'));" ><i class="ic-delete ic-16"></i></a>' +
            '</div>' +
            '</li>' +
            '</ul>' +
            '</li>';
    return content;
}

//get team manager content
function getTeamManager(teammanagerid, profile_image, name, designation, savedmanagerid, teamcounter) {
    var teamManager = '<div class="teamMainDiv_Message" id="teamMainDiv_Message_' + teammanagerid + '_' + teamcounter + '"></div><div id="teamMainDiv_' + teammanagerid + '_' + teamcounter + '" teammanagerid="' + teammanagerid + '" class="pos-r teamMainDiv_class"><ul class="teams-list" id="myteamslist_' + teammanagerid + '_' + teamcounter + '" rel="">' +
            '<li id="team-manager_' + teammanagerid + '_' + teamcounter + '" rel="' + teammanagerid + '" class="team-manager team-managerclass_' + teammanagerid + '">' +
            '<div class="pull-right">' +
            '<div class="pull-right action-col visible-xs">' +
            '<div class="btn-group">' +
            '<button id="actiondropdown_' + teammanagerid + '_' + teamcounter + '" type="button" class="btn btn-primary dropdown-toggle mobile-assign-user " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" managerid="' + teammanagerid + '">Action <span class="caret"></span>' +
            '</button>' +
            '<ul class="dropdown-menu">' +
            '<li><a style="cursor:pointer;" id="changemanager_' + teammanagerid + '_' + teamcounter + '" rel="0" teamcounter="' + teamcounter + '" onclick="changemanager(' + teammanagerid + ', 0, ' + savedmanagerid + ', $(this).attr(\'teamcounter\'));" teamid="0" savedmanagerid="' + savedmanagerid + '">Change Manager</a></li>' +
            '<li><a style="cursor:pointer;display:none;" teamid="0" teamcounter="' + teamcounter + '" onclick="deleteteam(' + teammanagerid + ', $(this).attr(\'teamid\'), $(this).attr(\'teamcounter\'));" id="deleteteam_' + teammanagerid + '_' + teamcounter + '">Delete Team</a></li>' +
            '<li><a data-target="#assignUserModal" data-toggle="modal" teamcounter= "' + teamcounter + '" style="cursor:pointer;" id="mobileassignUser_' + teammanagerid + '_' + teamcounter + '" rel="' + teammanagerid + '" onclick="disableCreateNewTeam(' + teammanagerid + ');" class="mobileassignUser" data-keyboard="false">Assign User</a></li>' +
            '</ul>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="user-right">' +
            '<a class="user-left">' +
            '<img id="profile_image_' + teammanagerid + '_' + teamcounter + '" src="' + profile_image + '" class="img-circle profile_image" width="35" height="35" alt="">' +
            '</a>' +
            '<div class="user-right">' +
            '<div class="pull-right action-col">' +
            '<button class="hidden-xs btn btn-link" id="changemanager_' + teammanagerid + '_' + teamcounter + '" rel="0" teamcounter="' + teamcounter + '" onclick="changemanager(' + teammanagerid + ', 0, $(this).attr(\'savedmanagerid\'), $(this).attr(\'teamcounter\'));" teamid="0" savedmanagerid="' + savedmanagerid + '">Change Manager</button> ' +
            '<button class="hidden-xs btn btn-link" id="deleteteam_' + teammanagerid + '_' + teamcounter + '" teamid="0" teamcounter="' + teamcounter + '" onclick="deleteteam(' + teammanagerid + ', $(this).attr(\'teamid\'), $(this).attr(\'teamcounter\'));">Delete Team</button> ' +
            '<button id="assignUser_' + teammanagerid + '_' + teamcounter + '" rel="' + teammanagerid + '" teamcounter= "' + teamcounter + '" onclick="disableCreateNewTeam(' + teammanagerid + ');" class="hidden-xs btn btn-primary assign-user user" data-action="#user-creation-ctn">Assign User</button> ' +
            '<button id="doneTeam_' + teammanagerid + '_' + teamcounter + '" rel="' + teammanagerid + '" teamcounter= "' + teamcounter + '" rel2="0" newteam="1" data-action="#user-creation-ctn" class="btn done-user btn-success done mr-r5" onclick="enableCreateNewTeam(' + teammanagerid + ', $(this).attr(\'teamcounter\'));" oldmanagerid="0" savedmanagerid="' + savedmanagerid + '">Done</button> ' +
            '</div>' +
            '<div class="user-right">' +
            '<h5 class="name">' + name + '</h5>' +
            '<p class="designation">' + designation + '</p>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</li>' +
            '</ul>' +
            '<div id="myteamschildlist_' + teammanagerid + '_' + teamcounter + '" class="user-creation-ctn myteamschildlist" mymanagerid="' + teammanagerid + '"><div class="user-drop-area" id="user-drop-area_' + teammanagerid + '_' + teamcounter + '">' +
            '<p id="teammessage_' + teammanagerid + '_' + teamcounter + '" class="text-center">Drag & Drop a User from left Sidebar to Assign</p>' +
            '</div></div>';

    return teamManager;
}

//get team subordinae ids at the time of change manager
function updateteamlist(userid, teammanagerid, teamcounter) {
    var teamlist = $('#myteamslist_' + teammanagerid + '_' + teamcounter).attr('rel');
    if (teamlist != '') {
        var teamlistarr = teamlist.split(',');
        var myval = -1;
        for ($k = 0; $k < teamlistarr.length; $k++) {
            if (parseInt(userid) === parseInt(teamlistarr[$k])) {
                myval = $k;
            }
        }
        if (myval > -1) {
            teamlistarr.splice(myval, 1);
        }
        $('#myteamslist_' + teammanagerid + '_' + teamcounter).attr('rel', teamlistarr.toString());
    }
}