// CREATE-NEW-DEPARTMENT
$(document).ready(function () {
    $("#createDepartment").on('submit', (function (e) {
        var identifyForm = $(this).attr("data-identify");
        e.preventDefault();
        $.ajax({
            url: 'create-department',
            method: 'POST',
            data: $(this).serialize(),
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                if (response.statusCode == 200) {
                    var message = "New Department is added.";
                    $('#createDepartment').find('.error-ctn')
                            .text(message)
                            .css('color', 'green').fadeIn(300);
                    $('#createDepartment').trigger("reset");
                    setTimeout("$('#newDepartmentModal').modal('hide');", 800);
                    getDepartment(identifyForm);
                } else {
                    alert("There might be some error occured.");
                }
            },
            error: function (response) {
                var error = jQuery.parseJSON(response.responseText);
                if (error.statusCode == 412) {
                    var message = error.message;
                    $('#createDepartment').find('.error-ctn')
                            .text(message)
                            .css('color', 'red').fadeIn(300);
                } else {
                    alert("There might be some error occured.");
                }
            }
        });
    }));
});