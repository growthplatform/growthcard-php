$(document).ready(function (e) {
    //custom popover
    $('[data-toggle="popover"]').popover({
        html: true,
        trigger: 'manual',
        container: 'body',
        template: '<div class="popover user-popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>',
        content: function () {
            var info = $(this);
            var content = ['<div class="user-body"><img src="' + info.data("image") + '" class="img-circle" width="70" height="70" alt="">',
                '<h4 class="item-name">' + info.data("name") + '</h4>',
                '<h5>' + info.data("company") + '</h5>',
                '<p>' + info.data("department") + '</p>',
                '<p>' + info.data("designation") + '</p>',
                '<p>' + info.data("days") + '</p>',
                '</div><div class="user-footer">',
                '<a href="mailto:email@appster.com.au">' + info.data("email") + '</a>',
                '</div>'].join('');
            return content;
        },
        placement: 'bottom',
    }).click(function (e) {
        $(this).popover('toggle');
    })

    // check popover status on click of body
    $('body').on('click', function (e) {
        $('[data-toggle="popover"]').each(function () {
            //the 'is' for buttons that trigger popups
            //the 'has' for icons within a button that triggers a popup
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                $(this).popover('hide');
            }
        });
    });

    // custom scrollbar
    $(window).load(function () {
        $(".sidebar-scroll-ctn").mCustomScrollbar();
    });
});