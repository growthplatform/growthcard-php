$(document).ready(function () {
    $('.addnewdepartment').click(function () {
        $('.departmentName_error').html('');
        $('#departmentName').val('');
    });
    $('.createNewJob').click(function () {
        $('.jobName_error').html('');
        $('#jobName').val('');
    });

    $('.addnewuser').click(function () {
        $('.createUser_error').html('');
        $('#firstName').val('');
        $('#lastName').val('');
        $('#email').val('');
        $('#password').val('');
        $("#createUser > * > * > *").removeClass("has-error");
        var validator = $("#createUser").validate();
        validator.resetForm();
    });
    $(document).on('focus', ':input', function () {
        $(this).attr('autocomplete', 'off');
    });
});

$('#createUserModal').on('shown.bs.modal', function () {
    $('input[type=email], input[type=password]').val('');
})

$('#newJobModal').on('show.bs.modal', function (e) {
    var target = $(e.relatedTarget).data('overlay')
    $(this).find('#createJob').attr("data-identify", target);
    $('#createUserModal, #editUserModal').css('z-index', 1039);
});

$('#newJobModal').on('hidden.bs.modal', function () {
    $('#createUserModal,#editUserModal').css('z-index', 1041);
});

$('#newDepartmentModal').on('show.bs.modal', function (e) {
    var target = $(e.relatedTarget).data('overlay')

    $(this).find('#createDepartment').attr("data-identify", target);
    $('#createUserModal, #editUserModal').css('z-index', 1039);
});

$('#newDepartmentModal').on('hidden.bs.modal', function () {
    $('#createUserModal,#editUserModal').css('z-index', 1041);
});

//GET-USER-DATA
function get_edit_user_data(id)
{
    get_user_data("edit-jobs-list", "edit-department-list")
    var url = 'user/' + id + '/edit';
    $.ajax({
        type: "GET",
        url: url,
        dataType: 'json',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response)
        {
            $("#editUserModal").modal();
            $("#editUser input[name=firstName]").val(response.result.firstName);
            $("#editUser input[name=lastName]").val(response.result.lastName);
            $("#editUser input[name=userId]").val(id);
            $("#editUser input[name=email]").val(response.result.email);
            $("#editUser input[name=password]").val('');
            $("#editUser input[name=companyName]").val(response.result.member.company.companyName);
            $('#edit-jobs-list').selectpicker('val', response.result.member.designation.designationId);
            $('#edit-department-list').selectpicker('val', response.result.userdepartment.department.departmentId);
            $("#editUser input[name=userId]").val(response.result.userId);
        }
    });
}

//DELETE USER
var flag = 0;
function destroy_user(id)
{
    var url = 'user/' + id;
    var r = confirm('Do you want to Delete ?');
    if (r == true) {
        $.ajax({
            type: "DELETE",
            url: url,
            data: {"flag": flag},
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                if (response.statusCode == 200) {
                    location.reload();
                } else {
                    alert("There might be some error occured.");
                }
            },
            error: function (response) {
                var error = jQuery.parseJSON(response.responseText);
                if (error.statusCode == 412) {
                    alert(error.message);
                } else if (error.statusCode == 605) {
                    var check = confirm('This User is assigned as a Manager in another team. Deleting a Manager will also be delete his team. Do you still want to continue?');
                    if (check == true) {
                        flag = 1;
                        $.ajax({
                            type: "DELETE",
                            url: url,
                            dataType: 'json',
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data: {"flag": flag},
                            success: function (response) {
                                if (response.statusCode == 200) {
                                    location.reload();
                                }
                            }
                        });
                    }
                } else {
                    alert("There might be some error occured.");
                }
            },
        });

    }
}

function get_user_data(job, department) {
    var jobId = "#" + job,
            departMentId = "#" + department;
    $.ajax({
        url: 'designation',
        method: 'GET',
        dataType: 'json',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            if (response.statusCode == 200) {
                var html = '';
                $.each(response.result, function (key, val) {
                    html += "<option value ='" + val.designationId + "'>" + val.designationTitle + "</option>";
                });
                $(jobId).html(html).selectpicker('refresh');
            } else {
                alert("There might be some error occured.");
            }
        }
    });
    $.ajax({
        url: 'department',
        method: 'GET',
        dataType: 'json',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            if (response.statusCode == 200) {
                var html = '';
                $.each(response.result, function (key, val) {
                    html += "<option value ='" + val.departmentId + "'>" + val.departmentName + "</option>";
                });
                $(departMentId).html(html).selectpicker('refresh');
            } else {
                alert("There might be some error occured.");
            }
        }
    });

}

//GET-JOB-LIST
function  getJob(job) {
    jobId = "#" + job;
    $.ajax({
        url: 'designation',
        method: 'GET',
        dataType: 'json',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            console.log(response);
            if (response.statusCode == 200) {
                var html = '';
                $.each(response.result, function (key, val) {
                    html += "<option value ='" + val.designationId + "'>" + val.designationTitle + "</option>";
                });
                $(jobId).html(html).selectpicker('refresh');
            } else {
                alert("There might be some error occured.");
            }
        }
    });
}

//GET-DEPARTMENT-LIST
function  getDepartment(department) {
    departMentId = "#" + department;
    $.ajax({
        url: 'department',
        method: 'GET',
        dataType: 'json',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            console.log(response);
            if (response.statusCode == 200) {
                var html = '';
                $.each(response.result, function (key, val) {
                    html += "<option value ='" + val.departmentId + "'>" + val.departmentName + "</option>";
                });

                $(departMentId).html(html).selectpicker('refresh');
            } else {
                alert("There might be some error occured.");
            }
        }
    });

}

$('#createUserModal').on('shown.bs.modal', function () {
    get_user_data("create-jobs-list", "create-department-list");
});

$('.close').trigger("reset");

//EDIT-USER-DATA
$(document).ready(function () {
    if ($('#editUser').length > 0) {
        var editUserForm = $('#editUser');
        editUserForm.validate({
            ignore: ":hidden:not(.selectpicker)",
            onkeyup: false,
            onclick: false,
            rules: {
                firstName: {
                    required: true,
                },
                lastName: {
                    required: true,
                },
                email: {
                    required: true,
                },
                companyName: {
                    required: true,
                },
                password: {
                    required: true,
                    minlength: 3,
                },
                job: {
                    required: true,
                },
                department: {
                    required: true,
                },
            },
            submitHandler: function (form, event) {
            }
        });

        editUserForm.on('submit', (function (e) {
            if (editUserForm.valid()) {
                e.preventDefault();
                var userId = $("#editUser input[name=userId]").val();
                var url = 'user/' + userId;
                $.ajax({
                    url: url,
                    method: 'PUT',
                    data: $(this).serialize(),
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (response) {
                        if (response.statusCode == 200) {
                            var message = "User is modified."
                            editUserForm.find(".error-ctn").html('<p id="firstName-error" class="success">' + message + '</p>');
                            location.reload();
                        } else {
                            alert("There might be some error occured.");
                        }
                    },
                    error: function (response) {
                        var error = jQuery.parseJSON(response.responseText);
                        if (error.statusCode == 412) {
                            editUserForm.find(".error-ctn").html('<p id="firstName-error" class="error">' + error.message + '</p>');
                        } else {
                            alert("There might be some error occured.");
                        }
                    },
                });
            }
        }));
    }

    if ($('#createUser').length > 0) {
        var createUserForm = $('#createUser');
        createUserForm.validate({
            ignore: ":hidden:not(.selectpicker)",
            onkeyup: false,
            onclick: false,
            rules: {
                firstName: {
                    required: true,
                },
                lastName: {
                    required: true,
                },
                email: {
                    required: true,
                },
                companyName: {
                    required: true,
                },
                password: {
                    required: false,
                    minlength: 6,
                    /*noSpace:true*/
                },
                job: {
                    required: true,
                },
                department: {
                    required: true,
                },
            },
            submitHandler: function (form, event) {
            }
        });

        createUserForm.on('submit', (function (e) {
            if (createUserForm.valid()) {
                e.preventDefault();
                $.ajax({
                    url: 'user',
                    method: 'POST',
                    data: $(this).serialize(),
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (response) {
                        if (response.statusCode == 200) {
                            var message = "New User is added."
                            createUserForm.find(".error-ctn").html('<p id="firstName-error" class="success">' + message + '</p>');
                            location.reload();
                        } else {
                            alert("There might be some error occured.");
                        }
                    },
                    error: function (response) {
                        var error = jQuery.parseJSON(response.responseText);
                        if (error.statusCode == 412) {
                            createUserForm.find(".error-ctn").fadeOut(3000)
                            createUserForm.find(".error-ctn").html('<p id="firstName-error" class="error">' + error.message + '</p>');
                            $('#createUser')[0].reset();
                            $(".selectpicker").selectpicker('refresh')
                        } else {
                            alert("There might be some error occured.");
                        }
                    },
                });
            }
        }));
    }
})
