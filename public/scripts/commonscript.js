$(document).ready(function (e) {
    $(document).on("keypress", "input[type='password'],.password-space", function (e) {
        if (e.keyCode == 32) {
            $(this).val($(this).val() + "");
            $(this).parents("form").find(".error-ctn").html('<p id="firstName-error" class="error">Space not allowed</p>')
            return false; // return false to prevent space from being added
        }
    });
    $(function () {
        $('.password-visibility').password()
    });

    // Set Default validator for all
    $.validator.setDefaults({
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorClass: 'help-block',
        errorPlacement: function (error, element) {

        },
        invalidHandler: function (event, validator) {
            // 'this' refers to the form
            var errors = validator.numberOfInvalids();
            if (errors) {
                var message = errors == 1
                        ? 'You missed 1 field. It has been highlighted'
                        : 'You missed ' + errors + ' fields. They have been highlighted';
                $(this).find(".error-ctn").html('<p class="error">' + message + '</p>');
                $(this).find(".error-ctn").show();
            } else {
                $(this).find(".error-ctn").hide();
            }
        },
    });

    $('.modal').on('hidden.bs.modal', function () {
        $('#usertypelabel').attr('managerid', 0);
    })
});
