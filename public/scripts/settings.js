$(document).ready(function () {
    $(document).on('focus', ':input', function () {
        $(this).attr('autocomplete', 'off');
    });
    
    //CHANGE-PASSWORD
    $("#setting").on('submit', (function (e) {
        e.preventDefault();
        var temp = $(this).serialize();
        var oldPassword = $("#oldPassword").val();
        var newPassword = $("#newPassword").val();
        var retypePassword = $("#retypePassword").val();
        if (newPassword != retypePassword) {
            var message = "Confirm password doesn't match with password.";
            $('.new-window').fadeIn(5000)
                    .find('p')
                    .text(message)
                    .css('color', 'red');
            $('#setting').trigger("reset");
            return false;
        } else
        {
            $.ajax({
                url: 'change-password',
                method: 'POST',
                data: {'oldPassword': oldPassword, 'newPassword': newPassword},
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                    if (response.statusCode == 200) {
                        var message = "Your password has been changed.";
                        $('.new-window').fadeIn(300)
                                .find('p')
                                .text(message)
                                .css('color', 'green');

                        setTimeout("location.reload(true);", 800);
                    } else {
                        alert("There might be some error occured.");
                    }
                },
                error: function (response) {
                    var error = jQuery.parseJSON(response.responseText);
                    if (error.statusCode == 401 || error.statusCode == 412) {
                        var message = error.message;
                        $('.new-window').fadeIn(300)
                                .find('p')
                                .text(message)
                                .css('color', 'red');
                        $('#setting').trigger("reset");
                    } else {
                        alert("There might be some error occured.");
                    }
                },
            });
        }
    }));
});