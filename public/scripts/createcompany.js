//CREATE-NEW-COMPANY
$(document).ready(function () {
    $('#createCompanyModal').on('shown.bs.modal', function () {
        $('input[type=email], input[type=password]').val('');
    })
    $('.close').trigger("reset");
    $("#createCompany").on('submit', (function (e) {
        e.preventDefault();
        $.ajax({
            url: 'company',
            method: 'POST',
            data: $(this).serialize(),
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                if (response.statusCode == 200) {
                    var message = "New Company is created."
                    $('#createCompany').find(".error-ctn").html('<p id="firstName-error" class="success">' + message + '</p>');
                    location.reload();
                } else {
                    alert("There might be some error occured.");
                }
            },
            error: function (response) {
                var error = jQuery.parseJSON(response.responseText);
                if (error.statusCode == 412) {
                    $('#createCompany').find(".error-ctn").html('<p id="firstName-error" class="error">' + error.message + '</p>');
                } else {
                    alert("There might be some error occured.");
                }
            },
        });
    }));
});