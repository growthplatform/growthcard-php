//CREATE-NEW-EFFECTIVE-ACTION
$(document).ready(function () {
    $('#createEffectiveModal').on('shown.bs.modal', function () {
        get_user_data("create-jobs-list");
    });
    $("#effectiveAction").on('submit', (function (e) {
        e.preventDefault();
        $.ajax({
            url: 'create-effective-action',
            method: 'POST',
            data: $(this).serialize(),
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                if (response.statusCode == 200) {
                    var message = "New Effective Action is added.";
                    $('#effectiveAction').find('.error-ctn')
                            .text(message)
                            .css('color', 'green').fadeIn(300);
                    $('#effectiveAction').trigger("reset");
                    setTimeout("location.reload(true);", 800);
                } else {
                    alert("There might be some error occured.");
                }
            },
            error: function (response) {
                var error = jQuery.parseJSON(response.responseText);
                if (error.statusCode == 412) {
                    var message = error.message;
                    $('#effectiveAction').find('.error-ctn')
                            .text(message)
                            .css('color', 'red').fadeIn(300);
                    $('#effectiveAction').trigger("reset");
                } else {
                    alert("There might be some error occured.");
                }
            },
        });
    }));
});