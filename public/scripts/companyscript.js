//GET-COMPANY-DATA
function get_edit_company_data(id, event)
{
    event.stopPropagation();
    var url = 'company/' + id + '/edit';
    $.ajax({
        type: "GET",
        url: url,
        dataType: 'json',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response)
        {
            $("#editCompanyModal").modal();
            $("#editCompany input[name=firstName]").val(response.result.companyadmin.firstName);
            $("#editCompany input[name=lastName]").val(response.result.companyadmin.lastName);
            $("#editCompany input[name=email]").val(response.result.companyadmin.email);
            $("#editCompanyModal input[name=password]").val('');
            $("#editCompany input[name=companyId]").val(id);
            $("#editCompany input[name=companyName]").val(response.result.companyName);
        }
    });
}

//DELETE COMPANY
function destroy_company(id, event)
{
    event.stopPropagation();
    var url = 'company/' + id;
    var r = confirm('Do you want to Delete?');
    if (r == true) {
        $.ajax({
            type: "DELETE",
            url: url,
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                if (response.statusCode == 200) {
                    window.location.reload();
                    return false;
                } else {
                    alert("There might be some error occured.");
                }
            },
            error: function (response) {
                var error = jQuery.parseJSON(response.responseText);
                if (error.statusCode == 412) {
                    alert(error.message);
                } else {
                    alert("There might be some error occured.");
                }
            },
        });
    }
}

//EDIT COMPANY
$("#editCompany").on('submit', (function (e) {
    e.preventDefault();
    var compnayId = $("#editCompany input[name=companyId]").val();
    var url = 'company/' + compnayId;
    $.ajax({
        url: url,
        method: 'PUT',
        data: $(this).serialize(),
        dataType: 'json',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            if (response.statusCode == 200) {
                var message = "Company is modified."
                $('#editCompany').find(".error-ctn").html('<p id="firstName-error" class="success">' + message + '</p>');
                setTimeout("location.reload(true);", 200);
                return false;
            } else {
                alert("There might be some error occured.");
            }
        },
        error: function (response) {
            var error = jQuery.parseJSON(response.responseText);
            if (error.statusCode == 412) {
                $('#editCompany').find(".error-ctn").html('<p id="firstName-error" class="error">' + error.message + '</p>');
            } else {
                alert("There might be some error occured.");
            }
        },
    });
}));

//LOAD COMPANY DASHBOARD DATA
function load_company_dashboard(id) {
    url = 'companyId/' + id
    $.ajax({
        url: url,
        method: 'GET',
        dataType: 'json',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            window.location.replace("dashboard");
        },
    });
}