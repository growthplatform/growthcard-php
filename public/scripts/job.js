//EDIT-JOB
$(document).ready(function () {
    $("#editJob").on('submit', (function (e) {
        e.preventDefault();
        $.ajax({
            url: 'edit-job',
            method: 'POST',
            data: $(this).serialize(),
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                if (response.statusCode == 200) {
                    var message = "Job Update successfully.";
                    $('.new-window').fadeIn(300)
                            .find('p')
                            .text(message)
                            .css('color', 'green');
                    setTimeout("location.reload(true);", 800);
                } else {
                    alert("There might be some error occured.");
                }
            },
            error: function (response) {
                var error = jQuery.parseJSON(response.responseText);
                if (error.statusCode == 412) {
                    var message = error.message;
                    $('.new-window').fadeIn(300)
                            .find('p')
                            .text(message)
                            .css('color', 'red');
                } else {
                    alert("There might be some error occured.");
                }
            },
        });
    }));

//CREATE-JOB
    $("#createJob").on('submit', (function (e) {
        var identifyForm = $(this).attr("data-identify");
        var flag = $("#flag").val();
        e.preventDefault();
        $.ajax({
            url: 'create-job',
            method: 'POST',
            data: $(this).serialize(),
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                if (response.statusCode == 200) {
                    var message = "New Job is added.";
                    $('#createJob').find('.error-ctn')
                            .text(message)
                            .css('color', 'green').fadeIn(300);
                    if (flag == "1") {
                        setTimeout("location.reload(true);", 800);
                    } else {
                        $('#createJob').trigger("reset");
                        setTimeout("$('#newJobModal').modal('hide');", 800);
                        getJob(identifyForm);
                    }
                } else {
                    alert("There might be some error occured.");
                }
            },
            error: function (response) {
                var error = jQuery.parseJSON(response.responseText);
                if (error.statusCode == 412) {
                    var message = error.message;
                    $('#createJob').find('.error-ctn')
                            .text(message)
                            .css('color', 'red').fadeIn(300);
                } else {
                    alert("There might be some error occured.");
                }
            }
        });
    }));
});