(function ($) {
    $(document).on('focus', ':input', function () {
        $(this).attr('autocomplete', 'off');
    });
    $.fn.changeColor = function (options) {
        var setting = $.extend({
            color: '#00ff00',
            backgroundColor: '#000000',
        }, options)

        return this.each(function () {
            $(this).css({
                color: setting.color,
                backgroundColor: setting.backgroundColor,
            })
            $(this).on("click", function () {
                $(this).css({
                    color: '#ff00ff',
                    backgroundColor: '#ff0000',
                })

            })
        })
    }
}(jQuery));

function manageEdit(data) {
    if ($(data).text() == "Edit") {
        $('.messagecount').show();
        $(data).addClass("btn-success").text("Done");
        $(data).parents('#manage-wrapper').find(".btn-file").show();
        $(data).parents('#manage-wrapper').find('input, select, textarea').each(function () {
            $(this).attr('readonly', false);
        });
    } else {
        $('#uploadimage_form').submit();
    }
}

$(document).ready(function (e) {
    $(document).on("click", ".img-delete", function (e) {
        $(this).parents(".popup-img-ctn").remove();
    })
});
