jQuery.event.props.push('dataTransfer');
var maximagecount = 5;
var imagetypearr = ["image/png", "image/jpeg", "image/jpg"];
var text_max = 160;
var destination_path = $('#destination_path').val();
var incorrectfilecount = 0;
var incorrectfilesizecount = 0;
var err = '';
var allowedsizeinbytes = 5242880; //5 MB
var allowedcsvsizeinbytes = 2097152; //5 MB
var allowedsizeinMB = '5 MB'; //5 MB
var allowedcsvsizeinMB = '2 MB'; //5 MB

// IIFE to prevent globals
(function () {
    var imagelength = $('#manage-img-list li').length;
    if (imagelength < maximagecount) {
        $('#uploadimage').prop("disabled", false);
    } else {
        $('#uploadimage').prop("disabled", true);
    }

    $('.message').each(function () {
        var relval = $(this).attr('rel');
        $('#message_' + relval).attr('maxlength', text_max);
        var text_length = $('#message_' + relval).val().length;
        var text_remaining = text_max - text_length;
        if (text_remaining > 1) {
            $('#messagecount_' + relval).html(text_remaining + ' characters remaining');
        } else {
            $('#messagecount_' + relval).html(text_remaining + ' character remaining');
        }
    });

    $("body").on("keyup", '.message', function (event) {
        var relval = $(this).attr('rel');
        var text_length = $('#message_' + relval).val().length;
        var text_remaining = text_max - text_length;
        if (text_remaining > 1) {
            $('#messagecount_' + relval).html(text_remaining + ' characters remaining');
        } else {
            $('#messagecount_' + relval).html(text_remaining + ' character remaining');
        }
    });

    $('.uploadimage').click(function () {
        $('#uploadimagemessage').html('');
        $('#manage-popup-img-wrapper').html('');
        var finalLength = getFileCount();
        if (finalLength <= maximagecount) {
            $('#useimagefile').prop("disabled", false);
            $(this).attr('data-target', '#uploadImageModal');
        } else {
            $('#useimagefile').prop("disabled", true);
        }
    });

    $('#uploadimageformcross').click(function () {
        $('#manage-popup-img-wrapper').html('');
    });

    $('#uploadimage_form').submit(function (e) {
        var shouldsubmit = true;
        $('body').find('.caption').each(function () {
            var relval = $(this).attr('rel');
            if ($.trim($('#caption_' + relval).val()) == '') {
                $('#caption_error_' + relval).html('Please enter some text').removeClass().addClass("error");
                shouldsubmit = false;
            } else {
                $('#caption_error_' + relval).html('');
            }
        });

        $('body').find('.message').each(function () {
            var relval = $(this).attr('rel');
            if ($.trim($('#message_' + relval).val()) == '') {
                $('#message_error_' + relval).html('Please enter some text').removeClass().addClass("error");
                shouldsubmit = false;
            } else {
                $('#message_error_' + relval).html('');
            }
        });

        if (shouldsubmit === true) {
            return true;
        } else {
            return false;
        }
    });

    $("#uploadcsvform").submit(function (e) {
        var csvfileuploadedlength = document.getElementById('csvcreateteam').files.length;
        var ajaxfileuploadstatus = parseInt($('#ajaxfileuploadstatus').val());
        if (csvfileuploadedlength > 0) {
            var uploadedfile = document.getElementById('csvcreateteam').files[0];
            if (uploadedfile.type == 'text/csv') {
                var sizeinbytes = parseInt(uploadedfile.size);
                if (sizeinbytes > allowedcsvsizeinbytes) {
                    $('#csvmessage').html('File more than ' + allowedcsvsizeinMB + '. can\'t be uploaded').removeClass().addClass("selected-file error");
                    return false;
                } else {
                    $('#fileuploadmode').val('browseupload');
                    $('#ajaxfilepath').val('');
                    $('#ajaxfilename').val('');
                    $('#ajaxfilemimetype').val('');
                    if (ajaxfileuploadstatus === 2) {
                        return true;
                    } else if (ajaxfileuploadstatus === 1) {
                        $('#loading').html('Please wait, file is uploading...').removeClass().addClass("selected-file error");
                        return false;
                    } else {
                        $('#csvmessage').html('Please upload valid CSV file').removeClass().addClass("selected-file error");
                        return false;
                    }
                }
            } else {
                $('#csvmessage').html('Please upload valid CSV file').removeClass().addClass("selected-file error");
                return false;
            }

        } else {
            if (ajaxfileuploadstatus === 2) {
                return true;
            } else if (ajaxfileuploadstatus === 1) {
                $('#loading').html('Please wait, file is uploading...').removeClass().addClass("selected-file error");
                return false;
            } else {
                $('#csvmessage').html('Please upload valid CSV file').removeClass().addClass("selected-file error");
                return false;
            }
        }
    });

    $("#useimagefile").click(function (e) {
        var filecount = getFileCount();
        if (filecount <= maximagecount) {
            $('#pageMessageDiv').html('');
            var popupImageLen = $("#manage-popup-img-wrapper img").length;
            if (popupImageLen > 0) {
                var allfilesuploaded = false;
                var atleastonefileslefttoupload = false;
                $('.ajaxfileuploaderror').each(function (k, val) {
                    var errorstatus = parseInt($(this).val());
                    if (errorstatus === 0) {
                        var relval = $(this).attr('rel');
                        var filetypefound = false;
                        var sizeinbytes = parseInt($('#ajaxfilesize_' + relval).val());
                        for (var i in imagetypearr) {
                            if (imagetypearr[i] == $('#ajaxfilemimetype_' + relval).val()) {
                                filetypefound = true;
                            }
                        }
                        if (filetypefound === false) {
                            $('#uploadimagemessage').html('Please upload valid image file').removeClass().addClass("selected-file error");
                        } else if (sizeinbytes > allowedsizeinbytes) {
                            $('#uploadimagemessage').html('File more than ' + allowedsizeinMB + '. can\'t be uploaded').removeClass().addClass("selected-file error");
                        } else {
                            $('.ajaxfileuploadstatus').each(function () {
                                var uploadstatus = parseInt($(this).val());
                                if (uploadstatus === 1) {
                                    atleastonefileslefttoupload = true;
                                    allfilesuploaded = false;
                                } else if (uploadstatus === 2) {
                                    allfilesuploaded = true;
                                }
                            });
                            if (atleastonefileslefttoupload === true) {
                                $('#uploadimagemessage').html('Please wait one or more file is left to upload').removeClass().addClass("selected-file error");
                            } else if (allfilesuploaded === true) {
                                var imagename = $('#ajaxfilename_' + relval).val();
                                var imageval = $('#ajaxfilenewname_' + relval).val();
                                var imagetype = $('#ajaxfilemimetype_' + relval).val();
                                var imagetmp_name = $('#ajaxfilepath_' + relval).val();
                                var imagesize = $('#ajaxfilesize_' + relval).val();
                                var imageerror = $('#ajaxfileuploaderror_' + relval).val();
                                var imagecontent = '<li class="imageli" id="imageli_' + relval + '"><input type="hidden" name="ajaxupload[name][' + relval + ']" value="' + imagename + '" /><input type="hidden" name="ajaxupload[newname][' + relval + ']" value="' + imageval + '" /><input type="hidden" name="ajaxupload[type][' + relval + ']" value="' + imagetype + '" /><input type="hidden" name="ajaxupload[tmp_name][' + relval + ']" value="' + imagetmp_name + '" /><input type="hidden" name="ajaxupload[error][' + relval + ']" value="' + imageerror + '" /><input type="hidden" name="ajaxupload[size][' + relval + ']" value="' + imagesize + '" /><p class="limessage" id="limessage_' + relval + '"></p><div style="background-image:url(' + destination_path + imageval + ')" class="manage-img-ctn"><span class="btn btn-file">Change Image<input type="file" rel="' + relval + '" id="uploadedimage_' + relval + '" name="new_uploadedimage[' + relval + ']" class="manage-img-upload uploadedimage"></span></div><div class="manage-form-ctn"><div class="section-one"><div class="form-group"><label>Caption*</label><input type="text" rel="' + relval + '" name="new_caption[' + relval + ']" id="caption_' + relval + '" value="" class="form-control caption" rel2="new" maxlength="100"><p class="caption_error" name="caption_error_' + relval + '" id="caption_error_' + relval + '"></p></div><div class="form-group"><label>Message*</label><textarea rel="' + relval + '" name="new_message[' + relval + ']" id="message_' + relval + '" rows="3" maxlength="160" class="form-control message" rel2="new" style="resize: none;"></textarea><span class="messagecount" id="messagecount_' + relval + '">160 characters remaining</span><p class="message_error" name="message_error_' + relval + '" id="message_error_' + relval + '"></p></div></div><div class="section-second"><a onclick="deleteElement(\'imageli\', \'' + relval + '\', \'tmp\');" rel="' + relval + '" id="deleteli_' + relval + '" class="manage-row-delete deleteli" style="cursor:pointer;"><i class="ic-delete ic-16"></i></a></div></div></li>';
                                var imagelength = $('#manage-img-list li').length;
                                if (imagelength > 0) {
                                    $('#manage-img-list').prepend(imagecontent);
                                } else {
                                    $('#pageMessageDiv').after('<ul id="manage-img-list" class="list-unstyled col-xs-12">' + imagecontent + '</ul>');
                                }
                                $('#fileuploadmode').val('browseupload');
                                $('#uploadimagesubmit').text("Done");
                                $('#uploadimagesubmit').parents('#manage-wrapper').find(".btn-file").show();
                                $('#uploadimagesubmit').parents('#manage-wrapper').find('input, select, textarea').each(function () {
                                    $(this).attr('readonly', false);
                                });
                                $('body').find('#noteamdiv').remove();
                                $('#uploadimagesubmit').prop("disabled", false);
                                $('body').find('#useimagefile').prop("disabled", false);
                                if (filecount >= maximagecount) {
                                    $('body').find('#uploadimage').prop("disabled", true);
                                }
                            }
                        }
                    }
                });
                if (allfilesuploaded === false) {
                    $('#uploadimagemessage').html('Please wait one or more file is left to upload').removeClass().addClass("selected-file error");
                } else if (allfilesuploaded === true) {
                    $('#uploadimageformcross').click();
                }
            } else {
                $('#uploadimagemessage').html('Please upload valid image file').removeClass().addClass("selected-file error");
            }

        } else {
            $('#uploadimage').prop("disabled", true);
            $('#uploadimagemessage').html('Exceeding image upload limit').removeClass().addClass("selected-file error");
        }
    });

    var s;
    function avatarObject(element, fileType) {
        var ele = element, nameType = fileType;
        this.settings = {
            bod: $(".avatar-drop"),
            fileInput: element
        },
        this.init = function () {
            s = this.settings;
            this.bindUIActions();
            return ele;
        },
                this.bindUIActions = function () {
                    var list = this;
                    var timer;
                    s.bod.on("dragover", function (event) {
                        clearTimeout(timer);
                        if (event.currentTarget == s.bod[0]) {
                            $(ele).parents(".drop-area").css("background-color", "#f1f4f5");
                            list.showDroppableArea();
                        }
                        // Required for drop to work
                        return false;
                    });

                    s.bod.on('dragleave', function (event) {
                        if (event.currentTarget == s.bod[0]) {
                            // Flicker protection
                            timer = setTimeout(function () {
                                list.hideDroppableArea();
                            }, 200);
                        }
                    });

                    s.bod.on('drop', function (event, ele) {
                        // Or else the browser will open the file
                        event.preventDefault();
                        list.handleDrop(event.dataTransfer.files, event);
                    });

                    $(document).delegate(s.fileInput, 'change', function (event) {
                        event.preventDefault();
                        list.handleDrop(event.target.files, event);

                    });
                },
                this.showDroppableArea = function () {
                    s.bod.addClass("droppable");
                },
                this.hideDroppableArea = function () {
                    s.bod.removeClass("droppable");
                },
                this.handleDrop = function (files, event) {

                    var list = this;
                    // Multiple files can be dropped. Lets only deal with the "first" one.
                    var file = files[0];
                    if (nameType == "image") {
                        if (file.type.match('image.*')) {
                            list.resizeImage(file, event, function (data) {
                                list.placeImage(file, data, ele);
                            });
                        } else {
                        }
                    }

                    if (nameType == "csv") {
                        if (file.type == 'text/csv') {
                            var sizeinbytes = parseInt(file.size);
                            if (sizeinbytes > allowedcsvsizeinbytes) {
                                $('#ajaxfileuploadstatus').val('0');
                                $('#csvmessage').html('File more than ' + allowedcsvsizeinMB + ' can\'t be uploaded').removeClass().addClass("selected-file error");
                            } else {
                                list.validateCSV(file);
                            }
                        } else {
                            $('#ajaxfileuploadstatus').val('0');
                            $('#csvmessage').html('Please upload valid CSV file').removeClass().addClass("selected-file error");
                        }
                    }

                },
                this.resizeImage = function (file, event, callback) {
                    var list = this,
                            target = event.target,
                            fileTracker = new FileReader;
                    fileTracker.onload = function (event) {
                        var data = event.target.result;

                        var options = {
                            canvas: true
                        };

                        loadImage.parseMetaData(file, function (data) {
                            var filetypefound = false;
                            for (var i in imagetypearr) {
                                if (imagetypearr[i] == file.type) {
                                    filetypefound = true;
                                }
                            }

                            var sizeinbytes = parseInt(file.size);
                            if (filetypefound === true) {
                                if (sizeinbytes <= allowedsizeinbytes) {
                                    if (data.exif) {
                                        options.orientation = data.exif.get('Orientation');
                                    }
                                    // Load the image from disk and inject it into the DOM with the correct orientation
                                    loadImage(
                                            file,
                                            function (canvas) {
                                                var imgDataURL = canvas.toDataURL("image/jpeg");
                                                list.placeImage(file, imgDataURL, target);
                                            },
                                            options
                                            );
                                } else {
                                    incorrectfilesizecount++;
                                    $('#uploadimagemessage').html('You are trying to upload one or more file having size more than ' + allowedsizeinMB + ', So can\'t be uploaded').removeClass().addClass("selected-file error");
                                }
                            } else {
                                incorrectfilecount++;
                                $('#uploadimagemessage').html('Incorrect file').removeClass().addClass("selected-file error");
                            }
                        });
                    }
                    fileTracker.readAsDataURL(file);
                    fileTracker.onabort = function () {
                    }
                    fileTracker.onerror = function () {
                    }
                },
                this.placeImage = function (file, data, ele) {
                    $(ele).parent().parent().find("img").attr("src", data);

                }
    }

    var uploadCsv = new avatarObject(".csv-upload-file", "csv");
    uploadCsv.validateCSV = function (file) {
        $('#csvmessage').html(file.name);
        var fileuploadmode = $('#fileuploadmode').val();
        if (fileuploadmode != 'browseupload') {
            ajaxfileupload(file);
        }
    }
    var companyPhoto = new avatarObject(".company-photo-upload", "image");
    companyPhoto.placeImage = function (file, data, ele) {
        var wrap = $('#company-photo');
        var imgCtn = '<img src="' + data + '" class=""  alt="Image for Profile">';
        wrap.find("img").remove();
        wrap.html(imgCtn).css("border", "none");
        wrap.parent().find(".change-button").css("display", "block");
        wrap.parents("form").find(':submit').prop('disabled', false);
    }
    var settingPhoto = new avatarObject(".setting-img-upload", "image");
    settingPhoto.placeImage = function (file, data, ele) {
        var myDiv = $(ele).parents(".setting-img-ctn")
        $(ele).parents(".setting-img-ctn").find("input:text").val(file.name);
        var imgCtn = '<img src="' + data + '" class="img-circle"  alt="Image for Profile" width="70" height="70">';
        myDiv.find(".upload-img-left img").remove();
        myDiv.find(".upload-img-left").prepend(imgCtn).css("border", "none");

    }

    var managePhoto = new avatarObject(".manage-img-upload", "image");
    managePhoto.placeImage = function (file, data, ele) {
        var myDiv = $(ele).parents(".manage-img-ctn");
        myDiv.css("background-image", 'url(' + data + ')')
    }
    var manageUplaod = new avatarObject(".manage-upload-img", "image");
    manageUplaod.handleDrop = function (files) {

        finalLength = getFileCount() + files.length;
        if (finalLength <= maximagecount) {
            var list = this;
            // Multiple files can be dropped. Lets only deal with the "first" one.
            for (var l = 0; l < files.length; l++) {
                var file = files[l];
                if (file.type.match('image.*')) {
                    list.resizeImage(file, function (data) {
                        list.placeImage(file, data, ele);
                    });
                } else {
                    $('#uploadimagemessage').html('Please upload valid image file').removeClass().addClass("selected-file error");
                    return;
                }
            } //first for loop
        } else {
            $('#uploadimagemessage').html('Exceeding image upload limit').removeClass().addClass("selected-file error");
        }
    }

    manageUplaod.placeImage = function (file, data, ele) {
        var counter = 't' + $('#manage-popup-img-wrapper > .popup-img-ctn').length;
        var wrap = $('#manage-popup-img-wrapper');
        var imgCtn = '<div id="popup-img-ctn_' + counter + '" class="popup-img-ctn" rel="' + counter + '"><input type="hidden" name="ajaxfilepath[]" id="ajaxfilepath_' + counter + '" value="" class="ajaxfilepath" /><input type="hidden" name="ajaxfilename[]" id="ajaxfilename_' + counter + '" value="" class="ajaxfilename" /><input type="hidden" name="ajaxfilemimetype[]" id="ajaxfilemimetype_' + counter + '" value="" class="ajaxfilemimetype" /><input type="hidden" name="ajaxfilenewname[]" id="ajaxfilenewname_' + counter + '" value="" class="ajaxfilenewname" /><input type="hidden" name="ajaxfilesize[]" id="ajaxfilesize_' + counter + '" value="" class="ajaxfilesize" /><input type="hidden" name="ajaxfileuploaderror[]" id="ajaxfileuploaderror_' + counter + '" value="-1" class="ajaxfileuploaderror" rel="' + counter + '" /><input type="hidden" name="ajaxfileuploadstatus[]" id="ajaxfileuploadstatus_' + counter + '" value="0" class="ajaxfileuploadstatus" rel="' + counter + '" /><p id="uploadmessage_' + counter + '" class="selected-file uploadmessage"></p><p id="loading_' + counter + '" class="selected-file error loading"></p><p id="imagemessage_' + counter + '" class="selected-file" title=""></p><div class="img-thumnail-ctn"><img src="' + data + '" class="img-rounded"  alt="Image for Profile" width="100" height="100"><span class="img-delete">x</span></div></div>';
        wrap.append(imgCtn).css("border", "none");
        $('#uploadimagemessage').html('');
        ajaxuploadimage(file, counter, imagetypearr);
    }

    companyPhoto.init();
    uploadCsv.init();
    settingPhoto.init();
    managePhoto.init();
    manageUplaod.init();
})();

function ajaxuploadimage(file, counter, imagetypearr) {
    var fd = new FormData();
    fd.append('ajaximageoperation[]', file);
    fd.append('fileuploadaction', 'ajaxupload');
    $.ajax({
        type: 'POST',
        url: $('#uploadimageform').attr('action'),
        data: fd,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function () {
            $('#loading_' + counter).html('uploading...').removeClass().addClass("selected-file error");
            $('#ajaxfileuploadstatus_' + counter).val('1');
        },
        success: function (result) {
            //alert('in success'); return false;
            var filetype = result['type'];
            var filetypefound = false;
            var fileuploaderror = parseInt(result['error']);
            if (fileuploaderror === 0) {
                for (var i in imagetypearr) {
                    if (imagetypearr[i] == filetype) {
                        filetypefound = true;
                    }
                }
                if (filetypefound === true) {
                    var filename = result['name'];
                    $('#fileuploadmode').val('ajaxupload');
                    $('#ajaxfilepath_' + counter).val(result['tmp_name']);
                    $('#ajaxfilename_' + counter).val(filename);
                    $('#ajaxfilemimetype_' + counter).val(filetype);
                    $('#ajaxfilesize_' + counter).val(result['size']);
                    $('#ajaxfilenewname_' + counter).val(result['newname']);
                    $('#ajaxfileuploaderror_' + counter).val(fileuploaderror);
                    $('#imagemessage_' + counter).attr('title', filename);
                    var str = new String(filename);
                    if (str.length > 15) {
                        $('#imagemessage_' + counter).html($.trim(str.substring(0, 12)) + '...').removeClass().addClass("selected-file success");
                    } else {
                        $('#imagemessage_' + counter).html(filename).removeClass().addClass("selected-file success");
                    }

                    $('#ajaxfileuploadstatus_' + counter).val('2');
                    $('#loading_' + counter).html('').removeClass().addClass("selected-file success");
                    $('#uploadimagemessage').html('');
                } else {
                    $('#ajaxfileuploadstatus_' + counter).val('0');
                    $('#uploadimagemessage').html('Please upload valid image').removeClass().addClass("selected-file error");
                }
            } else {
                $('#ajaxfileuploadstatus_' + counter).val('0');
                $('#uploadimagemessage').html('There is some problem while uploading file. Please try again after some time').removeClass().addClass("selected-file error");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $('#ajaxfileuploadstatus_' + counter).val('0');
            $('#uploadimagemessage').html('There is some problem while uploading file. Please try again after some time').removeClass().addClass("selected-file error");
            // Here the responseText contains the file content before the correct json object
        }
    });
}

function setUploadAction(source) {
    $('#csvmessage').html('');
    $('#loading').html('');
    if (source == 'createuserbulk') {
        var action = $('#createuserbulkaction').val();
        $('#uploadcsvform').attr('action', action);
        $('#downloadteamsample').hide();
        $('#downloadusersample').show();
    } else if (source == 'createteambulk') {
        var action = $('#createteambulkaction').val();
        $('#uploadcsvform').attr('action', action);
        $('#downloadteamsample').show();
        $('#downloadusersample').hide();
    }
}

function ajaxfileupload(file) {
    var fd = new FormData();
    fd.append('ajaxcsvoperation[]', file);
    fd.append('fileuploadaction', 'ajaxupload');
    $.ajax({
        type: 'POST',
        url: $('#uploadcsvform').attr('action'),
        data: fd,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function () {
            $('#loading').html('uploading...').removeClass().addClass("selected-file error");
            $('#ajaxfileuploadstatus').val('1');
        },
        success: function (result) {
            var filetype = result['type'];
            var fileuploaderror = parseInt(result['error']);
            if (fileuploaderror === 0) {
                if (filetype == 'text/csv') {
                    var filename = result['name'];
                    $('#fileuploadmode').val('ajaxupload');
                    $('#ajaxfilepath').val(result['tmp_name']);
                    $('#ajaxfilename').val(filename);
                    $('#ajaxfilemimetype').val(filetype);
                    $('#ajaxfilesize').val(result['size']);
                    $('#ajaxfilenewname').val(result['newname']);
                    $('#csvmessage').html(filename);
                    $('#ajaxfileuploadstatus').val('2');
                    $('#loading').html('Ready to Submit').removeClass().addClass("selected-file success");
                } else {
                    $('#ajaxfileuploadstatus').val('0');
                    $('#csvmessage').html('Please upload valid CSV file').removeClass().addClass("selected-file error");
                }
            } else {
                $('#ajaxfileuploadstatus').val('0');
                $('#csvmessage').html('There is some problem while uploading file. Please try again after some time').removeClass().addClass("selected-file error");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $('#ajaxfileuploadstatus').val('0');
            $('#csvmessage').html('There is some problem while uploading file. Please try again after some time').removeClass().addClass("selected-file error");
            // Here the responseText contains the file content before the correct json object
        }
    });
}

function deleteElement(ele, keyID, eleType) {
    if (eleType == 'tmp') {
        $('body').find('#' + ele + '_' + keyID).remove();
        $('#pageMessageDiv').html('Successfully removed from the list').removeClass().addClass("selected-file success");
    } else {
        var deleteuserstatus = confirm("Do you want to delete it?");
        if (deleteuserstatus === true) {
            $.ajax({
                url: 'delete-appimage',
                method: 'POST',
                data: {'id': keyID},
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                beforeSend: function () {
                    $('#pageMessageDiv').html('processing...').removeClass().addClass("error");
                },
                success: function (result) {
                    $('#limessage').html('');
                    var deletestatus = parseInt(result);
                    if (deletestatus === 1) {
                        if (eleType == 'id') {
                            var ullen = parseInt($('#manage-img-list li').length);
                            if (ullen > 1) {
                                $('body').find('#' + ele + '_' + keyID).remove();
                                $('#pageMessageDiv').html('Successfully removed').removeClass().addClass("selected-file success");
                            } else {
                                $('#manage-img-list').remove();
                                $('#pageMessageDiv').html('Successfully removed').removeClass().addClass("selected-file success");
                                var noteamdiv = '<div id="noteamdiv" class="dash-empty-ctn noteamdiv" style="z-index:2;position:relative"><div class="display-table text-center"><span class="display-cell dash-empty-text">No image appear to be here yet. <a style="cursor:pointer;" data-toggle="modal" data-target="#uploadImageModal" id="uploadimage-add" class="uploadimage">Click here</a> to upload images</span></div></div>';
                                $('#pageMessageDiv').after(noteamdiv);
                            }
                        }
                        var imagelength = $('#manage-img-list li').length;
                        if (imagelength <= maximagecount) {
                            $('#uploadimage').prop("disabled", false);
                        } else {
                            $('#uploadimage').prop("disabled", true);
                        }

                        var imagelength = $('#manage-img-list li').length;
                        if (imagelength === 0) {
                            $('#uploadimagesubmit').prop("disabled", true);
                        }
                    } else {
                        $('#pageMessageDiv').html('Error while deleting. Please try again after some time.').removeClass().addClass("selected-file error");
                    }
                }
            });

        }
    }
    $('#uploadimagemessage').html('');
    var imagelength = $('#manage-img-list li').length;
    if (imagelength < maximagecount) {
        $('#uploadimage').prop("disabled", false);
    }
}

function getFileCount() {
    var popupLen = $("#manage-popup-img-wrapper img").length;
    var imgLen = $("#manage-img-list > li").length;
    return filecount = popupLen + imgLen;
}
