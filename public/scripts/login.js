//LOGIN PAGE VALIDATION
$(document).ready(function () {
    $('#loginForm').submit(function () {
        var errorFound = true;
        var email = $('#email').val();
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        var passwordTmp = $('#password').val();
        var password = new String(passwordTmp);
        if (email == '') {
            $('#email_error').html('Please enter email');
            var errorFound = true;
        } else if (!emailReg.test(email)) {
            $('#email_error').html('Please enter valid email');
            var errorFound = true;
        } else {
            $('#email_error').html('');
            var errorFound = false;
        }

        if (password == '') {
            $('#password_error').html('Please enter password');
            var errorFound = true;
        } else if (password.length < 6) {
            $('#password_error').html('Passwords must be at least six characters');
            var errorFound = true;
        } else {
            $('#email_error').html('');
            var errorFound = false;
        }

        if (errorFound === false) {
            return true;
        } else {
            return false;
        }
    });
});