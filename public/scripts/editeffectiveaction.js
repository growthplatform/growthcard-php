//EDIT-EFFECTIVE-ACTION
$(document).ready(function () {
    $("#editeffectiveAction").on('submit', (function (e) {
        e.preventDefault();
        $.ajax({
            url: 'edit-effective-action',
            method: 'POST',
            data: $(this).serialize(),
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                if (response.statusCode == 200) {
                    var message = "Effective Action Update successfully.";
                    $('.new-window').fadeIn(300)
                            .find('p')
                            .text(message)
                            .css('color', 'green');
                    setTimeout("location.reload(true);", 800);
                } else {
                    alert("There might be some error occured.");
                }
            },
            error: function (response) {
                var error = jQuery.parseJSON(response.responseText);
                if (error.statusCode == 412) {
                    var message = error.message;
                    $('.new-window').fadeIn(300)
                            .find('p')
                            .text(message)
                            .css('color', 'red');

                } else {
                    alert("There might be some error occured.");
                }
            },
        });
    }));
});