<html lang="en">
    <head>
        <title>GrowthCard</title>
        @include('layouts.headerInclude')
    </head>
    <body>
        @include('layouts.indexHeader')
        <main class="page-wrapper">
            <section class="page-heading-ctn clearfix">
                <h3 class="pull-left page-heading">Users</h3>
                <div class="pull-right">
                    @if ($userType === 2)
                    <button class="btn btn-bdr" data-toggle="modal" data-target="#uploadCsvModal" onclick="setUploadAction('createuserbulk');">Bulk Upload</button>
                    @endif
                    <button class="btn btn-primary addnewuser" data-action="#team-creation-ctn" data-toggle="modal" data-target="#createUserModal">Add new User</button>
                </div>
            </section>
            <?php if ($data->isEmpty()) { ?>
                <div class="dash-empty-ctn">
                    <div class="display-table text-center">
                        <span class="display-cell dash-empty-text" id="">No users appear to be here yet.
                            <a href="#" data-action="#team-creation-ctn" data-toggle="modal" data-target="#createUserModal">Add</a> one now right now</span>
                    </div>
                </div>
            <?php } else { ?>
                <section class="container-fluid">
                    <div id="pageMessageDiv" class="text-center">
                        @if (Session::has('createuserbulk_success'))
                        {{ Session::get('createuserbulk_success') }}
                        @elseif (Session::has('createuserbulk_failure'))
                        We found error in {{ Session::get('createuserbulk_failure.errorline') }} <a href="{{ Session::get('createuserbulk_failure.downloadurl') }}">Click here</a> to download error file.
                        @elseif (Session::has('createuserbulk_failure_empty'))
                        {{ Session::get('createuserbulk_failure_empty') }}
                        @elseif (Session::has('fileupload_failure'))
                        {{ Session::get('fileupload_failure.filenotexist') }}
                        @endif
                    </div>
                    <ul class="item-list-ctn item-list-full list-unstyled">
                        <?php
                        foreach ($data as $result) {
                            ?>
                            <li class="clearfix item-list-inner">
                                <ul class="list-unstyled item-row clearfix">
                                    <li class="col-xs-12 col-sm-4 item-info">
                                        <div class="item-right">
                                            <div class="pull-right visible-xs">
                                                <a href="#" onClick="get_edit_user_data(<?php echo $result['user']['id']; ?>)" class="edit"
                                                   ><i class="ic-edit ic-16"></i></a>
                                                <a href="#" onClick="destroy_user(<?php echo $result['user']['id']; ?>)" class="delete"><i class="ic-delete ic-16"></i></a>
                                            </div>
                                            <div class="item-right">
                                                <h5 class="name">
                                                    <span data-toggle="popover" data-image="{!!$result['user']['profile_image']!!}"  data-email="{!! $result['user']['email']!!}" data-days=" <?php
                                                    foreach ($result['user']['days'] as $data) {
                                                        $dayArr = ["1" => "MON", "2" => "TUE", "3" => "WED", "4" => "THU", "5" => "FRI", "6" => "SAT", "7" => "SUN"];
                                                        echo $dayArr[$data->workDay] . ", ";
                                                    }
                                                    ?>"  data-department="{!!$result['userdepartment']['department']['department_name']!!}"data-name="{!!$result['user']['first_name']!!} {!!$result['user']['last_name']!!}" data-designation="{!!$result['designation']['title']!!}" data-company="{!!$result['company']['company_name']!!}" >

                                                        {!!$result['user']['first_name']!!} {!!$result['user']['last_name']!!}
                                                    </span></h5>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="col-md-3 col-sm-2 department hidden-xs">
                                        {!!$result['userdepartment']['department']['department_name']!!}
                                    </li>
                                    <li class="col-sm-4 email hidden-xs">
                                        {!! $result['user']['email']!!}
                                    </li>
                                    <li type="hidden" name ="userId"value="<?php echo $result['user']['id']; ?>">
                                    </li>
                                    <li class="col-md-1 col-xs-2 col-sm-2 hidden-xs">
                                        <div class="pull-right">
                                            <a href="#" onClick="get_edit_user_data(<?php echo $result['user']['id']; ?>)" class="edit hidden-xs"
                                               ><i class="ic-edit ic-16"></i></a>
                                            <a href="#" onClick="destroy_user(<?php echo $result['user']['id']; ?>)" class="delete"><i class="ic-delete ic-16"></i></a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        <?php } ?>
                    </ul>
                </section>
            <?php } ?>
        </main>
        @include('layouts.uploadCsvFile')
        @include('layouts.footer')
        @include('layouts.createUser')
        @include('layouts.editUser')
        @include('layouts.addNewJob')
        @include('layouts.addNewDepartment')
        @include('layouts.footerInclude')
        <script src="{{ URL::to('scripts/userscript.js').'?'.env('JS_VERSION') }}"></script>
    </body>
</html>
