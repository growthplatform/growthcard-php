<html lang="en">
    <head>
        <title>GrowthCard</title>
        @include('layouts.headerInclude')
    </head>
    <body>
        @include('layouts.companyHeader')
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
        <main class="page-wrapper">
            <section class="page-heading-ctn clearfix">
                <h3 class="pull-left page-heading">Companies</h3>
                <div class="pull-right">
                    <button class="btn btn-primary" data-action="#team-creation-ctn" data-toggle="modal" data-target="#createCompanyModal">Create a new Company</button>
                </div>
            </section>
            <section class="container-fluid">
                <ul class="item-list-ctn item-list-hover item-list-full list-unstyled">
                    <?php foreach ($data as $result) { ?>
                        <li class="clearfix item-list-inner  "   onClick="load_company_dashboard(<?php echo $result['companyId']; ?>)">
                            <ul class="list-unstyled item-row clearfix">
                                <li class="col-xs-12 col-sm-4 item-info">
                                    <div class="item-right">
                                        <div class="pull-right visible-xs">
                                            <a href="#" onClick="get_edit_company_data(<?php echo $result['companyId']; ?>, event)" class="edit" ><i class="ic-edit ic-16"></i></a>
                                            <a href="#" onClick="destroy_company(<?php echo $result['companyId']; ?>, event)" class="delete"><i class="ic-delete ic-16"></i></a>
                                        </div>
                                        <div class="item-right">
                                            <h5 class="name"><span data-toggle="" data-name="{!!$result['companyadmin']->first_name!!} {!!$result['companyadmin']->last_name!!}">{!!$result['companyadmin']->first_name!!} {!!$result['companyadmin']->last_name!!}</span></h5>
                                        </div>
                                    </div>
                                </li>
                                <li class="col-md-3 col-sm-2 department hidden-xs">
                                    {!!$result->company_name!!}
                                </li>
                                <li class="col-sm-4 email hidden-xs">
                                    {!!$result['companyadmin']->email!!}
                                </li>
                                <li type="hidden" name ="companyId"  value="<?php echo $result['companyId']; ?>">
                                </li>
                                <li class="col-md-1 col-xs-2 col-sm-2 hidden-xs">
                                    <div class="pull-right">
                                        <a href="#"  onClick="get_edit_company_data(<?php echo $result['companyId']; ?>, event)" class="edit hidden-xs"><i class="ic-edit ic-16"></i></a>
                                        <a href="#" onClick="destroy_company(<?php echo $result['companyId']; ?>, event)" class="delete"><i class="ic-delete ic-16"></i></a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    <?php } ?>
                </ul>
            </section>
        </main>
        @include('layouts.footer')
        @include('layouts.createCompany')
        @include('layouts.editCompany')
        @include('layouts.footerInclude')
        <script src="{{ URL::to('scripts/companyscript.js').'?'.env('JS_VERSION') }}"></script>
        <script>
                                        $(document).ready(function () {
                                            $(document).on('focus', ':input', function () {
                                                $(this).attr('autocomplete', 'off');
                                            });
                                        });
        </script>
    </body>
</html>
