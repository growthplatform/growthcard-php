<?php
header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
header("Pragma: no-cache"); // HTTP 1.0.
header("Expires: 0 "); // Proxies.
?>
<meta name="csrf-token" content="{{ csrf_token() }}">
<header class="navbar navbar-default navbar-fixed-top landing-header" id="site-header">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#site-header-collapse" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar top-bar"></span>
                <span class="icon-bar middle-bar"></span>
                <span class="icon-bar bottom-bar"></span>
            </button>
            @if(Auth::User()->user_type == 1)
            <a class="navbar-brand" href="<?php echo url('/'); ?>">
                <img alt="Brand" src="images/logo.png" class="pull-left" width="55" height="47">
            </a>
            @else
            <a class="navbar-brand" href="<?php echo url('dashboard'); ?>">
                <img alt="Brand" src="images/logo.png" class="pull-left" width="55" height="47">
            </a>
            @endif
            <?php
            $companyId = Session::get('companyId');
            ?>
            @if((Auth::User()->user_type == 1 || Auth::User()->user_type == 2) && isset($companyId) && $companyId!=0 )
            <span class="company-name ellipsis">
                <?php
                if (isset($companyId) && $companyId > 0) {
                    $companyName = \App\Models\Company::select('company_name')->where('id', $companyId)->first();
                }
                if (isset($companyName)) {
                    Session::put('companyName', $companyName->company_name);
                    echo $companyName->company_name;
                }
                ?>
            </span>
            @endif
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="site-header-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="<?php
                if (Session::get('menuid') == 1) {
                    echo 'active';
                }
                ?>"><a href="dashboard">Teams</a></li>
                <li class="<?php
                if (Session::get('menuid') == 2) {
                    echo 'active';
                }
                ?>"><a href="<?php echo url('users'); ?>">Users</a></li>
                <li class="<?php
                if (Session::get('menuid') == 3) {
                    echo 'active';
                }
                ?>"><a href="effective-actions">Effective Actions</a></li>
                @if((int)Auth::User()->user_type !== 1)
                <li class="<?php
                if (Session::get('menuid') == 5) {
                    echo 'active';
                }
                ?>"><a href="manage-image">Manage Images</a></li>
                @endif
                <li class="dropdown profile">
                    <a href="#"  class="dropdown-toggle <?php
                    if (Session::get('menuid') == 4) {
                        echo 'active';
                    }
                    ?>" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <span >{!!Auth::User()->first_name .' '. Auth::User()->last_name!!}<span class="caret"></span></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="setting">Settings</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="logout">Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</header>