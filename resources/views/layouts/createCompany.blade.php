<!-- CREATE USER -->
<div class="modal fade" id="createCompanyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add a new Company</h4>
            </div>
            <form class="clearfix"  id="createCompany" autocomplete="off">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="modal-body">
                    <div class="error-ctn text-center"></div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="text" name = "firstName" class="form-control" placeholder="First Name"  maxlength="50"  required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="text" name = "lastName" class="form-control" placeholder="Last Name" maxlength="50" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input id="email" type="email" name = "email" class="form-control" placeholder="Email" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input id="password"  autocomplete="off" class="form-control password-visibility password-space" type="password" placeholder="password" content='no-cache' name = "password" maxlength="12" required/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="text" name = "companyName" class="form-control" placeholder="Company Name" maxlength="100" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success" >Add Company</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="{{ URL::to('scripts/createcompany.js').'?'.env('JS_VERSION')}}"></script>