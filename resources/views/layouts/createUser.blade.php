<!-- CREATE USER -->
<div class="modal fade" id="createUserModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add a new user</h4>
            </div>
            <div class="modal-body" id="addnewuserdiv">
                <form class="clearfix" id="createUser" novalidate>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="error-ctn text-center createUser_error"></div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group" id="firstNameDiv">
                                <input type="text" class="form-control" name = "firstName" id = "firstName" placeholder="First Name" maxlength="50"  >
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group" id="lastNameDiv">
                                <input type="text" class="form-control" name = "lastName" id = "lastName" placeholder="Last Name" maxlength="50" >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group" id="emailDiv">
                                <input id ="email" type="Email" name = "email" id = "email" class="form-control" placeholder="Email" >
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group" id="passwordDiv">
                                <input id ="password"  autocomplete="off" class="form-control password-visibility password-space" type="password" placeholder="password" name = "password" maxlength="12" />

                            </div>
                        </div>
                    </div>
                    <?php
                    $companyId = Session::get('companyId');
                    $companyName = \App\Models\Company::select('company_name')->where('id', $companyId)->first();
                    ?>
                    <div class="form-group">
                        <input type="text" name = "companyName" class="form-control" value="<?php
                        if (isset($companyName->company_name)) {
                            echo trim($companyName->company_name);
                        } else {
                            echo '';
                        }
                        ?>" readonly>
                    </div>
                    <div class="form-group">
                        <select class="selectpicker" id="create-jobs-list"  name="job" data-width="100%" title="Job title">

                        </select>
                    </div>
                    <div class="form-group">
                        <a href="#" class="create-add-link createNewJob" data-toggle="modal" data-overlay="create-jobs-list" data-target="#newJobModal"><span class="add-sign">+</span> Create a new Job</a>
                    </div>

                    <div class="form-group">
                        <select class="selectpicker" id="create-department-list"  name="department" data-width="100%" title="Department" >

                        </select>
                    </div>
                    <div class="form-group">
                        <a href="#" class="create-add-link addnewdepartment" data-toggle="modal" data-overlay="create-department-list" data-target="#newDepartmentModal"><span class="add-sign">+</span> Add new Department</a>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success" >Add User</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
