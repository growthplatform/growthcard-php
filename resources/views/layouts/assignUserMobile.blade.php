<!-- ADD NEW JOB -->
<div class="modal fade" id="assignUserModal" sourcebutton="" tabindex="-1" role="dialog" aria-labelledby="usertypelabel" teamcounter="0" previousteamcounter="0">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close canceluser"  data-dismiss="modal" aria-label="Close" id="crossclose" areamanager="0"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="usertypelabel" rel="1" managerid="0">Assign Manager</h4>
            </div>
            <div class="modal-body">
                <ul id="assign-user-ul" class="sidebar-ul clearfix">
                    <?php
                    foreach ($data as $key => $result) {
                        $designation = ucwords(strtolower($data[$key]->memberweb[0]->designation['title']));
                        $department = $result->userdepartment['department']['departmentName'];
                        ?>
                        <li class="clearfix assign-item user-data_dev" rel="<?php echo $result->id; ?>" userrole="1" id="userID_<?php echo $result->id; ?>">
                            <div class="pull-left">
                                <input id="mobileusercb_<?php echo $result->id; ?>" class="checkbox-custom mobileusercb" name="mobileusercb" rel="<?php echo $result->id; ?>" type="checkbox" >
                                <label for="mobileusercb_<?php echo $result->id; ?>" class="checkbox-custom-label"></label>
                            </div>
                            <div class="user-right">
                                <a class="user-left">
                                    <img id="profile_image_<?php echo $result->id; ?>" src="<?php echo $profile_image = (trim($result->profile_image) != '') ? $result->profile_image : 'images/user-img2.jpg'; ?>" class="img-circle profile_image" width="35" height="35" alt="">
                                </a>
                                <div class="user-right">
                                    <h5 id="user-name_<?php echo $result->id; ?>" rel="<?php echo $result->id; ?>" class="mr-0 user-name"><?php echo $result->first_name . ' ' . $result->last_name; ?></h5>
                                    <p id="user-profile_<?php echo $result->id; ?>" rel="<?php echo $result->id; ?>" class="mr-0 user-profile"><?php echo $designation; ?></p>
                                    <p id="user-department_<?php echo $result->id; ?>" rel="<?php echo $result->id; ?>" class="mr-0 user-department display-none"><?php echo $department; ?></p>
                                    <p id="user-email_<?php echo $result->id; ?>" rel="<?php echo $result->id; ?>" class="mr-0 user-email display-none"><?php echo $result->email; ?></p>
                                </div>
                            </div>
                        </li>
                    <?php } ?>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" id="innermobilecanceluser" class="btn btn-link canceluser" data-dismiss="modal" areamanager="0">Cancel</button>
                <button type="button" id="innermobileassignUser" class="btn btn-success" areamanager="0" savedmanagerid="0">Assign User</button>
            </div>
        </div>
    </div>
</div>
<script src="{{ URL::to('scripts/jquery-1.10.2.min.js')}}"></script>
