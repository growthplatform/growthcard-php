<!-- ADD NEW JOB -->
<div class="modal fade" id="newDepartmentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add a new Department</h4>
            </div>
            <form class="clearfix" id="createDepartment" data-identify="">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="modal-body">
                    <div class="error-ctn departmentName_error">
                    </div>
                    <div class="form-group ">
                        <input type="text" class="form-control" placeholder="Department name" name="departmentName" id="departmentName">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-success">Create</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="{{ URL::to('scripts/jquery-1.10.2.min.js') }}"></script>
<script src="{{ URL::to('scripts/adddepartment.js').'?'.env('JS_VERSION')}}"></script>