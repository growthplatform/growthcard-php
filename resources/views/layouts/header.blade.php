<?php
header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
header("Pragma: no-cache"); // HTTP 1.0.
header("Expires: 0 "); // Proxies.
?>
<meta name="csrf-token" content="{{ csrf_token() }}">
<header class="navbar navbar-default navbar-fixed-top landing-header" id="site-header">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#site-header-collapse" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar top-bar"></span>
                <span class="icon-bar middle-bar"></span>
                <span class="icon-bar bottom-bar"></span>
            </button>
            <a class="navbar-brand" href="#">
                <img alt="Brand" src="images/logo.png" class="pull-left" width="55" height="47">
            </a>
            @if(Auth::User()->user_type!=1)
            <span class="company-name ellipsis">
            </span>
            @endif  
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="site-header-collapse">
            <ul class="nav navbar-nav navbar-right">
                @if(Auth::User()->user_type!=1)
                <li><a href="#">Teams</a></li>
                <li><a href="users">Users</a></li>
                <li><a href="effective-actions">Effective Actions</a></li>
                @else
                <li class="active"><a href="<?php echo url('/'); ?>">Companies</a></li>
                @endif 
                <li class="dropdown profile">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <span>{!!Auth::User()->first_name .' '. Auth::User()->last_name!!}</span>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</header>
