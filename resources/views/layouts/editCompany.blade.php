<!-- EDIT COMPANY -->
<div class="modal fade" id="editCompanyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Company</h4>
            </div>
            <form id="editCompany" class="clearfix">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="modal-body">
                    <div class="error-ctn text-center"></div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="text" name = "firstName"  class="form-control"  maxlength="50"  required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="text" name = "lastName" class="form-control" maxlength="50" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="email" name = "email" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input   autocomplete="off" class="form-control password-visibility password-space" type="password" placeholder="password" name = "password" maxlength="12"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="text" name = "companyName" class="form-control" maxlength="100" required>
                    </div>
                    <div class="form-group">
                        <input type="hidden" name = "companyId" class="form-control" >
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success" >Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
</script>