<?php /* Some Common JS includes */ ?>
<script src="{{ URL::to('scripts/jquery-1.10.2.min.js') }}"></script>
<script src="{{ URL::to('scripts/jquery-migrate-1.2.1.min.js') }}"></script>
<script src="{{ URL::to('scripts/jquery.validate.min.js') }}"></script>
<script src="{{ URL::to('scripts/jquery-ui.min.js') }}"></script>
<script src="{{ URL::to('scripts/bootstrap.min.js') }}"></script>
<script src="{{ URL::to('scripts/load-image.all.min.js') }}"></script>
<script src="{{ URL::to('scripts/bootstrap-select.min.js') }}"></script>
<script src="{{ URL::to('scripts/jquery.mCustomScrollbar.concat.min.js') }}"></script>
<script src="{{ URL::to('scripts/password.js') }}"></script>
<script src="{{ URL::to('scripts/avatar.js') }}"></script>
<script src="{{ URL::to('scripts/script.js') }}"></script>
<script src="{{ URL::to('scripts/commonscript.js') }}"></script>
