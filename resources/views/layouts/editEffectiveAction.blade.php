<div class="modal fade effectiveModal" id="editEffectiveModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Effective Action</h4>
            </div>
            <form class="clearfix" id="editeffectiveAction">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="id" >
                <input type="hidden" name="jobid" >
                <div class="modal-body">
                    <div class="form-group new-window">
                        <p></p>
                        <input type="text" class="form-control" placeholder="Effective action name" name="effectiveAction">
                    </div>
                    <div class="form-group mr-b20">
                        <input type="text" class="form-control" placeholder="Job title" name="Jobtitle" readonly>    
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="{{ URL::to('scripts/editeffectiveaction.js').'?'.env('JS_VERSION')}}"></script>