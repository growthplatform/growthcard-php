<!-- EDIT JOB -->
<div class="modal fade" id="editJobModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Job</h4>
            </div>
            <form class="clearfix" id="editJob">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="id">
                <div class="modal-body new-window">
                    <p class="jobName_error"></p>
                    <div class="form-group">
                        <input type="text" class="form-control"  name="jobName">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="{{ URL::to('scripts/jquery-1.10.2.min.js') }}"></script>
<script src="{{ URL::to('scripts/job.js').'?'.env('JS_VERSION')}}"></script>
