<div class="modal fade effectiveModal" id="createEffectiveModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">New Effective Action</h4>
            </div>
            <form class="clearfix " id="effectiveAction">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="modal-body ">
                    <div class="error-ctn">
                    </div>
                    <div class="form-group ">
                        <input type="text" class="form-control" placeholder="Effective action name" name="effectiveAction">
                    </div>
                    <div class="form-group mr-b20">
                        <select class="selectpicker" data-width="100%" id="create-jobs-list"  name="Jobtitle" title="Job title">
                        </select>
                    </div>
                    <p class="new-job-link" >Job is not listed? <a href="#"  data-toggle="modal" data-target="#newJobModal">Create one now.</a></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-success">Create</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="{{ URL::to('scripts/createeffectiveaction.js').'?'.env('JS_VERSION')}}"></script>