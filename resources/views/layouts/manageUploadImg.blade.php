<!-- UPLOAD CSV FILE -->
<div class="modal fade" id="uploadImageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form role="form" action="{{url('upload-image-bulk')}}" enctype="multipart/form-data" id="uploadimageform"  method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="uploadimagebulkaction" id="uploadimagebulkaction" value="{{url('upload-image-bulk')}}" />
                <input type="hidden" name="fileuploadmode" id="fileuploadmode" value="" />
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="uploadimageformcross"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Upload Images</h4>
                </div>
                <div class="modal-body">
                    <span id="uploadimagemessage" class="selected-file"></span>
                    <div id="upload-image" class="avatar-photo-wrap avatar-drop drop-area">
                        <p id="uploadedImageFile">
                            Drag & Drop Images here </br>or
                            <span class="btn btn-file">Browse Images
                                <input type="file" multiple class="manage-upload-img" name="uploadimagefile[]" id="uploadimagefile" >
                            </span>
                            (Max: 5 MB)
                        </p>
                    </div>
                    <div id="manage-popup-img-wrapper" class="clearfix">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>
                    <input type="button" class="btn btn-success" id="useimagefile" value="Use Images" />
                </div>
            </form>
        </div>
    </div>
</div>
