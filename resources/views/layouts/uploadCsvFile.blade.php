<!-- UPLOAD CSV FILE -->
<div class="modal fade" id="uploadCsvModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form role="form" action="{{url('create-team-bulk')}}" enctype="multipart/form-data" id="uploadcsvform"  method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="createuserbulkaction" id="createuserbulkaction" value="{{url('create-user-bulk')}}" />
                <input type="hidden" name="createteambulkaction" id="createteambulkaction" value="{{url('create-team-bulk')}}" />
                <input type="hidden" name="fileuploadmode" id="fileuploadmode" value="" />
                <input type="hidden" name="ajaxfilepath" id="ajaxfilepath" value="" />
                <input type="hidden" name="ajaxfilename" id="ajaxfilename" value="" />
                <input type="hidden" name="ajaxfilemimetype" id="ajaxfilemimetype" value="" />
                <input type="hidden" name="ajaxfilenewname" id="ajaxfilenewname" value="" />
                <input type="hidden" name="ajaxfilesize" id="ajaxfilesize" value="" />
                <input type="hidden" name="ajaxfileuploadstatus" id="ajaxfileuploadstatus" value="0" />
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <span id="csvmessage" class="selected-file"></span>
                    <span id="loading" class="selected-file error"></span>
                    <div id="upload-csv" class="avatar-photo-wrap avatar-drop">
                        <p id="uploadedCSVFile">
                            Drag & Drop .csv file here </br>or
                            <span class="btn btn-file">Browse File
                                <input type="file" class="csv-upload-file" name="csvcreateteam" id="csvcreateteam">
                            </span>
                            (Max: 2 MB)
                        </p>
                    </div>
                </div>
                <div class="modal-footer">
                    <a id="downloadteamsample" style="display:none;" href="download/uploadteambulksample/upload_team_sample.csv" class="btn btn-primary pull-left">Download Sample CSV</a>
                    <a id="downloadusersample" style="display:none;" href="download/uploaduserbulksample/create_user_sample.csv" class="btn btn-primary pull-left">Download Sample CSV</a>
                    <button type="submit" id="submitcsvfile" class="btn btn-success">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
