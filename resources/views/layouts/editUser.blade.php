<!-- Edit USER -->
<div class="modal fade" id="editUserModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit user</h4>
            </div>
            <div class="modal-body" >
                <form class="clearfix"id="editUser">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="error-ctn text-center"></div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="text" class="form-control" name = "firstName"  maxlength="50"  required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="text"  name = "lastName" class="form-control" maxlength="50" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="email" name = "email" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input   autocomplete="off" class="form-control password-visibility password-space" type="password" placeholder="password" name = "password" maxlength="12" required/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="text" name = "companyName" class="form-control" maxlength="100" required readonly>
                    </div>
                    <div class="form-group">
                        <select class="selectpicker" id="edit-jobs-list"  name="job" data-width="100%" >
                        </select>
                    </div>
                    <div class="form-group">
                        <a href="#" class="create-add-link" data-toggle="modal" data-overlay="edit-jobs-list" data-target="#newJobModal"><span class="add-sign">+</span> Create a new Job</a>
                    </div>
                    <div class="form-group">
                        <select class="selectpicker" id="edit-department-list"  name="department" data-width="100%">
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="hidden" name = "userId" class="form-control" >
                    </div>
                    <div class="form-group">
                        <a href="#" class="create-add-link" data-toggle="modal" data-overlay="edit-department-list" data-target="#newDepartmentModal"><span class="add-sign">+</span> Add new Department</a>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success" >Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>