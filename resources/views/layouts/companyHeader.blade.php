<?php /* Header html of page */ ?>
<header class="navbar navbar-default navbar-fixed-top landing-header" id="site-header">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#site-header-collapse" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar top-bar"></span>
                <span class="icon-bar middle-bar"></span>
                <span class="icon-bar bottom-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo url('/'); ?>">
                <img alt="Brand" src="images/logo.png" class="pull-left" width="55" height="47">
            </a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="site-header-collapse">
            <ul class="nav navbar-nav navbar-right">
                @if(Auth::User()->user_type==1)
                <li><a href="<?php echo url('/'); ?>">Companies</a></li>
                <li class="<?php
                if (Session::get('menuid') == 5) {
                    echo 'active';
                }
                ?>"><a href="manage-image">Manage Images</a></li>
                @else
                <li class="<?php
                if (Session::get('menuid') == 1) {
                    echo 'active';
                }
                ?>"><a href="users">Teams</a></li>
                <li class="<?php
                if (Session::get('menuid') == 2) {
                    echo 'active';
                }
                ?>"><a href="users">Users</a></li>
                <li class="<?php
                if (Session::get('menuid') == 3) {
                    echo 'active';
                }
                ?>"><a href="effective-actions">Effective Actions</a></li>
                <li class="<?php
                if (Session::get('menuid') == 5) {
                    echo 'active';
                }
                ?>"><a href="manage-image">Manage Images</a></li>
                @endif
                <li class="dropdown profile">
                    <a href="#"  class="dropdown-toggle <?php
                    if (Session::get('menuid') == 4) {
                        echo 'active';
                    }
                    ?>" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <span >{!!Auth::User()->first_name .' '. Auth::User()->last_name!!}<span class="caret"></span></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="setting">Settings</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="logout">Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</header>