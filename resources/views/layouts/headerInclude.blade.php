<?php
header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
header("Pragma: no-cache"); // HTTP 1.0.
header("Expires: 0 "); // Proxies.
?>

<meta charset="utf-8">
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta name="author" content="">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="shortcut icon" href="{{ asset('images/favicon.png')}}">
<link rel="apple-touch-icon" href="{{ asset('images/favicon.png')}}">

<!--FONTS-->
<link href='https://fonts.googleapis.com/css?family=Rubik:300,400,500,700,900' rel='stylesheet' type='text/css'>
<link type='text/css' rel='stylesheet' href="{{ URL::to('styles/font-awesome.min.css') }}" >
<link type="text/css" rel="stylesheet" href="{{ URL::to('styles/jquery-ui.css') }}">
<link type="text/css" rel="stylesheet" href="{{ URL::to('styles/bootstrap.css') }}">
<link type="text/css" rel="stylesheet" href="{{ URL::to('styles/bootstrap-select.css') }}">
<link type="text/css" rel="stylesheet" href="{{ URL::to('styles/jquery.mCustomScrollbar.css') }}">
<link type="text/css" rel="stylesheet" href="{{ URL::to('styles/common-layout.css') }}">
<link type="text/css" rel="stylesheet" href="{{ URL::to('styles/style.css') }}">
<link type="text/css" rel="stylesheet" href="{{ URL::to('styles/style-responsive.css') }}">

