<!DOCTYPE html>
<html lang="en">
    <head>
        <title>GrowthCard</title>
        @include('layouts.headerInclude')
    </head>
    <body>
        @include('layouts.companyHeader')
        <main class="page-wrapper" id="manage-wrapper">
            <form role="form" action="{{url('save-upload-image')}}" enctype="multipart/form-data" id="uploadimage_form" name="uploadimage_form" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="destination_path" id="destination_path" value="<?php echo $destination_path; ?>" />
                <section class="page-heading-ctn clearfix">
                    <h3 class="pull-left page-heading">Manage Images</h3>
                    <div class="pull-right">
                        <?php
                        $imageexist = false;
                        if (isset($imagedataarr) && (count($imagedataarr) > 0)) {
                            $imageexist = true;
                        }
                        ?>
                        <button type="button" id="uploadimagesubmit" name="uploadimagesubmit" class="btn btn-primary" onClick="manageEdit(this)" <?php
                        if ($imageexist !== true) {
                            echo "disabled";
                        }
                        ?>>Edit</button>
                        <input type="button" class="btn btn-primary uploadimage" data-toggle="modal" id="uploadimage" value="Upload Images" />
                    </div>
                </section>
                <div id="pageMessageDiv" align="center" class="success">
                    @if (Session::has('imageupload_success'))
                    {{ Session::get('imageupload_success') }}
                    @endif
                </div>
                <?php
                if (!isset($imagedataarr) || (isset($imagedataarr) && count($imagedataarr) === 0)) {
                    ?>
                    <div id="noteamdiv" class="dash-empty-ctn noteamdiv" class="success">
                        <div class="display-table text-center">
                            <span class="display-cell dash-empty-text">No image appear to be here yet.
                                <a href="javascript:void(0)" data-toggle="modal" id="uploadimage-add" class="uploadimage">Click here</a> to upload images</span>
                        </div>
                    </div>
                    <section class="container-fluid">
                    <?php } else {
                        ?>
                        <ul class="list-unstyled col-xs-12" id="manage-img-list">
                            <?php foreach ($imagedataarr as $key => $dataArr) { ?>
                                <li id="imageli_<?php echo $dataArr['id']; ?>" class="imageli">
                                    <p id="limessage_<?php echo $dataArr['id']; ?>" class="limessage"></p>
                                    <div class="manage-img-ctn" style="background-image:url(<?php echo $dataArr['name']; ?>)">
                                        <span class="btn btn-file">Change Image
                                            <input type="file" class="manage-img-upload uploadedimage" name="uploadedimage[<?php echo $dataArr['id']; ?>]" id="uploadedimage_<?php echo $dataArr['id']; ?>" rel="<?php echo $dataArr['id']; ?>">
                                        </span>
                                    </div>
                                    <div class="manage-form-ctn">
                                        <div class="section-one">
                                            <div class="form-group">
                                                <label>Caption*</label>
                                                <input type="text" class="form-control caption" value="<?php echo $dataArr['caption']; ?>" id="caption_<?php echo $dataArr['id']; ?>" name="caption[<?php echo $dataArr['id']; ?>]" rel="<?php echo $dataArr['id']; ?>" rel2="old" maxlength="100" readonly>
                                                <p id="caption_error_<?php echo $dataArr['id']; ?>" name="caption_error_<?php echo $dataArr['id']; ?>" class="caption_error"></p>
                                            </div>
                                            <div class="form-group">
                                                <label>Message*</label>
                                                <textarea class="form-control message" maxlength="" rows="3" id="message_<?php echo $dataArr['id']; ?>" name="message[<?php echo $dataArr['id']; ?>]" rel="<?php echo $dataArr['id']; ?>" rel2="old" style="resize: none;" readonly ><?php echo $dataArr['message']; ?></textarea>
                                                <span id="messagecount_<?php echo $dataArr['id']; ?>" class="messagecount" style="display:none;"></span>
                                                <p id="message_error_<?php echo $dataArr['id']; ?>" name="message_error_<?php echo $dataArr['id']; ?>" class="message_error"></p>
                                            </div>
                                        </div>
                                        <div class="section-second">
                                            <a href="javascript:void(0)" class="manage-row-delete deleteli" id="deleteli_<?php echo $dataArr['id']; ?>" rel="<?php echo $dataArr['id']; ?>" onclick="deleteElement('imageli', <?php echo $dataArr['id']; ?>, 'id');"><i class="ic-delete ic-16"></i></a>
                                        </div>
                                    </div>
                                </li>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                </section>
            </form>
        </main>
        @include('layouts.footer')
        @include('layouts.manageUploadImg')
        @include('layouts.footerInclude')
        <script src="{{ URL::to('scripts/manageimage.js').'?'.env('JS_VERSION') }}"></script>
    </body>
</html>
