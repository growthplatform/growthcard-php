<html lang="en">
    <head>
        <title>Setting</title>
        @include('layouts.headerInclude')
    </head>
    <body>
        @include('layouts.settingHeader')
        <main class="setting-wrapper">
            <form id ="setting">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <input type="email" class="form-control" value='<?php echo $data->email ?>' readonly>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control"  value='<?php echo $data->first_name . ' ' . $data->last_name ?>'readonly>
                </div>
                <h4 class="change-pass-heading">Change Password</h4>
                <div class="form-group new-window">
                    <p> </p>
                    <input type="password" id ="oldPassword" class="form-control password-visibility password-space "  placeholder="Old Password" maxlength="12" required>
                </div>
                <div class="form-group">
                    <input type="password" id ="newPassword" class="form-control password-visibility password-space"  placeholder="New Password" maxlength="12" required>
                </div>
                <div class="form-group mr-b30">
                    <input type="password"  id ="retypePassword" class="form-control password-visibility password-space"  placeholder="Re-type New Password" maxlength="12" required>
                </div>
                <div class="form-group clearfix mr-b20">
                    <button type="submit" class="btn btn-success col-xs-4 col-xs-offset-4">Save</button>
                </div>
            </form>
        </main>
        @include('layouts.footerInclude')
        <script src="{{ URL::to('scripts/settings.js') .'?'.env('JS_VERSION')}}"></script>
    </body>
</html>

