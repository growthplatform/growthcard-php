<!DOCTYPE html>
<html lang="en">
    <head>
        <title>GrowthCard</title>
        @include('layouts.headerInclude')
    </head>
    <body>
        @include('layouts.indexHeader')
        <main class="page-wrapper">
            <section class="page-heading-ctn clearfix">
                <h3 class="pull-left page-heading">Effective Actions</h3>
                <div class="pull-right">
                    <button class="btn btn-bdr createNewJob" onclick="openJobPopup(1);" >+ Create a new Job</button>
                    <button class="btn btn-primary" data-action="#team-creation-ctn" data-toggle="modal" data-target="#createEffectiveModal">New Effective Action</button>
                </div>
            </section>
            <section class="container-fluid">
                <?php if ($data->isEmpty()) { ?>
                    <div class="dash-empty-ctn">
                        <div class="display-table text-center">
                            <span class="display-cell dash-empty-text">No effective actions appear to be here yet.
                                <a href="#" data-action="#team-creation-ctn" data-toggle="modal" data-target="#createEffectiveModal">Add</a> one now right now</span>
                        </div>
                    </div>
                <?php } else { ?>
                    <div id="effective-list-ctn">
                        <?php foreach ($data as $job) { ?>
                            <div class="pos-r effective-list-wrapper">
                                <ul class="effective-list">
                                    <li class="effective-heading-ctn clearfix">
                                        <h4 class="heading"><?php echo $job->title ?></h4>
                                        <div class="effective-right">
                                            <div class="pull-right action-col">
                                                <button class="btn btn-link pd-0 editTheJob" data-target="#editJobModal" onClick="get_edit_job(<?php echo "$job->id" . "," . "'$job->title'"; ?>)">Edit Job</button>
                                                <button class="btn btn-link pd-0" data-action="#team-creation-ctn" data-toggle="modal"  onClick="get_add_effective_action(<?php echo "$job->id" . "," . "'$job->title'"; ?>)" >Add Effective Action</button>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <ul class="item-list-ctn list-unstyled mr-0 item-no-img">
                                    <?php foreach ($job->effectiveAction as $result) { ?>
                                        <li class="clearfix item-list-inner">
                                            <ul class="list-unstyled item-row clearfix">
                                                <li class="col-xs-12 item-info">
                                                    <p class="pull-left mr-0"><?php echo $result->effective_action_name; ?></p>
                                                    <div class="pull-right ">
                                                        <a href="#" class="edit" data-action="#team-creation-ctn"  onClick="get_edit_effective_action(<?php echo "$job->id" . "," . "$result->id" . "," . "'$result->effective_action_name'" . "," . "'$job->title'"; ?>)"><i class="ic-edit ic-16"></i></a>
                                                        <a href="#" onClick="deletes(<?php echo $result->id; ?>)" class="delete"><i class="ic-delete ic-16"></i></a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
            </section>
        </main>
        @include('layouts.footer')
        @include('layouts.addNewJob')
        @include('layouts.editNewJob')
        @include('layouts.createEffectiveAction')
        @include('layouts.editEffectiveAction')
        @include('layouts.addEffectiveAction')
        @include('layouts.footerInclude')
        <script src="{{ URL::to('scripts/effectiveaction.js') .'?'.env('JS_VERSION')}}"></script>
    </body>
</html>

