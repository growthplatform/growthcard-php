<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>GrowthCard</title>
        @include('layouts.headerInclude')
    </head>
    <body class="pd-0">
        <section class="container auth-center-ctn">
            <div class="auth-wrapper">
                <header class="text-center">
                    <p><img src="{{ url('images/logo.png') }}" width="190" height="161"></p>
                </header>
                <form class="auth-form-ctn clearfix" role="form" method="POST" action="{{ url('/password/reset') }}">                        
                    <input type="hidden" name="token" value="{{ $token }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <h5 class="auth-form-heading">Reset Password</h5>
                    <div class="auth-form-inner">
                        <div class="form-group mr-b20{{ $errors->has('email') ? ' has-error' : '' }}">
                            <input type="email" class="form-control"  name="email" placeholder="Your Email" value="{{ old('email') }}">
                            @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group mr-b20{{ $errors->has('password') ? ' has-error' : '' }}">
                            <input type="password" class="form-control"   placeholder="New Password" name="password">
                            @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group mr-b20{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <input type="password" class="form-control"  placeholder="Confirm New Password" name="password_confirmation">
                            @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="row">
                            <div class="col-sm-8">
                                <a href="{{ url('login') }}" class="auth-links">Back to Sign in</a>
                            </div>
                            <div class="col-sm-4">
                                <button type="submit" class="btn btn-success btn-block">Reset</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
        @include('layouts.footerInclude')
    </body>
</html>