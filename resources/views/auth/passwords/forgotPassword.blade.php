<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>GrowthCard</title>
        @include('layouts.headerInclude')
    </head>
    <body class="pd-0">
        <section class="container auth-center-ctn">
            <div class="auth-wrapper">
                <header class="text-center">
                    <p><img src="{{ url('images/logo.png') }}" width="190" height="161"></p>
                </header>
                <form class="auth-form-ctn clearfix" role="form" method="POST" action="{{ url('/password/email') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <h5 class="auth-form-heading">Forgot Password</h5>
                    <div class="auth-form-inner">
                        @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                        @endif
                        <div class="form-group mr-b20{{ $errors->has('email') ? ' has-error' : '' }}">
                            <input type="email" class="form-control" name="email" value="{{ old('email') }}"placeholder="Your Email">
                            @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="row">
                            <div class="col-sm-8">
                                <a href="{{ url('login') }}" class="auth-links">Back to Sign in</a>
                            </div>
                            <div class="col-sm-4">
                                <button type="submit" class="btn btn-success btn-block">Reset</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
        @include('layouts.footerInclude')
    </body>
</html>