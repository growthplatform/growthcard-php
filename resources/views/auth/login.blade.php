<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>GrowthCard</title>
        @include('layouts.headerInclude')
    </head>
    <body class="pd-0">
        <section class="container auth-center-ctn">
            <div class="auth-wrapper">
                <header class="text-center">
                    <p><img src="images/logo.png" width="190" height="161"></p>
                </header>
                <form id="loginForm" name="loginForm" class="auth-form-ctn clearfix" role="form" method="POST" action="{{ url('/login') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <h5 class="auth-form-heading">Sign In</h5>
                    <div class="auth-form-inner">
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="form-group">
                                <input name="email" id="email" type="email" class="form-control"  placeholder="Your Email">
                                <span class="help-block error" id="email_error">
                                    @if ($errors->has('email'))
                                    {{ $errors->first('email') }}
                                    @endif
                                </span>
                            </div>
                            <div>
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <div class="form-group mr-b20">
                                        <input type="password" class="form-control"  placeholder="Your Password" name="password" id="password">
                                        <span class="help-block error" id="password_error">
                                            @if ($errors->has('password'))
                                            {{ $errors->first('password') }}
                                            @endif
                                        </span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-8">
                                        <a href="{{ url('/password/reset') }}" class="auth-links">Forgot Password</a>
                                    </div>
                                    <div class="col-xs-4">
                                        <button type="submit" class="btn btn-success btn-block">Sign In</button>
                                    </div>
                                </div>
                            </div>
                            </form>
                        </div>
                        </section>
                        @include('layouts.footerInclude')
                        <script src="{{ URL::to('scripts/login.js').'?'.env('JS_VERSION') }}"></script>
                        </body>
                        </html>
