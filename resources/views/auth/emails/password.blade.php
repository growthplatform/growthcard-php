<p>Hi User,</p>
<p>To reset your password <a href="<?php echo url('password/reset/' . $token) ?>">click here.</a></p>
<p>Sincerely,</p>
<p>GrowthCard Team</p>
