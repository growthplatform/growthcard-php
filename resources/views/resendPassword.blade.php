<p>Dear {{ $name }},</p>
<p>Here are your GrowthCard App account credentials.</p>
<p>Login ID: {{ $email }} <br/>
    New Password: {{ $newpass }}
<p>You can now log in with the above information in the GrowthCard App.</p>
<p>Sincerely,<br/>
    GrowthCard Team</p>