<!DOCTYPE html>
<html lang="en">
    <head>
        <title>GrowthCard</title>
        @include('layouts.headerInclude')
    </head>
    <body>
        @include('layouts.companyHeader')
        <div class="container">
            <section class="error-description row">
                <div class="col-sm-8 col-sm-offset-2">
                    <h1 class="error-title">Oops!</h1>
                    <h3>Looks like there is some technical error.</h3>
                    <h4>Please try again after some time</h4>
                </div>
            </section>
        </div>
        @include('layouts.footer')
        @include('layouts.manageUploadImg')
        @include('layouts.footerInclude')
    </body>
</html>
