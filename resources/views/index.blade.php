<!DOCTYPE html>
<html lang="en">
    <head>
        <title>GrowthCard</title>
        @include('layouts.headerInclude')
    </head>
    <body>
        @include('layouts.indexHeader')
        <main class="dashboard-wrapper page-wrapper team-wrapper">
            <aside class="left-sidebar">
                <div class="sidebar">
                    <div class="mCustomScrollbar sidebar-scroll-ctn">
                        <ul class="sidebar-ul">
                            <?php
                            if (isset($data) && count($data) > 0) {
                                foreach ($data as $key => $result) {
                                    $designation = ucwords(strtolower($data[$key]->memberweb[0]->designation['title']));
                                    $department = $result->userdepartment['department']['departmentName'];
                                    ?>
                                    <li class="clearfix sidebar-item user-data_dev" rel="<?php echo $result->id; ?>" userrole="1" id="userID_<?php echo $result->id; ?>">
                                        <div class="sidebar-content">
                                            <a class="user-left">
                                                <img id="profile_image_<?php echo $result->id; ?>" src="<?php echo $profile_image = (trim($result->profile_image) != '') ? $result->profile_image : 'images/user-img2.jpg'; ?>" class="img-circle profile_image" width="35" height="35" alt="">
                                            </a>
                                            <div class="user-right">
                                                <div class="pull-right user-setting-icon">
                                                    <i class="ic-move ic-16"></i>
                                                </div>
                                                <div class="user-right">
                                                    <h5 id="user-name_<?php echo $result->id; ?>" rel="<?php echo $result->id; ?>" class="mr-0 user-name"><?php echo $result->first_name . ' ' . $result->last_name; ?></h5>
                                                    <p id="user-profile_<?php echo $result->id; ?>" rel="<?php echo $result->id; ?>" class="mr-0 user-profile"> <?php echo $designation; ?></p>
                                                    <p id="user-department_<?php echo $result->id; ?>" rel="<?php echo $result->id; ?>" class="mr-0 user-department"><?php echo $department; ?></p>
                                                    <p id="user-email_<?php echo $result->id; ?>" rel="<?php echo $result->id; ?>" class="mr-0 user-email display-none"><?php echo $result->email; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                    </div>
                    <div class="dash-add-user">
                        <button class="btn btn-success btn-block btn-lg" data-toggle="modal" data-target="#createUserModal">Add New User</button>
                    </div>
                </div>
            </aside>
            <?php
            if (!isset($teamData) || (isset($teamData) && count($teamData) === 0)) {
                ?>
                <div id="noteamdiv" class="dash-empty-ctn noteamdiv">
                    <div class="display-table text-center">
                        <span class="display-cell dash-empty-text">No teams appear to be here yet.
                            <a href="javascript:void(0)" id="addteam" data-action="#team-creation-ctn">Add</a> one right now</span>
                    </div>
                </div>
            <?php } ?>

            <section class="main-content">
                <div class="page-heading-ctn clearfix">
                    <h3 class="pull-left page-heading">Team</h3>
                    <div class="pull-right">
                        @if ($userType === 2)
                        <button id="uploadCsv" class="btn btn-bdr" data-toggle="modal" data-target="#uploadCsvModal" onclick="setUploadAction('createteambulk');">Bulk Upload</button>
                        @endif
                        <a id="mobilecreateTeam" rel="1" class="btn btn-primary visible-xs" data-action="#team-creation-ctn">Create a Team</a>
                        <a id="createTeam" rel="1" class="btn btn-primary hidden-xs" data-action="#team-creation-ctn">Create a Team</a>
                    </div>
                </div>
                <div id="pageMessageDiv" align="center">
                    @if (Session::has('uploadteambulk_success'))
                    {{ Session::get('uploadteambulk_success') }}
                    @elseif (Session::has('uploadteambulk_failure'))
                    We found error in {{ Session::get('uploadteambulk_failure.errorline') }} <a href="{{ Session::get('uploadteambulk_failure.downloadurl') }}">Click here</a> to download error file.
                    @elseif (Session::has('uploadteambulk_failure_empty'))
                    {{ Session::get('uploadteambulk_failure_empty') }}
                    @elseif (Session::has('fileupload_failure'))
                    {{ Session::get('fileupload_failure.filenotexist') }}
                    @endif
                </div>
                <div id="team-list-ctn" class="col-xs-12">
                    <?php if (count($teamData) > 0) { ?>
                        <?php
                        foreach ($teamData as $teamID => $teamresult) {
                            $teammanagerid = $teamresult['user_id'];
                            $subordinatesids = '';
                            if (isset($teamData[$teamID]['subordinates']) && count($teamData[$teamID]['subordinates']) > 0) {
                                $subordinatesids = implode(',', array_keys($teamData[$teamID]['subordinates']));
                            }
                            $fullName = $teamresult['first_name'] . ' ' . $teamresult['last_name'];
                            $designation = $teamresult['designation'];
                            $profile_image = $teamresult['profile_image'];
                            ?>
                            <div class="teams-list-wrapper" id="teams-list-wrapper_<?php echo $teammanagerid; ?>_<?php echo $teamID; ?>">
                                <div class="teamMainDiv_Message" id="teamMainDiv_Message_<?php echo $teammanagerid; ?>_<?php echo $teamID; ?>"></div>
                                <div id="teamMainDiv_<?php echo $teammanagerid; ?>_<?php echo $teamID; ?>" teammanagerid="<?php echo $teammanagerid; ?>" class="pos-r  teamMainDiv_class">
                                    <ul class="teams-list" id="myteamslist_<?php echo $teammanagerid; ?>_<?php echo $teamID; ?>" rel="<?php echo $subordinatesids; ?>">
                                        <li id="team-manager_<?php echo $teammanagerid; ?>_<?php echo $teamID; ?>" rel="<?php echo $teammanagerid; ?>" class="team-manager team-managerclass_<?php echo $teammanagerid; ?>">
                                            <div class="pull-right">
                                                <div class="pull-right action-col visible-xs">
                                                    <div class="btn-group">
                                                        <button id="actiondropdown_<?php echo $teammanagerid; ?>_<?php echo $teamID; ?>" type="button" class="btn btn-primary dropdown-toggle mobile-assign-user" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" managerid="<?php echo $teammanagerid; ?>">Action <span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="javascript:void(0)" id="changemanager_<?php echo $teammanagerid; ?>_<?php echo $teamID; ?>" rel="2" teamcounter="<?php echo $teamID; ?>" onclick="changemanager(<?php echo $teammanagerid; ?>, $(this).attr('teamid'), <?php echo $teammanagerid; ?>, <?php echo $teamID; ?>);" teamid="<?php echo $teamID; ?>" savedmanagerid="<?php echo $teammanagerid; ?>">Change Manager</a></li>
                                                            <li><a href="javascript:void(0)" teamid="<?php echo $teamID; ?>" teamcounter="<?php echo $teamID; ?>" onclick="deleteteam(<?php echo $teammanagerid; ?>, <?php echo $teamID; ?>, <?php echo $teamID; ?>);" id="deleteteam_<?php echo $teammanagerid; ?>_<?php echo $teamID; ?>">Delete Team</a></li>
                                                            <li><a data-target="#assignUserModal" data-toggle="modal" href="javascript:void(0)" id="mobileassignUser_<?php echo $teammanagerid; ?>_<?php echo $teamID; ?>" teamcounter="<?php echo $teamID; ?>" rel="<?php echo $teammanagerid; ?>" onclick="disableCreateNewTeam(<?php echo $teammanagerid; ?>);" class="mobileassignUser" data-keyboard="false">Assign User</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="user-right">
                                                <a class="user-left"><img width="35" height="35" id="profile_image_<?php echo $teammanagerid; ?>_<?php echo $teamID; ?>" src="<?php echo $profile_image = (trim($teamresult['profile_image']) != '') ? $profile_image : 'images/user-img2.jpg'; ?>" class="img-circle profile_image" alt=""></a>
                                                <div class="user-right">
                                                    <div class="pull-right action-col">
                                                        <button class="btn btn-link  hidden-xs" id="changemanager_<?php echo $teammanagerid; ?>_<?php echo $teamID; ?>" rel="2" teamcounter="<?php echo $teamID; ?>" onclick="changemanager(<?php echo $teammanagerid; ?>, $(this).attr('teamid'), <?php echo $teammanagerid; ?>, <?php echo $teamID; ?>);" teamid="<?php echo $teamID; ?>" savedmanagerid="<?php echo $teammanagerid; ?>">Change Manager</button>
                                                        <button class="btn btn-link  hidden-xs" teamid="<?php echo $teamID; ?>" teamcounter="<?php echo $teamID; ?>" onclick="deleteteam(<?php echo $teammanagerid; ?>, <?php echo $teamID; ?>, <?php echo $teamID; ?>);" id="deleteteam_<?php echo $teammanagerid; ?>_<?php echo $teamID; ?>">Delete Team</button>
                                                        <button id="assignUser_<?php echo $teammanagerid; ?>_<?php echo $teamID; ?>" rel="<?php echo $teammanagerid; ?>" teamcounter= "<?php echo $teamID; ?>" onclick="disableCreateNewTeam(<?php echo $teammanagerid; ?>);" class="btn btn-primary assign-user user hidden-xs" data-action="#user-creation-ctn">Assign User</button>
                                                        <button id="doneTeam_<?php echo $teammanagerid; ?>_<?php echo $teamID; ?>" rel="<?php echo $teammanagerid; ?>" teamcounter= "<?php echo $teamID; ?>" rel2="<?php echo $teamID; ?>" newteam="0" data-action="#user-creation-ctn" class="btn done-user btn-success done" onclick="enableCreateNewTeam(<?php echo $teammanagerid; ?>, <?php echo $teamID; ?>);" style="display: none;" oldmanagerid="0" savedmanagerid="<?php echo $teammanagerid; ?>">Done</button>
                                                    </div>
                                                    <div class="user-right">
                                                        <h5 class="name"><?php echo $fullName; ?></h5>
                                                        <p class="designation"><?php echo $designation; ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <?php if (isset($teamresult['subordinates']) && count($teamresult['subordinates']) > 0) { ?>
                                        <ul id="managersubordinate_<?php echo $teammanagerid; ?>_<?php echo $teamID; ?>" class="item-list-ctn ui-helper-reset clearfix">
                                            <?php
                                            foreach ($teamresult['subordinates'] as $key2 => $teamsubordinateresult) {
                                                $subordinateid = $teamsubordinateresult['user_id'];
                                                $subordinatefullName = $teamsubordinateresult['first_name'] . ' ' . $teamsubordinateresult['last_name'];
                                                $subordinatedesignation = $teamsubordinateresult['designation'];
                                                $subordinatedepartment = $teamsubordinateresult['department'];
                                                $subordinateemail = $teamsubordinateresult['email'];
                                                $subordinateimage = $teamsubordinateresult['profile_image'];
                                                ?>
                                                <li id="subordinate_<?php echo $subordinateid; ?>" class="clearfix item-list-inner subordinate_class subordinate_class_<?php echo $teammanagerid; ?>_<?php echo $teamID; ?> managersubordinate_<?php echo $teammanagerid; ?>_<?php echo $teamID; ?> teamclass_<?php echo $subordinateid; ?>" rel="<?php echo $subordinateid; ?>">
                                                    <ul class="list-unstyled item-row clearfix">
                                                        <li class="col-xs-12 col-sm-4 item-info">
                                                            <a class="item-left"><img width="35" height="35" id="profile_image_<?php echo $subordinateid; ?>" src="<?php echo $profile_image = (trim($subordinateimage) != '') ? $subordinateimage : 'images/user-img2.jpg'; ?>" class="img-circle profile_image" alt=""></a>
                                                            <div class="item-right">
                                                                <div class="pull-right visible-xs">
                                                                    <!--<a href="#"><i class="ic-email ic-16"></i></a>-->
                                                                    <a href="javascript:void(0)" rel="<?php echo $teammanagerid; ?>" existinguserstatus="1"  id="deletelink_<?php echo $subordinateid; ?>" teamcounter = "<?php echo $teamID; ?>" onclick="deleteuser(<?php echo $subordinateid; ?>, <?php echo $teammanagerid; ?>, 2, <?php echo $teamID; ?>);"><i class="ic-delete ic-16"></i></a>
                                                                </div>
                                                                <div class="item-right">
                                                                    <h5 class="name">
                                                                        <span rel="popover" id="name_<?php echo $subordinateid; ?>"><?php echo $subordinatefullName; ?></span>
                                                                    </h5>
                                                                    <p class="designation hidden-xs" id="disignation_department_<?php echo $subordinateid; ?>"><?php echo $subordinatedesignation; ?><span class="visible-xs" id="designation_<?php echo $subordinateid; ?>">, <?php echo $subordinatedepartment; ?></span></p>
                                                                    <p class="designation visible-xs"><?php echo $subordinatedesignation; ?><span class="" id="department_<?php echo $subordinateid; ?>">, <?php echo $subordinatedepartment; ?></span></p>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li class="col-md-3 col-sm-2 department hidden-xs"><?php echo $subordinatedepartment; ?></li>
                                                        <li class="col-sm-4 email hidden-xs" id="email_<?php echo $subordinateid; ?>"><?php echo $subordinateemail; ?></li>
                                                        <li class="col-md-1 col-xs-2 col-sm-2 hidden-xs">
                                                            <div class="pull-right">
                                                                <a href="javascript:void(0)" rel="<?php echo $teammanagerid; ?>" existinguserstatus="1"  id="deletelink_<?php echo $subordinateid; ?>" teamcounter = "<?php echo $teamID; ?>" onclick="deleteuser(<?php echo $subordinateid; ?>, <?php echo $teammanagerid; ?>, 2, <?php echo $teamID; ?>);">
                                                                    <i class="ic-delete ic-16"></i>
                                                                </a>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    <?php } ?>
                                    <div style="display: none;" id="myteamschildlist_<?php echo $teammanagerid; ?>_<?php echo $teamID; ?>" class="user-creation-ctn myteamschildlist" mymanagerid="<?php echo $teammanagerid; ?>"><div class="user-drop-area existinguser-drop-area ui-droppable" id="user-drop-area_<?php echo $teammanagerid; ?>_<?php echo $teamID; ?>" rel="<?php echo $teammanagerid; ?>" rel2="<?php echo $teamID; ?>"><p id="teammessage_<?php echo $teammanagerid; ?>_<?php echo $teamID; ?>" class="text-center">Drag &amp; Drop a User from left Sidebar to Assign</p></div></div>
                                </div>
                            </div>
                        <?php }
                        ?>
                    <?php }
                    ?>
                </div>
                <section id="team-creation-ctn" rel="0">
                    <div class="team-inner-ctn">
                        <div class="col-sm-12">
                            <div id="team-drop-area" areamanager="0" teamcounter="0" previousteamcounter="0">
                                <p class="drop-text hidden-xs">Drag & Drop a <b class="f-link">Manager</b> from left Sidebar to Assign</p>
                                <p class="drop-text visible-xs"><a href="javascript:void(0)" id="mobilemanager" data-toggle="modal" data-keyboard="false" data-target="#assignUserModal" areamanager="0" savedmanagerid="0" teamcounter="0" previousteamcounter="0">Add Manager</a></p>
                                <a href="javascript:void(0)" id="cancelteamcreationdiv" class="drop-cancel">Cancel</a>
                            </div>
                        </div>
                    </div>
                </section>
            </section>
        </main>
        @include('layouts.uploadCsvFile')
        @include('layouts.addNewJob')
        @include('layouts.addNewDepartment')
       	@include('layouts.assignUserMobile')
        @include('layouts.footerInclude')
        @include('layouts.createUser')
        <script src="{{ URL::to('scripts/userscript.js').'?'.env('JS_VERSION') }}"></script>
        <script src="{{ URL::to('scripts/index.js').'?'.env('JS_VERSION') }}"></script>
    </body>
</html>
