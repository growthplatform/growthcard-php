<?php

namespace App\Models;

use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Config;
use App\Models\EffectiveAction;
use Illuminate\Support\Facades\DB;

class Feed extends \Eloquent {

    use Eloquence,
        Mappable;

use SoftDeletes;

    protected $table = 'feed';
    protected $primaryKey = 'id';
    protected $maps = [
        'feedId' => 'id',
        'announcementId' => 'announcement_id',
        'postId' => 'post_id',
        'likeCount' => 'like_count',
        'commentCount' => 'comment_count',
        'effectiveActionId' => 'effective_action_id',
        'likeCount' => 'like_count',
        'commentCount' => 'comment_count',
        'companyId' => 'company_id',
        'teamId' => 'team_id',
        'pastEffectiveActionId' => 'past_ea_id',
        'userId' => 'user_id',
    ];
    protected $hidden = ['id', 'announcement_id', 'post_id', 'like_count', 'comment_count', 'effective_action_id',
        'like_count', 'comment_count', 'company_id', 'team_id', 'user_id', 'created_at', 'updated_at', 'deleted_at', 'department_id', 'past_ea_id'];
    protected $fillable = [];
    protected $appends = ['announcementId', 'feedId', 'postId', 'teamId', 'likeCount', 'commentCount', 'effectiveActionId', 'likeCount', 'commentCount', 'pastEffectiveActionId'];
    protected $dates = ['deleted_at'];

    public function __construct() {
        parent:: __construct();
    }

    public static function createFeed($data) {
        return Feed::insertGetId($data);
    }

    public function generateFeed($postData) {
        $userId = $postData['userId'];
        $teamId = $postData['teamId'];
        $companyId = $postData['companyId'];
        $limit = Config::get('constants.limit');
        $pageNo = $postData['pageNo'];
        $flag1 = $postData['flag1'];
        $flag2 = $postData['flag2'];
        $departmentId = 0;
        if ($flag2 != 0) {
            $departmentId = $postData['flag2'];
        }
        $skip = ($pageNo - 1) * $limit;
        $getTeamMembers = Feed::getTeamMembers($teamId, $companyId, $flag1, $userId);


        $teamMembers = array();
        $getManager = 0;
        if (isset($getTeamMembers) && is_object($getTeamMembers)) {
            if ($flag1 == 2) {
                $getManager = $getTeamMembers->user_id;
            }
            if ($flag1 == 1 || $flag1 == 3) {
                foreach ($getTeamMembers as $getTeamMember) {
                    $teamMembers[] = $getTeamMember->user_id;
                }
            }
        }
        $getFeeds = Feed::with(['user.member' => function($q) use ($teamId) {
                        $q->where('team_id', $teamId);
                    }, 'user.member.designation', 'announcement', 'post', 'effectiveaction', 'pasteffectiveaction'])
                ->select('id', 'past_ea_id', 'type', 'announcement_id', 'created_by', 'team_id', 'post_id', 'like_count', 'effective_action_id', 'like_count', 'company_id', 'team_id', 'user_id', 'created_at', 'updated_at', 'deleted_at', 'department_id', DB::raw('(CASE WHEN (feed.type = 1 OR feed.type = 2) THEN feed.created_by ELSE feed.user_id END) AS abc'));

        if ($flag1 != 3) {
            $getFeeds = $getFeeds->where(function ($query) use($userId, $companyId) {
                $query->where('user_id', $userId)
                        ->where('company_id', $companyId);
            });
        }
        if ($flag1 == 2) {
            $getFeeds = $getFeeds->where(function ($query) use($teamId, $getManager) {
                $query->where('team_id', $teamId)
                        ->where('created_by', $getManager);
            });
        }
        if ($flag1 == 1 || $flag1 == 3) {
            $getFeeds = $getFeeds->orWhere(function ($query) use($companyId, $teamMembers) {
                $query->whereIn('type', [3, 4])
                        ->whereIn('user_id', $teamMembers)
                        ->where('company_id', $companyId);
            });
        }
        if ($flag2 != 0) {
            $getFeeds = $getFeeds->having('department_id', '=', $departmentId);
        }

        $feedsCount = count($getFeeds->get());
        $totalPages = ceil($feedsCount / $limit);
        $feeds = $getFeeds->orderBy('created_at', 'desc')->skip($skip)->take($limit)->get();
        foreach ($feeds as $feed) {
            $feed->isLiked = FeedLike::feedIsLiked($userId, $feed->id);
            if ($feed->type == 3 || $feed->type == 4) {
                $eaCheck = EffectiveAction::checkFeedEa($feed->user['userId'], $feed->effective_action_id, $feed->team_id, $feed->past_ea_id);
                $comment = Comment::with('user')
                        ->where('created_at', '>=', $eaCheck['startTime'])
                        ->orderBy('created_at', 'desc')
                        ->where('user_id', $feed->created_by)
                        ->where('team_id', $feed->team_id)
                        ->where('effective_action_id', $feed->effective_action_id);
                if ($eaCheck['flag'] == 2) {
                    $comment = $comment->where('created_at', '<=', $eaCheck['endTime']);
                }
                $feed->commentCount = $comment->count();
                $feed->comment = $comment->first();
                $graphSummary = Users::getEffectiveSummary($feed->user['userId'], $feed->effective_action_id, $eaCheck);
                if ($graphSummary['AVG'] != 0) {
                    $graphSummary['AVG'] = round($graphSummary['AVG']);
                    $avgArray = array(
                        'adhrenceCount' => $graphSummary['AVG'],
                        'days' => 'Avg');
                    $graphSql = Users::getGraph($feed->user['userId'], $feed->effective_action_id, $eaCheck);
                    $graph = DB::select($graphSql . ' order by a.work_day desc limit 7');
                    $graph[] = $avgArray;
                    $feed->effectiveaction['graph'] = array_reverse($graph);
                } else {
                    $feed->effectiveaction['graph'] = array();
                }
            }
        }
        $recentAnnouncementId = Users::select('recent_announcement')->where('id', $userId)->first();
        $recentAnnouncement = NULL;
        if (is_object($recentAnnouncementId) && isset($recentAnnouncementId->recent_announcement)) {
            $announcementCreator = Announcement::select('user_id')->where('id', $recentAnnouncementId->recent_announcement)->first();
            $recentAnnouncement = Announcement::with(['user', 'member' => function($q) use($announcementCreator) {
                            $q->where('role', 1);
                            $q->where('user_id', $announcementCreator->user_id);
                        }, 'member.designation'])
                    ->where('user_id', $announcementCreator->user_id)
                    ->where('id', $recentAnnouncementId->recent_announcement)
                    ->first();
        }
        $arr['pageNo'] = $pageNo;
        $arr['totalPages'] = $totalPages;
        $arr['result']['feed'] = $feeds;
        $arr['result']['recentAnnouncement'] = $recentAnnouncement;
        if ($pageNo > $totalPages) {
            return array('status' => 'true', 'result' => $arr, 'message' => trans('messages.no_more_data_not_found'));
        } else {
            return array('status' => 'true', 'result' => $arr, 'message' => '');
        }
    }

    public function user() {
        return $this->belongsTo('App\Models\Users', 'abc');
    }

    public function userdepartment() {
        return $this->belongsTo('App\Models\UserDepartment', 'user_id');
    }

    public function announcement() {
        return $this->belongsTo('App\Models\Announcement', 'announcement_id');
    }

    public function post() {
        return $this->belongsTo('App\Models\Post', 'post_id');
    }

    public function effectiveaction() {
        return $this->belongsTo('App\Models\EffectiveAction', 'effective_action_id');
    }

    public function pasteffectiveaction() {
        return $this->belongsTo('App\Models\PastEffectiveAction', 'past_ea_id');
    }

    public function getTeamMembers($teamId, $companyId, $flag1, $userId) {
        $teamMember = TeamMember::select('user_id')
                ->where('team_id', $teamId)
                ->where('company_id', $companyId)
                ->groupBy('user_id');
        if ($flag1 == 1) {
            $teamMember = $teamMember->get();
        }
        if ($flag1 == 2) {
            $teamMember = $teamMember->where('role', 1)->first();
        }
        if ($flag1 == 3) {
            $teamMember = $teamMember->where('role', 2)->where('user_id', '!=', $userId)->get();
        }
        return $teamMember;
    }

    public function member() {
        return $this->belongsTo('App\Models\TeamMember', 'user_id');
    }

}
