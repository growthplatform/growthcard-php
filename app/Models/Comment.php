<?php

namespace App\Models;

use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Config;
use App\Models\Users;
use App\Utility\CommonMethod;

class Comment extends \Eloquent {

    use Eloquence,
        Mappable;

use SoftDeletes;

    protected $table = 'comment';
    protected $primaryKey = 'id';
    protected $maps = [
        'commentId' => 'id',
        'createdAt' => 'created_at'
    ];
    protected $hidden = ['id', 'effective_action_id', 'commented_by', 'user_id', 'team_id',
        'created_at', 'updated_at', 'deleted_at'];
    protected $fillable = [];
    protected $appends = ['commentId', 'createdAt'];
    protected $dates = ['deleted_at'];

    public function commentOnEffectiveAction($postData) {
        $this->user = new Users();
        $userId = $postData['userId'];
        $teamId = $postData['teamId'];
        $subordinateId = $postData['subordinateId'];
        $content = $postData['content'];
        $effectiveActionId = $postData['effectiveActionId'];
        $companyId = $postData['companyId'];        
        $checkManager = TeamMember::checkTeamManager($userId, $teamId);
        $countRole = TeamMember::where('user_id', $subordinateId)->whereNotNull('team_id')->where('role', 1)->count();
        $microDate = CommonMethod::getMicroDateTime();
        if ($checkManager == 0) {                   //comment by subordinate
            $insertArray = array('content' => $content,
                'effective_action_id' => $effectiveActionId,
                'commented_by' => $userId,
                'user_id' => $subordinateId,
                'team_id' => $teamId,
                'role' => 2,
                'created_at' => $microDate
            );
        } else {                    //comment by manager
            $insertArray = array('content' => $content,
                'effective_action_id' => $effectiveActionId,
                'commented_by' => $userId,
                'user_id' => $subordinateId,
                'team_id' => $teamId,
                'role' => 1,
                'created_at' => $microDate
            );
            $payload['type'] = 3;
            $effectiveActionName = EffectiveAction::select('effective_action_name')->where('id', $effectiveActionId)->first();
            $user = Users::select('first_name', 'last_name')->where('id', $userId)->first();
            $userName = ucwords($user->first_name . ' ' . $user->last_name);
            $payload['message'] = $userName . ' has commented on your assigned Effective Action ' . $effectiveActionName->effective_action_name;
            $payload['userId'] = $subordinateId;
            $payload['teamId'] = $teamId;
            $payload['effectiveActionId'] = $effectiveActionId;
            $payload['message1'] = $userName;
            $payload['message2'] = ' has commented on your assigned Effective Action ';
            $payload['message3'] = $effectiveActionName->effective_action_name;
            PushNotification::sendPushNotification($subordinateId, $payload);
        }
        $member = TeamMember::select('ea_assign_at')
                ->where('user_id', $subordinateId)
                ->where('team_id', $teamId)
                ->where('effective_action_id', $effectiveActionId)
                ->first();

        $commentExist = Comment::select('id')
                ->where('user_id', $subordinateId)
                ->where('effective_action_id', $effectiveActionId)
                ->where('team_id', $teamId);
        if (is_object($member) && isset($member->ea_assign_at)) {
            $commentExist = $commentExist->where('created_at', '>=', $member->ea_assign_at)->first();
        } else {
            $commentExist = $commentExist->first();
        }
        $commentId = Comment::insertGetId($insertArray);
        $comment = Comment::with(['user', 'member' => function($q) use($teamId) {
                        $q->where('team_id', $teamId);
                    }, 'member.designation'])
                ->where('team_id', $teamId)
                ->where('effective_action_id', $effectiveActionId)
                ->where('id', $commentId)
                ->first();
        if (!is_object($commentExist) && !isset($commentExist->id)) {
            $department = UserDepartment::select('department_id')->where('user_id', $subordinateId)->first();
            $data = array('announcement_id' => NULL,
                'post_id' => NULL,
                'effective_action_id' => $effectiveActionId,
                'company_id' => $companyId,
                'team_id' => $teamId,
                'user_id' => $subordinateId,
                'type' => 3,
                'department_id' => $department->department_id,
                'created_by' => $subordinateId
            );
            //if ($countRole == 0) {
                Feed::createFeed($data);
           // }
        }

        TeamMember::where('user_id', $subordinateId)
                ->where('team_id', $teamId)
                ->where('effective_action_id', $effectiveActionId)
                ->update(['last_activity_time' => date('Y-m-d H:i:s')]);

        return $comment;
    }

    public function getCommentsOfEffectiveAction($postData) {
        $teamId = $postData['teamId'];
        $userId = $postData['userId'];
        $effectiveActionId = $postData['effectiveActionId'];
        $pageNo = $postData['pageNo'];
        $limit = Config::get('constants.limit');
        $skip = ($pageNo - 1) * $limit;
        $subordinateId = $postData['subordinateId'];
        $checkManagers = TeamMember::select('team_id')
                ->where('user_id', $userId)
                ->where('team_id', $teamId)
                ->where('role', 1)
                ->get();

        if (is_object($checkManagers) && count($checkManagers) > 0) {
            $checkManagers = TeamMember::select('team_id')
                    ->where('user_id', $userId)
                    ->whereNotNull('team_id')
                    ->where('role', 1)
                    ->get();
            foreach ($checkManagers as $checkManager) {
                $teams[] = $checkManager->team_id;
            }
        }

        $memberUpdatedTime = TeamMember::select('updated_at', 'ea_assign_at')
                ->where('user_id', $subordinateId)
                ->where('team_id', $teamId)
                ->where('effective_action_id', $effectiveActionId)
                ->first();
        $getComments = Comment::with(['user', 'member' => function($q) {
                        $q->whereNotNull('team_id');
                    }, 'member.designation'])
                ->where('user_id', $subordinateId)
                ->where('effective_action_id', $effectiveActionId);
        if (isset($teams)) {
            $getComments = $getComments->whereIn('team_id', $teams);
        } else {
            $getComments = $getComments->where('team_id', $teamId);
        }
        $pastEffectiveActionId = 0;
        if(!empty($postData['pastEffectiveActionId'])){
            $pastEffectiveActionId = $postData['pastEffectiveActionId'];
        }
        if ($pastEffectiveActionId > 0) {
            $pastEaTime = PastEffectiveAction::select('start_date', 'created_at')->where('id', $pastEffectiveActionId)->first();
            $getComments = $getComments->where('created_at', '>=', $pastEaTime->start_date)
                    ->where('created_at', '<=', $pastEaTime->created_at);
        } else {
            if (isset($memberUpdatedTime) && $memberUpdatedTime->ea_assign_at != NULL) {
                $getComments = $getComments->where('created_at', '>=', $memberUpdatedTime->ea_assign_at);
            }
        }

        $getCommentsCount = $getComments->count();
        $totalPages = ceil($getCommentsCount / $limit);
        $comments = $getComments->orderBy('created_at', 'desc')->skip($skip)->take($limit)->get();
        $arr['pageNo'] = $pageNo;
        $arr['totalPages'] = $totalPages;
        $arr['comment'] = $comments;
        if ($pageNo > $totalPages) {
            return array('status' => 'true', 'result' => $arr, 'message' => trans('messages.no_more_data_not_found'));
        } else {
            return array('status' => 'true', 'result' => $arr, 'message' => '');
        }
    }

    public function user() {
        return $this->belongsTo('App\Models\Users', 'commented_by', 'id');
    }

    public function member() {
        return $this->belongsTo('App\Models\TeamMember', 'commented_by', 'user_id');
    }

}
