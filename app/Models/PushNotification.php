<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Repositories\Push\PushNotificationApple;
use App\Repositories\Push\PushNotificationAndroid;

class PushNotification extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $maps = [
        'effectiveActionId' => 'effective_action_id',
        'teamId' => 'team_id',
        'userId' => 'user_id',
        'createdAt' => 'created_at',
        'pastEffectiveActionId' => 'past_ea_id'
    ];
    protected $table = 'push_notifications';
    protected $primaryKey = 'id';
    protected $fillable = [];
    protected $hidden = ['created_at', 'updated_at', 'effective_action_id', 'team_id', 'user_id','past_ea_id'];
    protected $appends = ['effectiveActionId', 'teamId', 'userId', 'createdAt','pastEffectiveActionId'];
    protected $dates = ['deleted_at'];

    public function send($user_detail, $payload, $badge) {
        if (isset($user_detail->device_token)) {
            switch ($user_detail->device_type) {
                case 'android':
                    $send_push = new PushNotificationAndroid();
                    break;
                case 'ios':
                    $send_push = new PushNotificationApple(env('ENVIRONMENT'));
                    break;
                default:
            }
            $this->payload = json_encode($payload);
            $send_push->send($user_detail->device_token, $payload['message'], $payload, $badge);
            return true;
        } else {
            return false;
        }
    }

    public function setPayloadAttribute($payload) {
        $this->attributes['payload'] = print_r($payload, true);
    }

    public static function sendPushNotification($userId, $payload) {
        $push = new PushNotification();
        $userDetail = Device::where('user_id', $userId)->get();
        $badge = $push->updateBadge($userId);
        $push->user_id = $userId;
        $push->read = 0;
        $push->type = $payload['type'];
        if ($payload['teamId'] > 0) {
            $push->team_id = $payload['teamId'];
        }
        if ($payload['effectiveActionId'] > 0) {
            $push->effective_action_id = $payload['effectiveActionId'];
        }
        $push->user_id = $payload['userId'];
        $push->message1 = $payload['message1'];
        $push->message2 = $payload['message2'];
        $push->message3 = $payload['message3'];
        $push->save();
        unset($payload['message1']);
        unset($payload['message2']);
        unset($payload['message3']);
        if ($userDetail) {
            foreach ($userDetail as $user) {
                $push->send($user, $payload, $badge);
            }
        }
    }

    public function updateBadge($userId) {
        $row = Users::select('badge')->where('id', $userId)->first();
        $new_badge = $row->badge + 1;
        Users::where('id', $userId)->update(array('badge' => $new_badge));
        return (int) $new_badge;
    }

    public static function notificationsList($postData) {
        $limit = 10;
        $pageNo = $postData['pageNo'];
        $teamId = $postData['teamId'];
        $skip = ($pageNo - 1) * $limit;
        $userId = $postData['userId'];
        $pushNotifications = PushNotification::select('id', 'team_id', 'read', 'created_at', 'type', 'effective_action_id', 'past_ea_id', 'user_id', 'message1', 'message2', 'message3')
                ->where('user_id', $userId)
                ->where('team_id', $teamId)
                ->orderBy('created_at', 'desc');
        $itemsCount = count($pushNotifications->get());
        $totalPages = ceil($itemsCount / $limit);
        $pushNotificationsData = $pushNotifications->skip($skip)->take($limit)->get();
        foreach ($pushNotificationsData as $notification) {
            $notificationIds[] = $notification->id;
            $image = Users::select('profile_image')->where('id', $notification->user_id)->first();
            $notification->profileImage = $image->profile_image;
            unset($notification->id);
        }
        if (isset($notificationIds)) {
            PushNotification::whereIn('id', $notificationIds)->update(['read' => 1]);
            Users::where('id', $userId)->update(['badge' => 0]);
        }
        $arr['pageNo'] = $pageNo;
        $arr['totalPages'] = $totalPages;
        $arr['notifications'] = $pushNotificationsData;
        if ($pageNo > $totalPages) {
            return array('status' => 'true', 'result' => $arr, 'message' => trans('messages.no_more_data_not_found'));
        } else {
            return array('status' => 'true', 'result' => $arr, 'message' => '');
        }
    }

    public function getEffectiveActionIdAttribute() {
        if ($this->attributes['effective_action_id'] != NULL) {
            return $this->attributes['effective_action_id'];
        } else {
            return 0;
        }
    }

    public function getTeamIdAttribute() {
        if ($this->attributes['team_id'] != NULL) {
            return $this->attributes['team_id'];
        }
    }

    public function getUserIdAttribute() {
        if ($this->attributes['user_id'] != NULL) {
            return $this->attributes['user_id'];
        }
    }

    public function getCreatedAtAttribute() {
        if ($this->attributes['created_at'] != NULL) {
            return $this->attributes['created_at'];
        }
    }
    public function getPastEffectiveActionIdAttribute() {
        if ($this->attributes['past_ea_id'] != NULL) {
            return $this->attributes['past_ea_id'];
        }
    }

    public static function cronPush() {
        $date = date('Y-m-d');
        $date2 = date('Y-m-d', strtotime('-4 days', strtotime($date)));
        $getTeamMembers = TeamMember::select('user_id', 'effective_action_id', 'team_id')
                ->where('effective_action_id', '!=', 'NULL')
                ->groupBy('user_id')
                ->get();
        foreach ($getTeamMembers as $member) {
            $getData = UserEffectiveActionHistory::select(DB::raw("COUNT(id) AS adhrenceCount,created_date"))
                    ->where('user_id', $member->user_id)
                    ->where('effective_action_id', $member->effective_action_id)
                    ->where('created_date', '<=', $date)
                    ->where('created_date', '>', $date2)
                    ->groupBy('created_date')
                    ->get();
            $getDataCount = count($getData);
            if ($getDataCount == 4) {
                $graphCount = 0;
                $lastCount = 0;
                $avg = 0;
                for ($i = 0; $i < $getDataCount; $i++) {
                    $graphCount = $graphCount + $getData[$i]['adhrenceCount'];
                    if ($i == 3) {
                        $lastCount = $getData[$i]['adhrenceCount'];
                        $avg = floor($graphCount / 4);
                        if ($lastCount == $avg) {
                            $teamManager = TeamMember::select('user_id')->where('team_id', $member->team_id)->where('role', 1)->first();
                            $userId = $teamManager->user_id;
                            $user = Users::select('first_name', 'last_name')->where('id', $userId)->first();
                            $effective = EffectiveAction::select('effective_action_name')->where('id', $member->effective_action_id)->first();
                            $effectiveData = $effective->effective_action_name;
                            $payload['type'] = 8;
                            $userName = ucwords($user->first_name . ' ' . $user->last_name);
                            $payload['message'] = $userName . ' has achieved peak achievement of assigned effective action ' . $effective;
                            $payload['userId'] = $userId;
                            $payload['teamId'] = $member->team_id;
                            $payload['effectiveActionId'] = $member->effective_action_id;
                            $payload['message1'] = $userName;
                            $payload['message2'] = ' has achieved peak achievement of assigned effective action ';
                            $payload['message3'] = $effectiveData;
                            PushNotification::sendPushNotification($userId, $payload);
                        }
                    }
                }
            }
        }
        return;
    }

}
