<?php

namespace App\Models;

use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Config;

class Post extends \Eloquent {

    use Eloquence,
        Mappable;

use SoftDeletes;

    protected $table = 'post';
    protected $primaryKey = 'id';
    protected $maps = [
        'postId' => 'id',
        'createdAt' => 'created_at',
    ];
    protected $hidden = ['id', 'user_id', 'company_id', 'updated_at', 'deleted_at', 'created_at'];
    protected $fillable = [];
    protected $appends = ['postId', 'createdAt'];
    protected $dates = ['deleted_at'];

    public function __construct() {
        parent:: __construct();
    }

    public static function createPost($postData) {
        $content = $postData['content'];
        $userId = $postData['userId'];
        $teamId = $postData['teamId'];
        $flag = $postData['flag'];  //1->my team, 2-> my department, 3->all
        $companyId = $postData['companyId'];
        $department = UserDepartment::select('department_id')->where('user_id', $userId)->first();
        $post = array(
            'content' => $content,
            'user_id' => $userId,
            'company_id' => $companyId,
            'department_id' => $department->department_id
        );
        $postId = Post::insertGetId($post);
        if ($postId) {
            $getRecipients = Post::getPostRecipients($flag, $companyId, $teamId, $department->department_id, $userId);
            foreach ($getRecipients as $recipient) {
                $data = array('announcement_id' => NULL,
                    'post_id' => $postId,
                    'effective_action_id' => NULL,
                    'company_id' => $companyId,
                    'team_id' => $teamId,
                    'type' => 2,
                    'created_by' => $userId
                );
                if ($flag == 1 || $flag == 3) {
                    $data['user_id'] = $recipient->id;
                } else {
                    $data['user_id'] = $recipient->user_id;
                }
                $data['department_id'] = $department->department_id;
                $receivers = array('user_id' => $data['user_id'], 'post_id' => $postId);
                PostReciever::insertGetId($receivers);
                Feed::createFeed($data);
            }
        }
        return;
    }

    public static function getPostRecipients($flag, $companyId, $teamId, $departmentId, $user_id) {
        if ($flag == 1 || $flag == 3) {
            return Users::select('id')->with('member')
                            ->whereHas('member', function($q) use($companyId, $flag, $teamId ) {
                                // Query the name field in team_member table
                                if ($flag == 1) {
                                    $q->where('company_id', $companyId);
                                    $q->where('team_id', $teamId);
                                    $q->groupBy('user_id');
                                }
                                if ($flag == 3) {
                                    $q->where('company_id', $companyId);
                                    $q->groupBy('user_id');
                                }
                            })
                            ->get();
        } else {
            return UserDepartment::select('user_id')->where('department_id', $departmentId)->get();
        }
    }

    public static function getPost($postData) {
        $userId = $postData['subordinateId'];
        $flag = $postData['flag'];
        $companyId = $postData['companyId'];
        $limit = Config::get('constants.limit');
        $pageNo = $postData['pageNo'];
        $skip = ($pageNo - 1) * $limit;
        $getPost = Post::with(['user', 'member', 'member.designation']);
        if ($flag == 1) {
            $ids = PostReciever::select('post_id')->where('user_id', $userId)->get();
            $postIds = array();
            foreach ($ids as $id) {
                $postIds[] = $id->post_id;
            }
            $getPost = $getPost->whereIn('id', $postIds)->where('company_id', $companyId);
        } else {
            $getPost = $getPost->where('user_id', $userId);
        }
        $postCount = $getPost->count();
        $totalPages = ceil($postCount / $limit);
        $post = $getPost->orderBy('updated_at', 'desc')->skip($skip)->take($limit)->get();
        $arr['pageNo'] = $pageNo;
        $arr['totalPages'] = $totalPages;
        $arr['post'] = $post;
        if ($pageNo > $totalPages) {
            return array('status' => 'true', 'result' => $arr, 'message' => trans('messages.no_more_data_not_found'));
        } else {
            return array('status' => 'true', 'result' => $arr, 'message' => '');
        }
    }

    public function user() {
        return $this->belongsTo('App\Models\Users', 'user_id');
    }

    public function member() {
        return $this->belongsTo('App\Models\TeamMember', 'user_id', 'user_id');
    }

    public static function deletePost($postData) {
        $postId = $postData['postId'];
        $date = date('Y-m-d H:i:s');
        Post::where('id', $postId)->update(['deleted_at' => $date]);
        PostReciever::where('post_id',$postId)->update(['deleted_at' => $date]);
        Feed::where('post_id', $postId)->update(['deleted_at' => $date]);
        return;
    }

}
