<?php

namespace App\Models;

use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserDepartment extends \Eloquent {

    use Eloquence,
        Mappable;

use SoftDeletes;

    protected $table = 'user_department';
    protected $primaryKey = 'id';
    protected $maps = [
        'userDepartmentId' => 'id',
        'departmentId' => 'department_id',
    ];
    protected $hidden = ['id', 'user_id', 'department_id',
        'deleted_at', 'created_at', 'updated_at'];
    protected $fillable = [];
    protected $appends = [];
    protected $dates = ['deleted_at'];

    public function department() {
        return $this->belongsTo('App\Models\Department', 'department_id', 'id');
    }

}
