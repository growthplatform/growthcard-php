<?php

namespace App\Models;

use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Device;
use App\Models\PushNotification;
use App\Models\PastEffectiveAction;
use Config;
use App\Utility\CommonMethod;
use Illuminate\Support\Facades\DB;

class TeamMember extends \Eloquent {

    use Eloquence,
        Mappable;

use SoftDeletes;

    protected $table = 'team_member';
    protected $primaryKey = 'id';
    protected $maps = [
        'teamMemberID' => 'id',
        'teamId' => 'team_id',
        'designationId' => 'designation_id',
        'effectiveActionId' => 'effective_action_id',
        'companyId' => 'company_id',
        'effectiveActionDescription' => 'effective_action_description',
        'createdDtTm' => 'created_at',
    ];
    protected $hidden = ['id', 'user_id', 'designation_id', 'role', 'company_id', 'effective_action_id',
        'deleted_at', 'created_at', 'updated_at', 'effective_action_description', 'team_id', 'assign_at'];
    protected $fillable = ['user_id', 'team_id', 'designation_id', 'role'];
    protected $appends = ['designationId', 'effectiveActionId', 'companyId', 'effectiveActionDescription', 'teamId', 'teamMemberID', 'createdDtTm'];
    protected $dates = ['deleted_at'];
    protected $casts = ['team_id' => 'integer', 'designation_id' => 'integer', 'effective_action_id' => 'integer', 'company_id' => 'integer', 'role' => 'integer'];

    const MANAGER_ROLE_ID = 1;
    const SUBORDINATE_ROLE_ID = 2;

    public function company() {
        return $this->belongsTo('App\Models\Company', 'company_id', 'id');
    }

    public function designation() {
        return $this->belongsTo('App\Models\Designation', 'designation_id', 'id');
    }

    public function user() {
        return $this->belongsTo('App\Models\Users', 'user_id', 'id');
    }

    public function days() {
        return $this->hasMany('App\Models\UserWorkDay', 'user_id');
    }

    public function userdepartment() {
        return $this->belongsTo('App\Models\UserDepartment', 'user_id', 'user_id');
    }

    public function effective() {
        return $this->belongsTo('App\Models\EffectiveAction', 'effective_action_id', 'id');
    }

    public static function checkTeamManager($userId, $teamId) {
        $teamManager = TeamMember::where('user_id', $userId)->where('team_id', $teamId)->where('role', static::MANAGER_ROLE_ID)->first();
        if (!is_object($teamManager)) {
            return 0;
        } else {
            return 1;
        }
    }

    public function getSubordinates($postData) {
        $userId = $postData['userId'];
        $checkManagers = TeamMember::select('team_id')
                ->where('user_id', $userId)
                ->whereNotNull('team_id')
                ->where('role', static::MANAGER_ROLE_ID)
                ->get();
        if (is_object($checkManagers) && count($checkManagers) > 0) {
            foreach ($checkManagers as $checkManager) {
                $teams[] = $checkManager->team_id;
            }
            $limit = Config::get('constants.limit');
            $pageNo = $postData['pageNo'];
            $skip = ($pageNo - 1) * $limit;
            $getSubordinates = TeamMember::with('designation', 'user')
                    ->whereIn('team_id', $teams)
                    ->groupBy('user_id');
        } else {
            return false;
        }
        $subordinatesCount = count($getSubordinates->get());
        $totalPages = ceil($subordinatesCount / $limit);
        $subordinates = $getSubordinates->orderBy('role', 'asc')->skip($skip)->take($limit)->get();
        $arr['pageNo'] = $pageNo;
        $arr['totalPages'] = $totalPages;
        $arr['subordinates'] = $subordinates;
        if ($pageNo > $totalPages) {
            return array('status' => 'true', 'result' => $arr, 'message' => trans('messages.no_more_data_not_found'));
        } else {
            return array('status' => 'true', 'result' => $arr, 'message' => '');
        }
    }

    public function assignEffectiveAction($postData) {
        $userId = $postData['userId'];
        $teamId = $postData['teamId'];
        $subordinateId = $postData['subordinateId'];
        $effectiveActionId = $postData['effectiveActionId'];
        $effectiveActionDescription = $postData['effectiveActionDescription'];
        $checkManager = $this->checkTeamManager($userId, $teamId);
        if ($checkManager == 0) {
            return 0;
        }
        $currentEffectiveAction = TeamMember::select('effective_action_id', 'created_at', 'effective_action_description', 'ea_assign_at')
                ->where('user_id', $subordinateId)
                ->first();
        $date = date('Y-m-d H:i:s');
        $microDate = CommonMethod::getMicroDateTime();

        if ($currentEffectiveAction->effective_action_id != '') {
            $pastEaId = PastEffectiveAction::insertGetId(array('user_id' => $subordinateId,
                        'effective_action_id' => $currentEffectiveAction->effective_action_id,
                        'start_date' => $currentEffectiveAction->ea_assign_at,
                        'effective_action_description' => $currentEffectiveAction->effective_action_description,
                        'created_at' => $microDate,
            ));
            Feed::where('user_id', $subordinateId)->where('effective_action_id', $currentEffectiveAction->effective_action_id)->whereNull('past_ea_id')->update(['past_ea_id' => $pastEaId]);
            PushNotification::where('user_id', $subordinateId)->where('effective_action_id', $currentEffectiveAction->effective_action_id)->whereNull('past_ea_id')->update(['past_ea_id' => $pastEaId]);
        }

        TeamMember::where('user_id', $subordinateId)
                ->update([
                    'effective_action_id' => $effectiveActionId,
                    'effective_action_description' => $effectiveActionDescription,
                    'created_at' => $date,
                    'updated_at' => $date,
                    'ea_assign_at' => $microDate
        ]);

        $payload['type'] = 2;
        if ($userId != $subordinateId) {
            $effectiveActionName = EffectiveAction::select('effective_action_name')->where('id', $effectiveActionId)->first();
            $user = Users::select('first_name', 'last_name')->where('id', $userId)->first();
            $userName = ucwords($user->first_name . ' ' . $user->last_name);
            $payload['message'] = $userName . ' has assigned you an Effective Action ' . $effectiveActionName->effective_action_name;
            $payload['userId'] = $subordinateId;
            $payload['teamId'] = $teamId;
            $payload['effectiveActionId'] = $effectiveActionId;
            $payload['message1'] = $userName;
            $payload['message2'] = ' has assigned you an Effective Action ';
            $payload['message3'] = $effectiveActionName->effective_action_name;
            PushNotification::sendPushNotification($subordinateId, $payload);
        }
        return 1;
    }

    public function changeEffectiveAction($postData) {
        $userId = $postData['userId'];
        $teamId = $postData['teamId'];
        $subordinateId = $postData['subordinateId'];
        $effectiveActionId = $postData['effectiveActionId'];
        $checkManager = $this->checkTeamManager($userId, $teamId);
        if ($checkManager == 0) {
            return 0;
        }
        TeamMember::where('user_id', $subordinateId)
                ->where('team_id', $teamId)
                ->update([
                    'effective_action_id' => NULL,
                    'effective_action_description' => '']);
        UserEffectiveActionHistory::where('user_id', $subordinateId)
                ->where('team_id', $teamId)
                ->where('effective_action_id', $effectiveActionId)
                ->forceDelete();
        return 1;
    }

    public function markAdhrence($postData) {
        $userId = $postData['userId'];
        $teamId = $postData['teamId'];
        $effectiveActionId = $postData['effectiveActionId'];
        $companyId = $postData['companyId'];
        $date = date('Y-m-d');
        $day = date('N');
        $countRole = TeamMember::where('user_id', $userId)
                //->whereNotNull('team_id')
                ->where('team_id', $teamId)
                ->where('role', static::MANAGER_ROLE_ID)
                ->count();

        $checkAssigned = TeamMember::select('id', 'updated_at')
                ->where('user_id', $userId)
                ->where('teamId', $teamId)
                ->where('effectiveActionId', $effectiveActionId)
                ->first();
        if (isset($checkAssigned->id)) {
            $adhrenceExist = UserEffectiveActionHistory::select('id')
                    ->where('user_id', $userId)
                    ->where('team_id', $teamId)
                    ->where('effective_action_id', $effectiveActionId)
                    ->where('created_at', '>=', $checkAssigned->updated_at->format('Y-m-d H:i:s'))
                    ->first();
            UserEffectiveActionHistory::insertGetId(['user_id' => $userId,
                'team_id' => $teamId,
                'effective_action_id' => $effectiveActionId,
                'created_date' => $date,
                'work_day' => $day
            ]);

            if (!is_object($adhrenceExist) && !isset($adhrenceExist->id)) {
                $department = UserDepartment::select('department_id')->where('user_id', $userId)->first();
                $data = array('announcement_id' => NULL,
                    'post_id' => NULL,
                    'effective_action_id' => $effectiveActionId,
                    'company_id' => $companyId,
                    'team_id' => $teamId,
                    'user_id' => $userId,
                    'type' => 4,
                    'department_id' => $department->department_id,
                    'created_by' => $userId
                );
                if ($countRole == 0) {
                    Feed::createFeed($data);
                }
            }

            $this->images = new Images();
            $imageArr = $this->images->getRandomImages($companyId);
            if (count($imageArr) === 0) {
                $imageArr = $this->images->getRandomImages(0);
                if (count($imageArr) === 0) {
                    $imageArr[0]['name'] = 'dog.png';
                    $imageArr[0]['caption'] = 'You seem to be doing Amazing!';
                    $imageArr[0]['message'] = 'I see that your work is getting better and better each day and I like it. Keep it up hard work.';
                }
            }

            foreach ($imageArr as $key => $dataArr) {
                if (env('STORAGETYPE') == 'AWS') {
                    $imageArr[$key]['name'] = $dataArr['name'];
                } else {
                    $imageArr[$key]['name'] = env('SERVER_ROOT') . $dataArr['name'];
                }
            }

            TeamMember::where('user_id', $userId)
                    ->where('team_id', $teamId)
                    ->where('effective_action_id', $effectiveActionId)
                    ->update(['last_activity_time' => date('Y-m-d H:i:s')]);
            return $imageArr;
        } else {
            return 2;
        }
        return;
    }

    public function getTeamMembers($postData) {
        $subordinateId = $postData['subordinateId'];
        $teamId = $postData['teamId'];
        $pageNo = $postData['pageNo'];
        $companyId = $postData['companyId'];
        $checkRole = TeamMember::select('role')
                ->where('user_id', $subordinateId)
                ->where('team_id', $teamId)
                ->first();

        $members = TeamMember::with('user')
                ->where('user_id', '!=', $subordinateId)
                ->where('company_id', $companyId)
                ->groupBy('user_id');

        if (!empty($checkRole) && ($checkRole->role == static::MANAGER_ROLE_ID)) {
            $checkManagers = TeamMember::select('team_id')
                    ->where('user_id', $subordinateId)
                    ->whereNotNull('team_id')
                    ->where('role', static::MANAGER_ROLE_ID)
                    ->get();
            if (is_object($checkManagers) && count($checkManagers) > 0) {
                foreach ($checkManagers as $checkManager) {
                    $teams[] = $checkManager->team_id;
                }
                $members = $members->whereIn('team_id', $teams);
            }
        } else {
            $members = $members->where('team_id', $teamId);
        }
        $limit = Config::get('constants.limit');
        $skip = ($pageNo - 1) * $limit;
        $membersCount = count($members->get());
        $totalPages = ceil($membersCount / $limit);
        $members = $members->orderBy('updated_at', 'desc')->skip($skip)->take($limit)->get();
        $arr['pageNo'] = $pageNo;
        $arr['totalPages'] = $totalPages;
        $arr['members'] = $members;
        if ($pageNo > $totalPages) {
            return array('status' => 'true', 'result' => $arr, 'message' => trans('messages.no_more_data_not_found'));
        } else {
            return array('status' => 'true', 'result' => $arr, 'message' => '');
        }
    }

    public function getDesignationID($userIDArr) {
        return TeamMember::select('user_id', 'designation_id')->whereIn('user_id', $userIDArr)->get()->keyBy('user_id')->toArray();
    }

    public function createUpdateTeamMember($dataArr) {
        $dtTime = date('Y-m-d H:i:s');
        foreach ($dataArr as $teamID => $rollArr) {
            foreach ($rollArr as $rollID => $rollDataArr) {
                if ($rollID === static::MANAGER_ROLE_ID) {
                    $t = microtime(true);
                    $micro = sprintf("%06d", ($t - floor($t)) * 1000000);
                    $dt = date('Y-m-d H:i:s.' . $micro, $t);
                    $user_id = $rollDataArr['user_id'];
                    $designation_id = $rollDataArr['designation_id'];
                    $company_id = $rollDataArr['company_id'];

                    $tempteamMemberIDArr1 = TeamMember::select('id', 'team_id')->where('user_id', $user_id)
                                    ->where('role', $rollID)
                                    ->where('team_id', $teamID)->get()->toArray();
                    if (count($tempteamMemberIDArr1) === 0) {
                        $tempteamMemberIDArr2 = TeamMember::select('id', 'team_id')->where('user_id', $user_id)
                                        ->where('role', $rollID)
                                        ->where('team_id', NULL)->get()->toArray();
                        if (count($tempteamMemberIDArr2) === 0) {
                            $tempteamMemberID = 0;
                        } else {
                            $tempteamMemberID = $tempteamMemberIDArr2[0]['teamMemberID'];
                        }
                    } else {
                        $tempteamMemberID = $tempteamMemberIDArr1[0]['teamMemberID'];
                    }

                    $eaArr = $this->getEADetail($user_id, $rollID);
                    if ($tempteamMemberID > 0) {
                        $dataArr = array();
                        $dataArr['teamMemberID'] = $tempteamMemberID;
                        $dataArr['team_id'] = $teamID;
                        $dataArr['effective_action_id'] = $eaArr['effective_action_id'];
                        $dataArr['effective_action_description'] = $eaArr['effective_action_description'];
                        $updateInsertStatus = $this->updateField($dataArr);
                        if (count($tempteamMemberIDArr1) === 0) {
                            $deleteLoginDeviceUser = $this->deleteLoginDeviceUser(array($user_id));
                        }
                    } else {
                        $teamMemberData = array('user_id' => $user_id,
                            'team_id' => $teamID,
                            'designation_id' => $designation_id,
                            'company_id' => $company_id,
                            'role' => $rollID,
                            'effective_action_id' => $eaArr['effective_action_id'],
                            'effective_action_description' => $eaArr['effective_action_description'],
                            'created_at' => $dtTime,
                            'updated_at' => $dtTime,
                            'assign_at' => $dt);
                        $updateInsertStatus = TeamMember::insertGetId($teamMemberData);
                        $deleteLoginDeviceUser = $this->deleteLoginDeviceUser(array($user_id));
                    }
                } else if ($rollID === 2) {
                    foreach ($rollDataArr as $dataArr) {
                        $t = microtime(true);
                        $micro = sprintf("%06d", ($t - floor($t)) * 1000000);
                        $dt = date('Y-m-d H:i:s.' . $micro, $t);

                        $user_id = $dataArr['user_id'];
                        $designation_id = $dataArr['designation_id'];
                        $company_id = $dataArr['company_id'];
                        $tmpteamMemberIDArr = TeamMember::select('id', 'team_id')->where('user_id', $user_id)
                                        ->where('role', $rollID)->get()->toArray();

                        if ((count($tmpteamMemberIDArr) > 0) && ($tmpteamMemberIDArr[0]['teamId'] === NULL)) {
                            $dataArr = array();
                            $dataArr['teamMemberID'] = $tmpteamMemberIDArr[0]['teamMemberID'];
                            $dataArr['team_id'] = $teamID;
                            $updateInsertStatus = $this->updateField($dataArr);
                            $userIdArr = array();
                            $userIdArr[] = $user_id;
                            $deleteLoginDeviceUser = $this->deleteLoginDeviceUser($userIdArr);
                        }
                    }
                }
            }
        }
        return $updateInsertStatus;
    }

    public function getTeam($company_id) {
        return TeamMember::with('user', 'designation', 'userdepartment.department')
                        ->where('company_id', $company_id)
                        ->where('team_id', '!=', 'NULL')
                        ->orderBy('assign_at', 'desc')
                        ->get();
    }

    public function deleteTeam($teamid, $date) {
        $userIdArr = array();
        $teamDetail = TeamMember::where('team_id', $teamid)
                ->get();
        $deleteproblemArr = array();
        foreach ($teamDetail as $team) {
            $userIdArr[] = $team->user_id;
            $data[] = array('team_id' => $team->team_id, 'user_id' => $team->user_id, 'role' => $team->role, 'effective_action_id' => $team->effective_action_id);
        }

        TeamAssignmentHistory::insert($data);
        foreach ($teamDetail as $key => $value) {
            $tmpCount = $this->getTeamCount((int) $value['role'], $value['user_id'], $teamid);
            $id = TeamMember::where('user_id', $value['user_id'])
                    ->where('role', $value['role'])
                    ->where('team_id', $teamid)
                    ->update(['team_id' => NULL, 'assign_at' => NULL, 'effective_action_id' => NULL, 'effective_action_description' => NULL]);
            $deleteproblemArr[$key] = $id;
            $pushNotificationDataArr = array();
            $pushNotificationDataArr['user_id'] = $value['user_id'];
            $pushNotificationDataArr['team_id'] = $teamid;
            $this->deletePushNotificationUser($pushNotificationDataArr);

            if (($tmpCount === 0) && ($value['effective_action_id'] > 0)) {
                $this->insertPastEffectiveAction($value['user_id'], $value['effective_action_id'], $value['created_at'], $value['effective_action_description']);
            }
        }
        foreach ($deleteproblemArr as $key => $statuscode) {
            if ($statuscode === 0) {
                return false;
            }
        }
        $this->deleteLoginDeviceUser($userIdArr);
        return true;
    }

    public function getTeamID($teammanagerid) {
        $teamiddata = TeamMember::where('user_id', $teammanagerid)->where('role', static::MANAGER_ROLE_ID)->first();
        return $teamiddata->teamId;
    }

    public function deleteSubordinate($userid, $role) {
        $pushNotificationDataArr = array();
        $userTeamData = TeamMember::select('team_id', 'effective_action_id', 'created_at', 'effective_action_description')
                ->where('user_id', $userid)
                ->where('role', $role)
                ->first();
        if (is_object($userTeamData)) {
            $data = array('team_id' => $userTeamData->team_id,
                'user_id' => $userid,
                'role' => $role,
                'effective_action_id' => $userTeamData->effective_action_id);
            $pushNotificationDataArr['user_id'] = $userid;
            $pushNotificationDataArr['team_id'] = $userTeamData->team_id;
            TeamAssignmentHistory::insert($data);
            $tmpCount = $this->getTeamCount((int) $role, $userid, $userTeamData->team_id);

            if (($tmpCount === 0) && ($userTeamData->effective_action_id > 0)) {
                $this->insertPastEffectiveAction($userid, $userTeamData->effective_action_id, $userTeamData->created_at, $userTeamData->effective_action_description);
            }
        }
        $deletestatus = TeamMember::where('user_id', $userid)
                ->where('role', $role)
                ->update(['team_id' => NULL, 'assign_at' => NULL, 'effective_action_id' => NULL, 'effective_action_description' => NULL]);
        $userIdArr[] = $userid;
        $this->deleteLoginDeviceUser($userIdArr);
        $this->deletePushNotificationUser($pushNotificationDataArr);
        return $deletestatus;
    }

    public function getManagerDashboard($postData) {
        $userId = $postData['userId'];
        $pageNo = $postData['pageNo'];
        $limit = Config::get('constants.limit');
        $skip = ($pageNo - 1) * $limit;

        $checkManagers = TeamMember::select('team_id')
                ->where('user_id', $userId)
                ->whereNotNull('team_id')
                ->where('role', static::MANAGER_ROLE_ID)
                ->get();

        if (is_object($checkManagers) && count($checkManagers) > 0) {
            foreach ($checkManagers as $checkManager) {
                $teams[] = $checkManager->team_id;
            }

            $members = TeamMember::with('user', 'designation', 'effective')
                    ->whereIn('team_id', $teams)
                    ->where('effective_action_id', '!=', 'NULL')
                    ->groupBy('user_id');
        } else {
            return 0;
        }
        $membersCount = $members->count();
        $totalPages = ceil($membersCount / $limit);
        $members = $members->orderBy('role', 'asc')->orderBy('last_activity_time', 'desc')->skip($skip)->take($limit)->get();
        foreach ($members as $member) {
            if ($member->effectiveActionId != NULL) {
                $effectiveActionId = $member->effectiveActionId;
                $subordinateId = $member->user['userId'];
                $eaCheck = array('flag' => 1,
                    'startTime' => $member->ea_assign_at,
                    'endTime' => ''
                );
                $effectiveActionSummary = Users::getEffectiveSummary($subordinateId, $effectiveActionId, $eaCheck);
                if ($effectiveActionSummary['AVG'] != 0) {
                    $effectiveActionSummary['AVG'] = round($effectiveActionSummary['AVG']);
                    $avgArray = array(
                        'adhrenceCount' => $effectiveActionSummary['AVG'],
                        'days' => 'Avg');
                    $graphSql = Users::getGraph($subordinateId, $effectiveActionId, $eaCheck);
                    $graph = DB::select($graphSql . ' order by a.work_day desc limit 7');
                    $graph[] = $avgArray;
                    $member->graph = array_reverse($graph);
                    $member->effectiveActionSummary = $effectiveActionSummary;
                } else {
                    $member->effectiveActionSummary = $effectiveActionSummary;
                    $member->graph = array();
                }
            }
        }
        $arr['pageNo'] = $pageNo;
        $arr['totalPages'] = $totalPages;
        $arr['members'] = $members;
        if ($pageNo > $totalPages) {
            return array('status' => 'true', 'result' => $arr, 'message' => trans('messages.no_more_data_not_found'));
        } else {
            return array('status' => 'true', 'result' => $arr, 'message' => '');
        }
    }

    public function getUserCompanyID($userid) {
        $userdata = TeamMember::select('company_id')->where('user_id', $userid)->first();
        if (count($userdata) > 0) {
            return $userdata->company_id;
        } else {
            return 0;
        }
    }

    public function getUserCompanyIDArr($userIdArr) {
        return TeamMember::select('id', 'user_id', 'company_id')->whereIn('user_id', $userIdArr)
                        ->get()->keyBy('user_id')->toArray();
    }

    public function getAssignedTeamID($userid, $role) {
        $teamiddata = TeamMember::select('id', 'team_id')->where('user_id', $userid)->where('role', $role)->orderBy('team_id', 'desc')->first();
        if (count($teamiddata) > 0) {
            $returnArr['teamId'] = $teamiddata->team_id;
            $returnArr['id'] = $teamiddata->id;
            return $returnArr;
        } else {
            return array();
        }
    }

    public function updateField($dataArr) {
        /* will be used at the time of doing any activity
          related to team means [creating, updating, deleting] team, [assign, unassign] subordinate */
        $t = microtime(true);
        $micro = sprintf("%06d", ($t - floor($t)) * 1000000);
        $dt = date('Y-m-d H:i:s.' . $micro, $t);
        $team_id = isset($dataArr['team_id']) ? $dataArr['team_id'] : 0;
        $teamMemberID = isset($dataArr['teamMemberID']) ? $dataArr['teamMemberID'] : 0;
        $action = isset($dataArr['action']) ? $dataArr['action'] : '';
        $effective_action_id = isset($dataArr['effective_action_id']) ? $dataArr['effective_action_id'] : NULL;
        $effective_action_description = isset($dataArr['effective_action_description']) ? $dataArr['effective_action_description'] : NULL;
        if ($teamMemberID > 0) {
            return $updateInsertStatus = TeamMember::where('id', $teamMemberID)
                    ->update(['team_id' => $team_id, 'assign_at' => $dt, 'effective_action_id' => $effective_action_id, 'effective_action_description' => $effective_action_description]);
        } else {
            if ($team_id === 0) {
                $deleteStatus = TeamMember::where('user_id', $oldmanagerid)
                        ->where('role', static::MANAGER_ROLE_ID)
                        ->update(['team_id' => NULL, 'assign_at' => NULL, 'effective_action_id' => NULL, 'effective_action_description' => NULL]);
            } else {

                $oldmanagerid = $dataArr['oldmanagerid'];
                if ($action == 'changemanager') {
                    $managerData = TeamMember::select('effective_action_id', 'effective_action_description', 'created_at')->where('user_id', $oldmanagerid)
                                    ->where('team_id', $team_id)->first();
                    TeamMember::where('user_id', $oldmanagerid)
                            ->where('team_id', $team_id)
                            ->update(['team_id' => NULL, 'assign_at' => NULL, 'effective_action_id' => NULL, 'effective_action_description' => NULL]);
                    $userIdArr[] = $oldmanagerid;
                    $deleteStatus = $this->deleteLoginDeviceUser($userIdArr);
                    $pushNotificationDataArr = array();
                    $pushNotificationDataArr['user_id'] = $oldmanagerid;
                    $pushNotificationDataArr['team_id'] = $team_id;
                    $this->deletePushNotificationUser($pushNotificationDataArr);
                    if (isset($managerData->effective_action_id) && is_object($managerData)) {
                        $this->insertPastEffectiveAction($oldmanagerid, $managerData->effective_action_id, $managerData->created_at->format('Y-m-d H:i:s'), $managerData->effective_action_description);
                    }

                    $user_id = isset($dataArr['user_id']) ? $dataArr['user_id'] : 0;
                    $role = isset($dataArr['role']) ? $dataArr['role'] : 0;
                    return $updateInsertStatus = TeamMember::where('user_id', $user_id)
                            ->where('role', $role)
                            ->update(['team_id' => $team_id, 'assign_at' => $dt, 'effective_action_id' => $effective_action_id, 'effective_action_description' => $effective_action_description]);
                }
            }
        }
    }

    public function insertField($dataArr) {
        return TeamMember::insertGetId($dataArr);
    }

    public function deleteLoginDeviceUser($userIdArr) {
        return Device::whereIn('user_id', $userIdArr)->delete();
    }

    public function deletePushNotificationUser($dataArr) {
        $user_id = isset($dataArr['user_id']) ? $dataArr['user_id'] : 0;
        $team_id = isset($dataArr['team_id']) ? $dataArr['team_id'] : 0;
        return PushNotification::where('user_id', $user_id)->where('team_id', $team_id)->delete();
    }

    public function insertPastEffectiveAction($userid, $effective_action_id, $created_at, $effective_action_description) {
        return PastEffectiveAction::insert(array('user_id' => $userid,
                    'effective_action_id' => $effective_action_id,
                    'start_date' => $created_at,
                    'effective_action_description' => $effective_action_description
        ));
    }

    public function getTeamCount($role, $user_id, $teamid) {
        $tmpCount = 0;
        if ($role === static::MANAGER_ROLE_ID) {
            $tmpCount = TeamMember::where('user_id', $user_id)
                    ->where('team_id', '!=', $teamid)
                    ->whereNotNull('team_id')
                    ->count();
        }
        return $tmpCount;
    }

    public function getEADetail($user_id, $rollID) {
        $tempteamMemberEADetail = TeamMember::select('effective_action_id', 'effective_action_description')->where('user_id', $user_id)
                        ->where('role', $rollID)
                        ->whereNotNull('team_id')->get()->toArray();

        if (count($tempteamMemberEADetail) > 0) {
            $eaArr['effective_action_id'] = $tempteamMemberEADetail[0]['effectiveActionId'];
            $eaArr['effective_action_description'] = $tempteamMemberEADetail[0]['effectiveActionDescription'];
        } else {
            $eaArr['effective_action_id'] = NULL;
            $eaArr['effective_action_description'] = NULL;
        }

        return $eaArr;
    }

}
