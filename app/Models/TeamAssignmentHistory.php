<?php

namespace App\Models;

use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;
use Illuminate\Database\Eloquent\SoftDeletes;

class TeamAssignmentHistory extends \Eloquent {

    use Eloquence,
        Mappable;

use SoftDeletes;

    protected $table = 'team_assignment_history';
    protected $primaryKey = 'id';
    protected $maps = [
        'teamId' => 'team_id',
        'userId' => 'user_id',
    ];
    protected $hidden = ['id', 'user_id', 'deleted_at', 'created_at', 'updated_at', 'team_id'];
    protected $fillable = ['user_id', 'team_id'];
    protected $appends = [ 'teamId', 'userId'];
    protected $dates = ['deleted_at'];
    protected $casts = ['team_id' => 'integer',];

}
