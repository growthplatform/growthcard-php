<?php

namespace App\Models;

use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Images extends \Eloquent {

    use Eloquence,
        Mappable;

use SoftDeletes;

    protected $table = 'images';
    protected $primaryKey = 'id';

    public function __construct() {
        parent:: __construct();
    }

    public function getAllImages($companyId, $user_id) {
        return static::select('id', 'name', 'caption', 'message')->where("created_by", $user_id)->where("company_id", $companyId)->get()->toArray();
    }

    public function getRandomImages($companyId) {
        return static::select('name', 'caption', 'message')->where("company_id", $companyId)->orderByRaw('RAND()')->take(1)->get()->toArray();
    }

    public function updateField($dataArr, $id) {
        if ($id > 0) {
            return static::where('id', $id)
                    ->update($dataArr);
        }
        return $id;
    }

    public function insertField($dataArr) {
        return static::insertGetId($dataArr);
    }
    
    public function getNameAttribute() {
        $fullImagePath = '';
        if(!empty($this->attributes['name'])){            
            $fullImagePath = \App\Utility\CommonMethod::get($this->attributes['name'], 'image');
        }

        return $fullImagePath;
    }

}
