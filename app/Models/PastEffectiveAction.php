<?php

namespace App\Models;

use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Config;
use Illuminate\Support\Facades\DB;

class PastEffectiveAction extends \Eloquent {

    use Eloquence,
        Mappable;

use SoftDeletes;

    protected $table = 'past_effective_action';
    protected $primaryKey = 'id';
    protected $maps = [
        'effectiveActionId' => 'effective_action_id',
        'startDate' => 'start_date',
        'endDate' => 'created_at',
        'effectiveActionDescription' => 'effective_action_description',
        'pastEffectiveActionId' => 'id'
    ];
    protected $hidden = ['id', 'user_id', 'start_date', 'created_at', 'effective_action_id', 'updated_at', 'deleted_at', 'effective_action_description'];
    protected $fillable = [];
    protected $appends = ['effectiveActionId', 'startDate', 'endDate', 'effectiveActionDescription','pastEffectiveActionId'];
    protected $dates = ['deleted_At'];
    protected $casts = [];

    public function getPastCustomEffectiveAction($postData) {
        $subordinateId = $postData['subordinateId'];
        $limit = Config::get('constants.limit');
        $pageNo = $postData['pageNo'];
        $skip = ($pageNo - 1) * $limit;
        $flag = $postData['flag'];
        $getPastEffectiveAction = PastEffectiveAction::with('effectiveaction')->where('user_id', $subordinateId);
        $countGetPastEffectiveAction = $getPastEffectiveAction->count();
        $totalPages = ceil($countGetPastEffectiveAction / $limit);
        if ($flag == 1) {
            $pastEffectiveAction = $getPastEffectiveAction
                    ->skip($skip)->take($limit)
                    ->orderBy('effective_action_description', 'asc')
                    ->get();
        } else {
            $pastEffectiveAction = $getPastEffectiveAction
                    ->orderBy('updated_at', 'desc')
                    ->skip($skip)->take($limit)
                    ->get();
        }

        foreach ($pastEffectiveAction as $action) {
            $effectiveActionId = $action->effective_action_id;
            $eaCheck = array('flag' => 2,
                'startTime' => $action->start_date,
                'endTime' => $action->created_at,
            );
            $summary = Users::getEffectiveSummary($subordinateId, $effectiveActionId, $eaCheck);
            if ($summary['AVG'] != 0) {
                $summary['AVG'] = round($summary['AVG']);
                $avgArray = array(
                    'adhrenceCount' => $summary['AVG'],
                    'days' => 'Avg');
                $graphSql = Users::getGraph($subordinateId, $effectiveActionId, $eaCheck);
                $graph =  DB::select($graphSql . ' order by a.work_day desc limit 7');
                array_push($graph, $avgArray);
                $action->graph = $graph;
                $action->effectiveActionSummary = $summary;
            } else {
                $action->graph = array();
            }
            $feedLike = Feed::select('like_count')->where('user_id', $subordinateId)
                    ->where('effective_action_id', $effectiveActionId)
                    ->whereIn('type', [3, 4])
                    ->groupBy('effective_action_id')
                    ->groupBy('user_id')
                    ->sum('like_count');
            if (!isset($feedLike)) {
                $feedLike = 0;
            }
            $action->likeCount = $feedLike;
        }

        $arr['pageNo'] = $pageNo;
        $arr['totalPages'] = $totalPages;
        $arr['pastEffectiveAction'] = $pastEffectiveAction;
        if ($pageNo > $totalPages) {
            return array('status' => 'true', 'result' => $arr, 'message' => trans('messages.no_more_data_not_found'));
        } else {
            return array('status' => 'true', 'result' => $arr, 'message' => '');
        }
    }

    public function effectiveaction() {
        return PastEffectiveAction::belongsTo('App\Models\EffectiveAction', 'effective_action_id');
    }

    public function deletePastCustomEffectiveAction($postData) {
        $date = date('Y-m-d H:i:s');
        $effectiveActionId = $postData['effectiveActionId'];
        $subordinateId = $postData['subordinateId'];
        $teamId = $postData['teamId'];
        $userId = $postData['userId'];
        $checkManager = TeamMember::checkTeamManager($userId, $teamId);
        if ($checkManager == 0) {
            return 0;
        }
        PastEffectiveAction::where('user_id', $subordinateId)
                ->where('effective_action_id', $effectiveActionId)
                ->update(['deleted_at' => $date]);
        return 1;
    }

}
