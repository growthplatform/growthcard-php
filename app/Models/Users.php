<?php

namespace App\Models;

use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Config;
use Auth;
use Hash;
use App\Models\UserWorkDay;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Users extends \Eloquent {

    use Eloquence,
        Mappable;

use SoftDeletes;

    protected $table = 'users';
    protected $primaryKey = 'id';
    protected $maps = [
        'userId' => 'id',
        'firstName' => 'first_name',
        'lastName' => 'last_name',
        'profileImage' => 'profile_image',
        'termsAccepted' => 'terms_accepted',
    ];
    protected $hidden = ['id', 'first_name', 'last_name', 'password', 'profile_image',
        'terms_accepted', 'user_type', 'remember_token', 'deleted_at',
        'created_at', 'updated_at', 'created_by', 'updated_by', 'recent_announcement'];
    protected $fillable = ['first_name', 'last_name', 'email', 'user_type'];
    protected $appends = ['userId', 'firstName', 'lastName', 'profileImage',
        'termsAccepted'];
    protected $dates = ['deleted_at'];
    protected $casts = ['terms_accepted' => 'integer', 'first_name' => 'string', 'last_name' => 'string'];

    public function __construct() {
        parent:: __construct();
        $this->user_work_day = new UserWorkDay();
    }

    public static function getUserAllData() {
        return Users::with('member', 'member.company', 'member.designation', 'member.effective', 'days', 'userdepartment.department');
    }

    public function member() {
        return $this->belongsTo('App\Models\TeamMember', 'id', 'user_id');
    }

    public function days() {
        return $this->hasMany('App\Models\UserWorkDay', 'user_id');
    }

    public function userdepartment() {
        return $this->belongsTo('App\Models\UserDepartment', 'id', 'user_id');
    }

    public function getUserAllDataWeb() {

        return Users::with('memberweb', 'member.company', 'member.designation', 'member.effective', 'days', 'userdepartment.department');
    }

    public function memberweb() {
        return $this->hasMany('App\Models\TeamMember', 'user_id');
    }

    public function findUserRoleInTeam($user_id) {
        return TeamMember::select('role')->where('user_id', $user_id)
                        ->whereNotNull('team_id')
                        ->groupBy('role');
    }

    public function updateTermsStatus($user_id) {
        $user = Users::find($user_id);
        $user->terms_accepted = 1;
        $user->save();
        return;
    }

    public function updateUserProfile($postData) {
        $user_id = $postData['userId'];
        $email = isset($postData['email']) ? $postData['email'] : '';
        $first_name = isset($postData['firstName']) ? $postData['firstName'] : '';
        $last_name = isset($postData['lastName']) ? $postData['lastName'] : '';
        $user = Users::find($user_id);
        if ($first_name != '') {
            $user->first_name = $first_name;
        }
        if ($last_name != '') {
            $user->last_name = $last_name;
        }
        if ($email != '') {
            $user->email = $email;
        }
        $user->save();
        if (isset($postData['workDay']) && $postData['workDay'] != '') {
            $workDay = str_split($postData['workDay']);
            $this->user_work_day->saveUserWorkDays($user_id, $workDay);
        }
        return;
    }

    public function getProfileImageAttribute() {
        if ($this->attributes['profile_image']) {
            $imageUrl = env('SERVER_ROOT') . env('PROFILE_IMAGE') . $this->attributes['profile_image'];
            return $imageUrl;
        }
        return url('/images/profile_default.png');
    }

    public function getUserNamePassword() {
        $user_id = Auth::User()->id;
        $user = Users::select('first_name', 'last_name', 'email')->where('id', $user_id)->first();
        return $user;
    }

    public function updatePassword($password, $user_id) {
        $newPassword = Hash::make($password);
        Users::where('id', $user_id)->update(array('password' => $newPassword));
        $this->message = trans('messages.password_updated');
        return;
    }

    public function createUser($inputs) {
        $firstName = $inputs['firstName'];
        $lastName = $inputs['lastName'];
        $password = $inputs['password'];
        $hashPassword = $this->hashPassword($password);
        $email = $inputs['email'];
        $userType = 3; // 3 for user
        $createdBy = $updatedBy = Auth::user()->id;
        $userData = array('first_name' => $firstName,
            'last_name' => $lastName,
            'password' => $hashPassword,
            'email' => $email,
            'user_type' => $userType,
            'created_by' => $createdBy,
            'updated_by' => $updatedBy
        );
        $userId = Users::insertGetId($userData);
        $companyId = \Session::get('companyId');
        $departmentId = $inputs['department'];
        $designationId = $inputs['job'];
        $userDepartment = array('user_id' => $userId, 'department_id' => $departmentId);
        UserDepartment::insertGetId($userDepartment);
        $teamMember = array('user_id' => $userId, 'designation_id' => $designationId, 'company_id' => $companyId, 'effective_action_description' => '');
        TeamMember::insertGetId($teamMember);
        return;
    }

    public function updateUser($inputs) {
        $firstName = $inputs['firstName'];
        $lastName = $inputs['lastName'];
        $password = $inputs['password'];
        $userId = $inputs['userId'];
        $hashPassword = $this->hashPassword($password);
        $email = $inputs['email'];
        $updatedBy = Auth::user()->id;
        $user = Users::findOrFail($userId);
        $user->first_name = $firstName;
        $user->last_name = $lastName;
        $user->email = $email;
        $user->password = $hashPassword;
        $user->updated_by = $updatedBy;
        $user->save();
        $departmentId = $inputs['department'];
        $designationId = $inputs['job'];
        UserDepartment::where('user_id', $userId)->update(['department_id' => $departmentId]);
        TeamMember::where('user_id', $userId)->update(['designation_id' => $designationId]);
        Feed::where('created_by', $userId)->update(['department_id' => $departmentId]);
        return;
    }

    public function destroyUser($id, $flag) {
        $checkManager = TeamMember::select('id', 'team_id')->where('user_id', $id)->where('team_id', '!=', 'NULL')->where('role', 1)->first();
        if ($flag == 0) {
            if (is_object($checkManager) && isset($checkManager->id)) {
                return 1;
            } else {
                $this->deletetUser($id, $checkManager);
            }
        } else {
            $this->deletetUser($id, $checkManager);
        }
    }

    public function deletetUser($id, $checkManager) {
        $date = date('Y-m-d H:i:s');
        $user = Users::findOrFail($id);
        $user->deleted_at = $date;
        if (is_object($checkManager) && isset($checkManager->id)) {
            TeamMember::where('team_id', $checkManager->team_id)->update(['team_id' => NULL, 'assign_at' => NULL]);
        }
        UserDepartment::where('user_id', $id)->update(['deleted_at' => $date]);
        UserWorkDay::where('user_id', $id)->update(['deleted_at' => $date]);
        TeamMember::where('user_id', $id)->update(['deleted_at' => $date, 'assign_at' => NULL]);
        Feed::where('user_id', $id)->update(['deleted_at' => $date]);
        PushNotification::where('user_id', $id)->delete();
        \Session::forget('userDelete');
        $user->save();
        return 2;
    }

    public function getSubordinateProfile($postData) {
        $teamId = $postData['teamId'];
        $subordinateId = $postData['subordinateId'];
        $companyId = $postData['companyId'];
        $effectiveAction = TeamMember::select('effective_action_id', 'created_at', 'ea_assign_at')
                ->where('team_id', $teamId)
                ->where('user_id', $subordinateId)
                ->first();
        $effectiveActionId = '';
        if (is_object($effectiveAction)) {
            $effectiveActionId = $effectiveAction->effective_action_id;
        }
        $user = Users::where('id', $subordinateId)
                ->with(['member' => function($q) use($teamId, $companyId) {
                        $q->where('team_id', $teamId);
                        $q->where('company_id', $companyId);
                    }, 'member.company', 'member.designation', 'member.effective', 'days', 'userdepartment.department'])
                ->first();
        if ($effectiveActionId != '') {
            $eaCheck = array('flag' => 1,
                'startTime' => $effectiveAction->ea_assign_at,
                'endTime' => ''
            );
            $effectiveActionSummary = $this->getEffectiveSummary($subordinateId, $effectiveActionId, $eaCheck);
            if ($effectiveActionSummary['AVG'] != 0) {
                $effectiveActionSummary['AVG'] = (int) round($effectiveActionSummary['AVG']);
                $avgArray = array(
                    'adhrenceCount' => $effectiveActionSummary['AVG'],
                    'days' => 'Avg');
                $graphSql = $this->getGraph($subordinateId, $effectiveActionId, $eaCheck);
                $graph = DB::select($graphSql . ' order by a.work_day desc limit 7');
                $graph[] = $avgArray;
                $user['graph'] = array_reverse($graph);
            } else {
                $user['graph'] = array();
            }
            $user['effectiveActionSummary'] = $effectiveActionSummary;
        }
        if (is_object($user)) {
            return $user;
        } else {
            return '';
        }
    }

    public static function getGraph($subordinateId, $effectiveActionId, $eaCheck) {
        $date = date('Y-m-d H:i:s');
        $flag = $eaCheck['flag'];
        $startTime = $eaCheck['startTime'];
        $endTime = $eaCheck['endTime'];
        if ($flag == 1) {
            $endTime = $date;
        }

        $history = "SELECT a.work_text as days, CASE WHEN a.is_selected =1 THEN count(b.id) WHEN count(b.id) > 0
                THEN count(b.id) end as adhrenceCount FROM growthcard.user_work_day as a "
                . "left join growthcard.user_effective_action_history as b on b.work_day = a.work_day and "
                . "b.user_id = $subordinateId and b.effective_action_id = $effectiveActionId and "
                . "b.created_at >= '$startTime' and b.created_date <= '$endTime' where "
                . "a.user_id = $subordinateId and a.deleted_at is null group by a.work_day having adhrenceCount is not null";
        return $history;
    }

    public static function getEffectiveSummary($subordinateId, $effectiveActionId, $eaCheck) {
        $flag = $eaCheck['flag'];
        $startTime = $eaCheck['startTime'];
        $endTime = $eaCheck['endTime'];
        $userDays = UserWorkDay::where('user_id', $subordinateId)->where('is_selected', 1)->get()->toArray();
        $adhrence = UserEffectiveActionHistory::
                        select(DB::raw("COUNT('user_effective_action_history.work_day') as count_w"), 'user_id', 'work_day', 'created_date')
                        ->where('user_id', $subordinateId)->where('effective_action_id', $effectiveActionId)
                        ->groupBy('work_day')->groupBy('created_date');
        if ($flag == 1) {
            $adhrence = $adhrence->where('user_effective_action_history.created_at', '>=', $startTime)->get()->toArray();
        } else {
            $adhrence = $adhrence->where('user_effective_action_history.created_at', '>=', $startTime)
                            ->where('user_effective_action_history.created_at', '<=', $endTime)
                            ->get()->toArray();
        }
        $effectiveActionSummary = array('MIN' => 0, 'MAX' => 0, 'AVG' => 0, 'TODAY' => 0);
        $userSelectedDays = array_column($userDays, 'workDay');
        $work = array_column($adhrence, 'count_w');
        $userWorkedDays = array_column($adhrence, 'work_day');
        $totalDays = array_unique(array_merge($userSelectedDays, $userWorkedDays));
        if (!empty($totalDays)) {
            //$averageWork = round(array_sum($work) / count($totalDays));
            $averageWork = array_sum($work) / count($totalDays);
        }
        if (!empty($work)) {
            $minWork = min($work);
            $maxWork = max($work);
        }
        $key = array_search(date('Y-m-d'), array_column($adhrence, 'created_date'));
        if (!empty($minWork)) {
            $effectiveActionSummary['MIN'] = $minWork;
        }
        if (!empty($maxWork)) {
            $effectiveActionSummary['MAX'] = $maxWork;
        }
        if (!empty($averageWork)) {
            //$effectiveActionSummary['AVG'] = (int) $averageWork;
            $effectiveActionSummary['AVG'] = $averageWork;
        }
        if (!empty($adhrence) && isset($key)) {
            $effectiveActionSummary['TODAY'] = $adhrence[$key]['count_w'];
        }
        return $effectiveActionSummary;
    }

    public function searchUser($postData) {
        $searchText = $postData['searchText'];
        $companyId = $postData['companyId'];
        $userId = $postData['userId'];
        $teamId = $postData['teamId'];

        $checkManager = TeamMember::checkTeamManager($userId, $teamId);
        if ($checkManager == 0) {
            return false;
        } else {
            $limit = Config::get('constants.limit');
            $pageNo = $postData['pageNo'];
            $skip = ($pageNo - 1) * $limit;
            $getUsers = Users::with('member', 'member.designation')
                    ->where('first_name', 'LIKE', "%$searchText%")
                    ->where('id', '!=', $userId)
                    ->whereHas('member', function($q) use($companyId) {
                // Query the name field in team_member table
                $q->where('company_id', $companyId);
            });
            $usersCount = count($getUsers->get());
            $totalPages = ceil($usersCount / $limit);
            $users = $getUsers->orderBy('updated_at', 'desc')->skip($skip)->take($limit)->get();
            $arr['pageNo'] = $pageNo;
            $arr['totalPages'] = $totalPages;
            $arr['users'] = $users;
            if ($pageNo > $totalPages) {
                return array('status' => 'true', 'result' => $arr, 'message' => trans('messages.no_more_data_not_found'));
            } else {
                return array('status' => 'true', 'result' => $arr, 'message' => '');
            }
        }
    }

    function switchProfile($postData) {
        $flag = $postData['flag'];
        $userId = $postData['userId'];
        $userToken = $postData['userToken'];
        $teamId = TeamMember::select('team_id')->where('user_id', $userId)->where('role', 2)->first();
        if ($teamId->team_id === 'NULL' || $teamId->team_id == '') {
            $user = Users::where('id', $userId)
                    ->with(['member' => function($q) {
                            $q->where('role', 1);
                            $q->where('team_id', '!=', 'NULL');
                        }, 'member.company', 'member.designation', 'member.effective', 'days', 'userdepartment.department'])
                    ->first();
            $user->role = $flag;
        } else {
            $user = Users::where('id', $userId)
                    ->with(['member' => function($q) use($flag) {
                            if ($flag == 1) {
                                $q->where('role', 1);
                            } else {
                                $q->where('role', 2);
                            }
                            $q->where('team_id', '!=', 'NULL');
                        }, 'member.company', 'member.designation', 'member.effective', 'days', 'userdepartment.department'])
                    ->first();
            $user->role = $user->member['role'];
        }
        $user->userToken = $userToken;
        $user->switch = 1;
        return $user;
    }

    public function getUserID($email) {
        $userdata = Users::select('id')->where('email', $email)->first();
        if (count($userdata) > 0) {
            return $userdata->id;
        } else {
            return 0;
        }
    }

    public function getUserIDArr($emailArr, $key = 'id') {
        $returnArr = array();
        $data = Users::select('id', 'email', 'profileImage')->whereIn('email', $emailArr)
                        ->get()->keyBy($key)->toArray();

        foreach ($data as $keyVal => $value) {
            if ($key == 'id') {
                $returnArr[$keyVal] = $value['email'];
            } else if ($key == 'email') {
                $returnArr[$keyVal] = $value['userId'];
            }
        }
        return $returnArr;
    }

    public function forgotPasswordMail($email, $user, $password) {
        $emailBodyArr = array
            (
            'name' => $user->first_name . ' ' . $user->last_name,
            'newpass' => $password,
            'email' => $email,
        );
        try {
            Mail::send('resendPassword', $emailBodyArr, function($message) use ($user) {
                $message->to($user->email, $user->first_name)->subject('GrowthCard App Password Recovery.');
                $message->from('no-reply@growthcard.com');
            });
        } catch (SmtpException $e) {
            Log::error(['method' => __METHOD__, 'error' => ['file' => $e->getFile(), 'line' => $e->getLine(), 'message' => $e->getMessage()], 'created_at' => date("Y-m-d H:i:s")]);
            return 0;
        }
    }

    private function hashPassword($password) {
        return Hash::make($password);
    }

}
