<?php

namespace App\Models;

use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;
use Illuminate\Database\Eloquent\Model;

class Device extends Model {

    use Eloquence,
        Mappable;

    protected $table = 'user_device_token';
    protected $primaryKey = 'id';
    protected $maps = [
        'userId' => 'user_id',
        'userToken' => 'user_token',
        'deviceType' => 'device_type',
        'deviceId' => 'device_id',
        'deviceToken' => 'device_token',
    ];
    protected $fillable = ['device_id', 'user_id', 'device_token', 'device_type', 'user_token'];
    protected $sounds = array('10' => 'default');
    protected $tokens = [];
    protected $casts = ['user_token' => 'string'];

    public static function unRegister_single($request) {
        Device::where('user_id', $request->input('user_id'))->where('device_id', $request->input('deviceId'))->delete();
        return true;
    }

    public static function unRegister_all($user_id) {
        Device::where('user_id', $user_id)->delete();
        return true;
    }

    public function register_device($device_id, $user_id, $device_token, $device_type) {
        $user_token = md5($device_id);
        $data = array('device_id' => $device_id,
            'user_id' => $user_id,
            'device_token' => $device_token,
            'device_type' => $device_type,
            'user_token' => $user_token);
        // insert an entry to device table for user authentication
        Device::firstOrCreate($data);
        return $user_token;
    }

}
