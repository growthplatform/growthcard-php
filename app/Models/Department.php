<?php

namespace App\Models;

use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Department extends \Eloquent {

    use Eloquence,
        Mappable;

use SoftDeletes;

    protected $table = 'department';
    protected $primaryKey = 'id';
    protected $maps = [
        'departmentId' => 'id',
        'departmentName' => 'department_name',
        'companyId' => 'company_id',
    ];
    protected $hidden = ['id', 'name', 'company_id', 'deleted_at', 'department_name',
        'created_at', 'updated_at', 'created_by', 'updated_by'];
    protected $fillable = ['name', 'company_id',];
    protected $appends = ['departmentId', 'departmentName'];
    protected $dates = ['deleted_at'];

    public function createDepartment($inputs) {
        $deparmentName = $inputs['departmentName'];
        $companyId = \Session::get('companyId');
        $createdBy = $updatedBy = Auth::user()->id;
        $userData = array(
            'department_name' => $deparmentName,
            'company_id' => $companyId,
            'created_by' => $createdBy,
            'updated_by' => $updatedBy
        );
        Department::insertGetId($userData);
        return;
    }

    public function getDepartmentID($title, $companyId) {
        $departmentdata = Department::select('id')->where('department_name', $title)->where('company_id', $companyId)->first();
        if (count($departmentdata) > 0) {
            return $departmentdata->id;
        } else {
            return 0;
        }
    }

}
