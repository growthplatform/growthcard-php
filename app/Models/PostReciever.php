<?php

namespace App\Models;

use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;
use Illuminate\Database\Eloquent\SoftDeletes;

class PostReciever extends \Eloquent {

    use Eloquence,
        Mappable;

use SoftDeletes;

    protected $table = 'post_reciever';
    protected $primaryKey = 'id';
    protected $maps = [
        'postId' => 'post_id',
        'userId' => 'user_id',
    ];
    protected $hidden = ['id', 'user_id', 'updated_at', 'deleted_at', 'created_at', 'post_id'];
    protected $appends = ['postId'];
    protected $dates = ['deleted_at'];

    public function __construct() {
        parent:: __construct();
    }

}
