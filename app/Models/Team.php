<?php

namespace App\Models;

use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class Team extends \Eloquent {

    use Eloquence,
        Mappable;

use SoftDeletes;

    protected $table = 'team';
    protected $primaryKey = 'id';
    protected $maps = [
        'teamId' => 'id',
        'companyId' => 'company_id',
    ];
    protected $hidden = ['id', 'company_id', 'deleted_at', 'created_at', 'updated_at',
        'created_by', 'updated_by'];
    protected $fillable = ['name', 'company_id'];
    protected $appends = ['teamId', 'companyId'];
    protected $dates = ['deleted_at'];
    protected $casts = ['id' => 'integer', 'company_id' => 'integer'];

    public function createTeam($inputsArr) {
        try {
            return Team::insertGetId($inputsArr);
        } catch (\Illuminate\Database\QueryException $e) {
            Log::error(['method' => __METHOD__, 'error' => ['file' => $e->getFile(), 'line' => $e->getLine(), 'message' => $e->getMessage()], 'created_at' => date("Y-m-d H:i:s")]);
            return 0;
        }
    }

    public function deleteTeam($teamid) {
        $date = date('Y-m-d H:i:s');
        $this->team_member = new TeamMember();
        $teammemberdeleteStatus = $this->team_member->deleteTeam($teamid, $date);
        if ($teammemberdeleteStatus === true) {
            $teamdeleteStatus = Team::where('id', $teamid)->update(['deleted_at' => $date]);
            if (isset($teamdeleteStatus)) {
                return 1;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

}
