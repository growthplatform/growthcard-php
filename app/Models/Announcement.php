<?php

namespace App\Models;

use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Config;

class Announcement extends \Eloquent {

    use Eloquence,
        Mappable;

use SoftDeletes;

    protected $table = 'announcement';
    protected $primaryKey = 'id';
    protected $maps = [
        'announcementId' => 'id',
        'createdAt' => 'created_at',
    ];
    protected $hidden = ['id', 'user_id', 'updated_at', 'deleted_at', 'created_at'];
    protected $fillable = ['subject', 'content', 'user_id',];
    protected $appends = ['announcementId', 'createdAt'];
    protected $dates = ['deleted_at'];

    public function __construct() {
        parent:: __construct();
    }

    public static function createAnnouncement($postData) {
        $subject = $postData['subject'];
        $content = $postData['content'];
        $userId = $postData['userId'];
        $teamId = $postData['teamId'];
        $companyId = $postData['companyId'];
        $flag = $postData['flag'];
        $checkManager = TeamMember::checkTeamManager($userId, $teamId);
        if ($checkManager == 0) {
            return 0;
        } else {
            $announcement = array('subject' => $subject,
                'content' => $content,
                'user_id' => $userId);
            $announcementId = Announcement::insertGetId($announcement);
            if ($flag == 0) {
                $recipients = $postData['senders'];
            } else {
                $getRecipients = Announcement::getAnnouncementRecipients($flag, $companyId, $teamId);
                foreach ($getRecipients as $recipient) {
                    $recipients[] = $recipient->id;
                }
            }
            if ($announcementId) {
                Users::whereIn('id', $recipients)->update(['recent_announcement' => $announcementId]);
            }
            $user = Users::select('first_name', 'last_name')->where('id', $userId)->first();
            $department = UserDepartment::select('department_id')->where('user_id', $userId)->first();
            foreach ($recipients as $recipient) {
                $data = array('announcement_id' => $announcementId,
                    'post_id' => NULL,
                    'effective_action_id' => NULL,
                    'company_id' => $companyId,
                    'team_id' => $teamId,
                    'user_id' => $recipient,
                    'type' => 1,
                    'department_id' => $department->department_id,
                    'created_by' => $userId
                );
                $receivers = array('user_id' => $recipient, 'announcement_id' => $announcementId);
                AnnouncementReciever::insertGetId($receivers);
                Feed::createFeed($data);
                if ($userId != $recipient) {
                    $payload['type'] = 5;
                    $userName = ucwords($user->first_name . ' ' . $user->last_name);
                    $payload['message'] = $userName . ' has posted a new anncouncement ' . $subject;
                    $payload['userId'] = $recipient;
                    $payload['teamId'] = $teamId;
                    $payload['effectiveActionId'] = NULL;
                    $payload['message1'] = $userName;
                    $payload['message2'] = ' has posted a new anncouncement ';
                    $payload['message3'] = $subject;
                    PushNotification::sendPushNotification($recipient, $payload);
                }
            }
        }
        return 1;
    }

    public static function getAnnouncementRecipients($flag, $companyId, $teamId) {
        return Users::select('id')->with('member')
                        ->whereHas('member', function($q) use($companyId, $flag, $teamId) {
                            // Query the name field in team_member table
                            if ($flag == 1 || $flag == 2 || $flag == 3) {
                                $q->where('company_id', $companyId);
                                $q->groupBy('user_id');
                            }
                            if ($flag == 2) {
                                $q->where('team_id', $teamId);
                                $q->where('role', 2);
                            }
                            if ($flag == 3) {
                                $q->where('team_id', '!=', 'NULL');
                                $q->where('role', 1);
                            }
                        })
                        ->get();
    }

    public static function getAnnouncement($postData) {        
        $teamId = $postData['teamId'];
        $userId = $postData['userId'];
        $limit = Config::get('constants.limit');
        $pageNo = $postData['pageNo'];
        $role = $postData['role'];
        $skip = ($pageNo - 1) * $limit;
        $teamManager = TeamMember::select('user_id')->where('team_id', $teamId)->where('role', 1)->first();
        $getAnnouncements = Announcement::with(['user', 'member.designation']);
        if ($role == 2) {
            $ids = AnnouncementReciever::select('announcement_id')->where('user_id', $userId)->get();
            foreach ($ids as $id) {
                $announcementIds[] = $id->announcement_id;
            }
            $getAnnouncements = $getAnnouncements->whereIn('id', $announcementIds);
        } else {
            $getAnnouncements = $getAnnouncements->where('user_id', $teamManager->user_id);
        }

        $announcementsCount = count($getAnnouncements->get());
        $totalPages = ceil($announcementsCount / $limit);
        $announcements = $getAnnouncements->orderBy('updated_at', 'desc')->skip($skip)->take($limit)->get();
        $arr['pageNo'] = $pageNo;
        $arr['totalPages'] = $totalPages;
        $arr['announcements'] = $announcements;
        if ($pageNo > $totalPages) {
            return array('status' => 'true', 'result' => $arr, 'message' => trans('messages.no_more_data_not_found'));
        } else {
            return array('status' => 'true', 'result' => $arr, 'message' => '');
        }
    }

    public function user() {
        return $this->belongsTo('App\Models\Users', 'user_id');
    }

    public function member() {
        return $this->belongsTo('App\Models\TeamMember', 'user_id', 'user_id');
    }

    public static function deleteAnnouncement($postData) {
        $date = date('Y-m-d H:i:s');
        $userId = $postData['userId'];
        $teamId = $postData['teamId'];
        $announcementId = $postData['announcementId'];
        $checkManager = TeamMember::checkTeamManager($userId, $teamId);
        if ($checkManager == 0) {
            return array('status' => 'false');
        } else {
            Users::where('recent_announcement', $announcementId)->update(['recent_announcement' => NULL]);
            Announcement::where('id',$announcementId)->update(['deleted_at' => $date]);
            AnnouncementReciever::where('announcement_id',$announcementId)->update(['deleted_at' => $date]);
            Feed::where('announcement_id', $announcementId)->update(['deleted_at' => $date]);
            return;
        }
    }

}
