<?php

namespace App\Models;

use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserWorkDay extends \Eloquent {

    use Eloquence,
        Mappable;

use SoftDeletes;

    protected $table = 'user_work_day';
    protected $primaryKey = 'id';
    protected $maps = [
        'workDayId' => 'id',
        'workDay' => 'work_day',
    ];
    protected $hidden = ['id', 'user_id', 'work_day', 'deleted_at', 'created_at', 'updated_at',];
    protected $fillable = ['user_id', 'work_day',];
    protected $appends = ['workDay'];
    protected $dates = ['deleted_at'];
    protected $casts = ['work_day' => 'integer'];

    public function saveUserWorkDays($user_id, $workDay) {
        $days = array(1 => 'Mon', 2 => 'Tue', 3 => 'Wed', 4 => 'Thu', 5 => 'Fri', 6 => 'Sat', 7 => 'Sun');
        for ($i = 0; $i < 7; $i++) {
            if ($workDay[$i] == 1) {
                $arr[] = array('user_id' => $user_id,
                    'work_day' => $i + 1,
                    'is_selected' => 1,
                    'work_text' => $days[$i + 1]
                );
            } else {
                $arr[] = array('user_id' => $user_id,
                    'work_day' => $i + 1,
                    'is_selected' => 0,
                    'work_text' => $days[$i + 1]
                );
            }
        }
        $userWorkDay = UserWorkDay::where('user_id', $user_id)->get();
        if (is_object($userWorkDay)) {
            UserWorkDay::where('user_id', $user_id)->delete();
        }
        UserWorkDay::insert($arr);
        return $workDay;
    }

}
