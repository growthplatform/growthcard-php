<?php

namespace App\Models;

use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Users;
use Illuminate\Support\Facades\Auth;

class Company extends \Eloquent {

    use Eloquence,
        Mappable;

use SoftDeletes;

    protected $table = 'company';
    protected $primaryKey = 'id';
    protected $maps = [
        'companyId' => 'id',
        'companyName' => 'company_name',
    ];
    protected $hidden = ['id', 'company_name', 'deleted_at',
        'created_at', 'updated_at', 'created_by', 'company_admin'];
    protected $fillable = ['id', 'name'];
    protected $appends = ['companyId', 'companyName'];
    protected $dates = ['deleted_at'];
    protected $casts = ['id' => 'integer'];

    public function getCompanyLogoAttribute() {
        return ($this->attributes['company_logo'] == null) ? '' : $this->attributes['company_logo'];
    }

    public function getCompaiesList() {
        return Company::with('companyadmin');
    }

    public function getCompanyUsersData() {
        return TeamMember::with('company', 'user', 'designation', 'effective', 'userdepartment.department', 'user.days');
    }

    public function companyadmin() {
        return $this->belongsTo('App\Models\Users', 'company_admin', 'id');
    }

    public function createCompnay($inputs) {
        $firstName = $inputs['firstName'];
        $lastName = $inputs['lastName'];
        $password = $inputs['password'];
        $hashPassword = \Hash::make($password);
        $email = $inputs['email'];
        $userType = 2; // 3 for user
        $createdBy = $updatedBy = Auth::user()->id;
        $userData = array('first_name' => $firstName,
            'last_name' => $lastName,
            'password' => $hashPassword,
            'email' => $email,
            'user_type' => $userType,
            'created_by' => $createdBy,
            'updated_by' => $updatedBy
        );
        $userId = Users::insertGetId($userData);
        $companyName = $inputs['companyName'];
        $companyData = array('company_name' => $companyName,
            'created_by' => $createdBy,
            'company_admin' => $userId
        );
        Company::insertGetId($companyData);
        return;
    }

    public function updateCompnay($inputs) {
        $firstName = $inputs['firstName'];
        $lastName = $inputs['lastName'];
        $hashPassword = \Hash::make($inputs['password']);
        $email = $inputs['email'];
        $companyName = $inputs['companyName'];
        $companyId = $inputs['companyId'];
        $company = Company::findOrFail($companyId);
        $updatedBy = Auth::user()->id;
        $user = Users::findOrFail($company->company_admin);
        $user->first_name = $firstName;
        $user->last_name = $lastName;
        $user->email = $email;
        $user->password = $hashPassword;
        $user->updated_by = $updatedBy;
        $user->save();
        $company->company_name = $companyName;
        $company->save();
        return;
    }

    public function destroyCompnay($id) {
        $date = date('Y-m-d H:i:s');
        $company = Company::findOrFail($id);
        $company->deleted_at = $date;
        $companyAdmin = Users::findOrFail($company->company_admin);
        $companyAdmin->deleted_at = $date;
        $companyAdmin->save();
        $companyUsers = TeamMember::select('user_id')->where('company_id', $company->id)->get();
        foreach ($companyUsers as $companyUser) {
            $arr[] = $companyUser->user_id;
        }
        if (isset($arr)) {
            Users::whereIn('id', $arr)->update(['deleted_at' => $date]);
        }
        TeamMember::where('team_member.company_id', $company->id)
                ->update(['team_member.deleted_at' => $date]);

        $company->save();
        return;
    }

    public function getUserCompanyID($userid) {
        $userdata = Company::select('id')->where('company_admin', $userid)->get()->toArray();
        if (count($userdata) > 0) {
            return $userdata[0]['companyId'];
        } else {
            return 0;
        }
    }

}
