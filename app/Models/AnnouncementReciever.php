<?php

namespace App\Models;

use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;
use Illuminate\Database\Eloquent\SoftDeletes;

class AnnouncementReciever extends \Eloquent {

    use Eloquence,
        Mappable;

use SoftDeletes;

    protected $table = 'announcement_reciever';
    protected $primaryKey = 'id';
    protected $maps = [
        'announcementId' => 'announcement_id',
        'userId' => 'user_id',
    ];
    protected $hidden = ['id', 'user_id', 'updated_at', 'deleted_at', 'created_at', 'announcement_id'];
    protected $appends = ['announcementId'];
    protected $dates = ['deleted_at'];

    public function __construct() {
        parent:: __construct();
    }

}
