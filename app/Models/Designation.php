<?php

namespace App\Models;

use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;

class Designation extends \Eloquent {

    use Eloquence,
        Mappable;

use SoftDeletes;

    protected $table = 'designation';
    protected $primaryKey = 'id';
    protected $maps = [
        'designationId' => 'id',
        'companyId' => 'comapny_id',
        'designationTitle' => 'title'
    ];
    protected $hidden = ['id', 'title', 'comapny_id', 'deleted_at',
        'created_at', 'updated_at', 'created_by', 'updated_by'];
    protected $fillable = [];
    protected $appends = ['designationId', 'companyId', 'designationTitle'];
    protected $dates = ['deleted_at'];
    protected $casts = ['id' => 'integer', 'comapny_id' => 'integer'];

    public function effectiveAction() {
        return $this->hasMany('App\Models\EffectiveAction', 'designation_id', 'id');
    }

    public function createJob($inputs) {
        $jobName = $inputs['jobName'];
        $companyId = \Session::get('companyId');
        $createdBy = $updatedBy = Auth::user()->id;
        $userData = array(
            'title' => $jobName,
            'comapny_id' => $companyId,
            'created_by' => $createdBy,
            'updated_by' => $updatedBy
        );
        Designation::insertGetId($userData);
        return;
    }

    public function editJob($inputs) {
        $id = $inputs['id'];
        $jobName = $inputs['jobName'];
        $updatedBy = Auth::user()->id;
        $userData = array(
            'title' => $jobName,
            'updated_by' => $updatedBy
        );
        $record = Designation::where('id', $id)->update($userData);
        if (isset($record)) {
            return true;
        } else {
            return false;
        }
    }

    public function getDesignationID($title, $companyId) {
        $designationdata = Designation::select('id')->where('title', $title)->where('comapny_id', $companyId)->first();
        if (count($designationdata) > 0) {
            return $designationdata->id;
        } else {
            return 0;
        }
    }

}
