<?php

namespace App\Models;

use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\PushNotification;
use Illuminate\Support\Facades\Log;

class FeedLike extends \Eloquent {

    use Eloquence,
        Mappable;

use SoftDeletes;

    protected $table = 'feed_like';
    protected $primaryKey = 'id';
    protected $maps = [
        'feedId' => 'id',
        'userId' => 'user_id'
    ];
    protected $hidden = ['id', 'feed_id', 'user_id', 'created_at', 'updated_at', 'deleted_at'];
    protected $fillable = ['feed_id', 'user_id'];
    protected $dates = ['deleted_at'];

    public function __construct() {
        parent:: __construct();
    }

    public static function feedIsLiked($userId, $feedId) {
        $feed = FeedLike::select('id')->where('feed_id', $feedId)->where('user_id', $userId)->first();
        if (isset($feed->id)) {
            return 1;
        } else {
            return 0;
        }
    }

    public function likeFeed($postData) {
        try {
            $userId = $postData['userId'];
            $feedId = $postData['feedId'];
            $data = array('user_id' => $userId, 'feed_id' => $feedId);
            $feedLikeCheck = FeedLike::select('id')->where('user_id', $userId)->where('feed_id', $feedId)->first();
            if (isset($feedLikeCheck->id)) {
                FeedLike::where('id', $feedLikeCheck->id)->delete();
                $isLiked = 0;
            } else {
                FeedLike::insertGetId($data);
                $isLiked = 1;
                $effectiveActionId = Feed::select('effective_action_id', 'created_by', 'team_id')->where('id', $feedId)->first();
                if ($effectiveActionId->created_by != $userId) {
                    $payload['type'] = 4;
                    $effectiveActionName = EffectiveAction::select('effective_action_name')->where('id', $effectiveActionId->effective_action_id)->first();
                    $team = $effectiveActionId->team_id;
                    $user = Users::select('first_name', 'last_name')->where('id', $userId)->first();
                    $userName = ucwords($user->first_name . ' ' . $user->last_name);
                    $payload['message'] = $userName . ' has given you thumbs up on your effective action ' . $effectiveActionName->effective_action_name;
                    $payload['userId'] = $effectiveActionId->created_by;
                    $payload['teamId'] = $team;
                    $payload['effectiveActionId'] = $effectiveActionId->effective_action_id;
                    $payload['message1'] = $userName;
                    $payload['message2'] = ' has given you thumbs up on your effective action ';
                    $payload['message3'] = $effectiveActionName->effective_action_name;
                    PushNotification::sendPushNotification($effectiveActionId->created_by, $payload);
                }
            }
            $count = FeedLike::where('feed_id', $feedId)->count();
            $feed = Feed::find($feedId);
            $feed->like_count = $count;
            $feed->save();
            return (array('LikeCount' => $count, 'isLiked' => $isLiked));
        } catch (Exception $e) {            
            Log::error(['method' => __METHOD__, 'error' => ['file' => $e->getFile(), 'line' => $e->getLine(), 'message' => $e->getMessage()], 'created_at' => date("Y-m-d H:i:s")]);
            return 0;
        }
    }

}
