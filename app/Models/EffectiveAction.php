<?php

namespace App\Models;

use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Config;
use Auth;
use Illuminate\Support\Facades\DB;

class EffectiveAction extends \Eloquent {

    use Eloquence,
        Mappable;

    use SoftDeletes;

    protected $table = 'effective_action';
    protected $primaryKey = 'id';
    protected $maps = [
        'effectiveActionId' => 'id',
        'effectiveActionTitle' => 'effective_action_name',
        'designationId' => 'designation_id',
        'assignEffectiveActionDate' => 'updated_at'
    ];
    protected $hidden = ['id', 'effective_action_name', 'designation_id',
        'created_at', 'updated_at', 'deleted_at', 'created_by', 'updated_by'];
    protected $fillable = ['name', 'designation_id',];
    protected $appends = ['effectiveActionId', 'designationId', 'effectiveActionTitle', 'assignEffectiveActionDate'];
    protected $dates = ['deleted_at'];
    protected $casts = ['id' => 'integer', 'designation_id' => 'integer'];

    public function getEffectiveAction($postData) {
        $designationId = $postData['designationId'];
        $pageNo = $postData['pageNo'];
        return $this->getPaginatedEffectiveActions($pageNo, $designationId);
    }

    public static function getPaginatedEffectiveActions($pageNo, $designationId) {
        $limit = Config::get('constants.limit');
        $skip = ($pageNo - 1) * $limit;
        $getEffectiveAction = EffectiveAction::where('designation_id', $designationId);
        $effectiveActionCount = count($getEffectiveAction->get());
        $totalPages = ceil($effectiveActionCount / $limit);
        $effectiveAction = $getEffectiveAction->orderBy('updated_at', 'desc')->skip($skip)->take($limit)->get();
        $arr['pageNo'] = $pageNo;
        $arr['totalPages'] = $totalPages;
        $arr['effectiveActions'] = $effectiveAction;
        if ($pageNo > $totalPages) {
            return array('status' => 'true', 'result' => $arr, 'message' => trans('messages.no_more_data_not_found'));
        } else {
            return array('status' => 'true', 'result' => $arr, 'message' => '');
        }
    }

    public function createCustomEffectiveAction($postData) {
        $effectiveActionName = $postData['effectiveActionName'];
        $designationId = $postData['designationId'];
        $userId = $postData['userId'];
        $id = EffectiveAction::insertGetId(array('effective_action_name' => $effectiveActionName,
                    'designation_id' => $designationId,
                    'created_by' => $userId,
                    'updated_by' => $userId
        ));
        if (isset($id)) {
            $pageNo = 1;
            return $this->getPaginatedEffectiveActions($pageNo, $designationId);
        } else {
            return false;
        }
    }

    public function createEffectiveAction($inputs) {
        $jobId = $inputs['Jobtitle'];
        $effectiveActionName = $inputs['effectiveAction'];
        $createdBy = $updatedBy = Auth::user()->id;
        $userData = array(
            'effective_action_name' => $effectiveActionName,
            'designation_id' => $jobId,
            'created_by' => $createdBy,
            'updated_by' => $updatedBy
        );
        $id = EffectiveAction::insertGetId($userData);
        if (isset($id)) {
            return true;
        } else {
            return false;
        }
    }

    public function destroyEffectiveAction($inputs) {
        $Id = $inputs['id'];
        $date = date('Y-m-d H:i:s');
        $id = EffectiveAction::where('id', $Id)->update(['deleted_at' => $date]);
        TeamMember::where('effective_action_id', $Id)->update(['effective_action_id' => 'NULL']);
        UserEffectiveActionHistory::where('effective_action_id', $Id)->update(['deleted_at' => $date]);
        Feed::where('effective_action_id', $Id)->update(['deleted_at' => $date]);
        PastEffectiveAction::where('effective_action_id', $Id)->update(['deleted_at' => $date]);
        PushNotification::where('effective_action_id', $Id)->delete();
        if (isset($id)) {
            return true;
        } else {
            return false;
        }
    }

    public function editEffectiveAction($inputs) {
        $id = $inputs['id'];
        $effectiveActionName = $inputs['effectiveAction'];
        $updatedBy = Auth::user()->id;
        $userData = array(
            'effective_action_name' => $effectiveActionName,
            'updated_by' => $updatedBy
        );
        $record = EffectiveAction::where('id', $id)->update($userData);
        if (isset($record)) {
            return true;
        } else {
            return false;
        }
    }

    public function effectiveaction() {
        return PastEffectiveAction::belongsTo('App\Models\EffectiveAction', 'effective_action_id');
    }

    public function curentEffectiveAction($postData) {
        $subordinateId = $postData['subordinateId'];
        $teamId = $postData['teamId'];
        $effectiveActionId = $postData['effectiveActionId'];
        $flag = $postData['flag'];
        if ($flag == 1) {
            // for subordinate cuurent ea
            $pastEffectiveActionId = 0;
            $effective = TeamMember::with('effective')
                    ->where('team_id', $teamId)
                    ->where('user_id', $subordinateId)
                    ->where('effective_action_id', $effectiveActionId)
                    ->first();
        } else {
            // for subordinate past ea
            $pastEffectiveActionId = $postData['pastEffectiveActionId'];
            $flag = 2;
            $effective = PastEffectiveAction::with('effectiveaction')
                    ->where('user_id', $subordinateId)
                    ->where('effective_action_id', $effectiveActionId)
                    ->first();
        }

        if (is_object($effective)) {
            $eaCheck = EffectiveAction::checkEffectiveAction($subordinateId, $effectiveActionId, $teamId, $flag, $pastEffectiveActionId, $effective);
            $effectiveActionSummary = Users::getEffectiveSummary($subordinateId, $effective->effective_action_id, $eaCheck);
            if ($effectiveActionSummary['AVG'] != 0) {
                $effectiveActionSummary['AVG'] = round($effectiveActionSummary['AVG']);
                $avgArray = array(
                    'adhrenceCount' => $effectiveActionSummary['AVG'],
                    'days' => 'Avg');

                $graphSql = Users::getGraph($subordinateId, $effective->effective_action_id, $eaCheck);
                $graph = DB::select($graphSql . ' order by a.work_day desc limit 7');
                $graph[] = $avgArray;
                $effectiveAction['graph'] = array_reverse($graph);
            } else {
                $effectiveAction['graph'] = array();
            }
            $effectiveAction['effectiveActionSummary'] = $effectiveActionSummary;
            $effectiveAction['effectiveActionDescription'] = $effective->effective_action_description;
            $effectiveAction['effectiveActionId'] = $effective->effective_action_id;
            if ($flag == 1) {
                $effectiveAction['startDate'] = $effective->ea_assign_at;
                $effectiveAction['endDate'] = '';
                $effectiveAction['effectiveActionTitle'] = $effective->effective['effective_action_name'];
            } else {
                $effectiveAction['startDate'] = $effective->start_date;
                $effectiveAction['endDate'] = $effective->created_at->format('Y-m-d H:i:s');
                $effectiveAction['effectiveActionTitle'] = $effective->effectiveaction['effective_action_name'];
            }
            return $effectiveAction;
        } else {
            return 0;
        }
    }

    public static function checkEffectiveAction($subordinateId, $effectiveActionId, $teamId, $flag, $pastEffectiveActionId, $effective) {
        $startTime = '';
        $endTime = '';
        if ($flag == 1) {
            $startTime = $effective->ea_assign_at;
        } else {
            $startTime = $effective->start_date;
            $endTime = $effective->created_at;
        }
        return array('flag' => $flag,
            'startTime' => $startTime,
            'endTime' => $endTime
        );
    }

    public static function checkFeedEa($userId, $effectiveActionId, $teamId, $pastEffectiveActionId) {
        $startTime = '';
        $endTime = '';
        if ($pastEffectiveActionId == NULL) {
            $flag = 1;
            $member = TeamMember::select('ea_assign_at')
                    ->where('user_id', $userId)
                    ->where('team_id', $teamId)
                    ->where('effective_action_id', $effectiveActionId)
                    ->first();
            if (!empty($member)) {
                $startTime = $member->ea_assign_at;
            }
        }
        if ($pastEffectiveActionId != NULL && empty($member)) {
            $flag = 2;
            $member = PastEffectiveAction::select('start_date', 'created_at')
                    ->where('user_id', $userId)
                    ->where('effective_action_id', $effectiveActionId)
                    ->where('id', $pastEffectiveActionId)
                    ->first();
            $startTime = $member->start_date;
            $endTime = $member->created_at;
        }
        return array('flag' => $flag,
            'startTime' => $startTime,
            'endTime' => $endTime
        );
    }

}
