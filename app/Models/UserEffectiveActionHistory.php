<?php

namespace App\Models;

use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserEffectiveActionHistory extends \Eloquent {

    use Eloquence,
        Mappable;

use SoftDeletes;

    protected $table = 'user_effective_action_history';
    protected $primaryKey = 'id';
    protected $maps = [
        'userId' => 'user_id',
        'effectiveActionId' => 'effective_action_id',
        'teamId' => 'team_id',
    ];
    protected $hidden = ['id', 'user_id', 'effective_action_id', 'team_id', 'deleted_at', 'created_at',];
    protected $fillable = [];
    protected $dates = ['deleted_at'];

}
