<?php namespace App\Interfaces;

interface PushNotificationInterface {
    public function send($device_identifier,$message,$data,$badge);
}


