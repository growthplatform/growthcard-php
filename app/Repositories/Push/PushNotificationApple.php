<?php

namespace App\Repositories\Push;

use App\Interfaces\PushNotificationInterface;

class PushNotificationApple implements PushNotificationInterface {

    private $used;

    function __construct($server = 'sandbox') {

        $this->used = ($server == 'sandbox') ? config('push_notification.apple.sandbox') : config('push_notification.apple.production');
    }

    public function send($device_identifier, $message, $data, $badge) {
        if (!$device_identifier || strlen($device_identifier) < 22) {
            return;
        }
        try {
            $ctx = stream_context_create();
            stream_context_set_option($ctx, 'ssl', 'local_cert', $this->used['pem_file']);
            stream_context_set_option($ctx, 'ssl', 'passphrase', $this->used['passphrase']);

            // Open a connection to the APNS server
            $fp = stream_socket_client(
                    $this->used['url'], $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
            if (!$fp) {
                return false;
            }
            $body['aps'] = array(
                'alert' => $message,
                'data' => $data,
                'sound' => 'default',
                'badge' => $badge
            );

            // Encode the payload as JSON
            $payload = json_encode($body);

            // Build the binary notification
            $msg = chr(0) . pack('n', 32) . pack('H*', $device_identifier) . pack('n', strlen($payload)) . $payload;
            // Send it to the server
            fwrite($fp, $msg, strlen($msg));
            fclose($fp);
        } catch (Exception $e) {
            return false;
        }
    }

}
