<?php

namespace App\Http\Controllers\Api;
use App\Http\Interfaces\PushNotificationInterface;

class PushNotificationFake implements PushNotificationInterface {

    private $used;

    function __construct() {
        
    }

    public function send($device_identifier, $message, $data , $badge) {
        // Do something if you want
    }

}
