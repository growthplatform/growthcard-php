<?php

namespace App\Repositories\Push;

use App\Interfaces\PushNotificationInterface;

class PushNotificationAndroid implements PushNotificationInterface {

    private $used;

    function __construct() {
        $this->used = config('push_notification.gcm.default');
    }

    public function send($device_identifier, $message, $data, $badge) {
        $registatoin_ids = array($device_identifier);
        $url = $this->used['url'];
        // Google Cloud Messaging GCM API Key
        $msg = array
            (
            'message' => $message,
            'payload' => $data
        );
        $fields = array
            (
            'registration_ids' => $registatoin_ids,
            'data' => $msg
        );
        $headers = array(
            'Authorization: key=' . $this->used['google_api_key'],
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
    }

}
