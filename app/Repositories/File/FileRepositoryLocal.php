<?php

namespace App\Repositories\File;

class FileRepositoryLocal {

    public function upload($image, $type, $name = '', $thumb = 0) {
        $filename = '';
        $thumnail_name = '';
        if (is_object($image) && $image->isValid()) {
            $extension = $image->getClientOriginalExtension();
            if ($name == '') {
                $filename = uniqid() . '.' . $extension;
            } else {
                $filename = $this->getFilename($name);
            }
            $path = $this->getPath($type);
            $image->move($path, $filename);
            if ($thumb == 1) {
                $f = explode(".", $filename);
                $thumnail_name = $f[0] . "_thumb.png";
                exec('ffmpeg -i ' . $image->getRealPath() . ' -ss 3 -f image2 -vframes 1 ' . $path . $thumnail_name);
            }
        }
        return array('image' => $filename, 'thumb' => $thumnail_name);
    }

    private function getFilename($filename) {
        $f = explode("/", $filename);
        $rev_arr = array_reverse($f);
        return $rev_arr[0];
    }

    private function getPath($type) {
        switch ($type) {
            case 1:
                $path = env('USER_IMAGE_PATH');
                break;
        }
        return $path;
    }

}
