<?php

namespace App\Repositories\File;

use App\library\aws\S3;

class FileRepositoryS3 {

    public function __construct() {
        $this->s3 = new S3(env('AWS_ACCESS_KEY_ID'), env('AWS_SECRET_ACCESS_KEY'));
    }

    public function upload($image, $type, $name = '', $thumb = 0, $id = '') {
        $filename = '';
        $thumnail_name = '';
        if (is_object($image) && $image->isValid()) {
            $extension = $image->getClientOriginalExtension();
            $filename = rand(1, 9999) . time() . '.' . $extension;
            $path = $this->getPath($type, $id);
            $this->s3->putObjectFile($image->getRealPath(), env('AWS_BUCKET_NAME'), $path . $filename);
            if ($thumb == 1) {
                $f = explode(".", $filename);
                $thumnail_name = $f[0] . "_thumb.png";
                exec('ffmpeg -i ' . $image->getRealPath() . ' -f image2 -vframes 1 ' . public_path('thumbnails/' . $thumnail_name) . ' > storage/logs/ffmpeglog.log');
                $this->s3->putObjectFile(public_path('thumbnails/' . $thumnail_name), env('AWS_BUCKET_NAME'), $path . $thumnail_name);
                unlink(public_path('thumbnails/' . $thumnail_name));
            }
        }
        return array('image' => $filename, 'thumb' => $thumnail_name);
    }

    private function getPath($type, $id) {
        switch ($type) {
            case 1:
                $path = env('USER_IMAGE_PATH');
                break;
        }
        return $path;
    }

}
