<?php

/*
  |--------------------------------------------------------------------------
  | Routes File
  |--------------------------------------------------------------------------
  |
  | Here is where you will register all of the routes in an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */


/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | This route group applies the "web" middleware group to every route
  | it contains. The "web" middleware group is defined in your HTTP
  | kernel and includes session state, CSRF protection, and more.
  |
 */

Route::group(array('prefix' => 'api/'), function() {
    Route::group(['prefix' => 'user/', 'middleware' => 'loginputs'], function() {
        Route::post('sign-in', 'UserApiController@postSignIn');
        Route::get('terms', 'UserApiController@getTerm');
        Route::put('terms-status', 'UserApiController@putTermsStatus');
        Route::post('work-days', 'UserApiController@postWorkDays');
        Route::put('user-profile', 'UserApiController@putUserProfile');
        Route::post('switch-profile', 'UserApiController@postSwitchProfile');
        Route::post('save-image', 'ImageController@postSaveImage');
        Route::post('get-subordinates', 'TeamMemberController@postGetSubordinates');
        Route::post('get-effective-actions', 'EffectiveActionController@postGetEffectiveAction');
        Route::post('custom-effective-action', 'EffectiveActionController@postCustomEffectiveAction');
        Route::post('assign-effective-action', 'TeamMemberController@postAssignEffectiveAction');
        Route::post('change-effective-action', 'TeamMemberController@postchangeEffectiveAction');
        Route::post('mark-adhrence', 'TeamMemberController@postMarkAdhrence');
        Route::post('get-subordinate-profile', 'UserApiController@postGetSubordinateProfile');
        Route::post('get-team-members', 'TeamMemberController@postGetTeamMember');
        Route::post('search-user', 'UserApiController@postSearchUser');
        Route::resource('announcement', 'AnnouncementController');
        Route::post('delete-announcement', 'AnnouncementController@postDeleteAnnouncement');
        Route::post('get-announcement', 'AnnouncementController@postGetAnnouncement');
        Route::post('get-past-effective-actions', 'EffectiveActionController@postGetPastEffectiveAction');
        Route::post('get-current-effective-action', 'EffectiveActionController@postGetCurrentEffectiveActionGraph');
        Route::post('comment-on-effective-action', 'EffectiveActionController@postCommentOnEffectiveAction');
        Route::post('get-comment-of-effective-action', 'EffectiveActionController@postGetCommentsOfEffectiveAction');
        Route::post('get-manager-dashboard', 'TeamMemberController@postManagerDashBoard');
        Route::resource('post', 'PostController');
        Route::post('get-post', 'PostController@postGetPost');
        Route::post('delete-post', 'PostController@postDeletePost');
        Route::post('generate-feed', 'FeedController@postGenerateFeed');
        Route::post('get-department', 'DepartmentController@postGetDepartment');
        Route::post('forgot-password', 'UserApiController@postForgotPassword');
        Route::post('like-feed', 'FeedController@postLikeFeed');
        Route::post('change-password', 'UserApiController@postChangePassword');
        Route::post('log-out', 'UserApiController@postLogOut');
        Route::post('delete-past-effective-action', 'EffectiveActionController@postDeletePastEffectiveAction');
        Route::post('push-notification', 'UserApiController@postPushNotificationList');
        Route::post('cron-push-notification', 'UserApiController@postCronPushNotification');
    });
    Route::resource('user', 'UserApiController');
});


Route::group(['middleware' => 'web'], function () {
    Route::auth();
    Route::get('/', 'UserWebController@index');
    Route::get('/home', 'UserWebController@index');
    Route::get('setting', 'UserWebController@getSetting');
    Route::get('dashboard', 'UserWebController@getDashboard');
    Route::get('manage-image', 'UserWebController@getManageImage');
    Route::get('users', 'UserWebController@getUser');
    Route::get('effective-actions', 'UserWebController@getEffectiveAction');
    Route::get('download/{uploadteam}/{filename}', 'TeamMemberController@getDownload');
    Route::get('companyId/{id}', 'UserWebController@storeCompanyId');
    Route::post('change-password', 'UserWebController@postChangePassword');
    Route::post('create-team', 'TeamMemberController@postCreateTeam');
    Route::post('upload-image-bulk', 'UserWebController@postUploadImageBulk');
    Route::post('save-upload-image', 'UserWebController@postSaveUploadImage');
    Route::post('delete-appimage', 'UserWebController@postDeleteImage');
    Route::post('create-team-bulk', 'TeamMemberController@postCreateTeamBulk');
    Route::post('create-user-bulk', 'UserWebController@postCreateUserBulk');
    Route::post('delete-team', 'TeamMemberController@postDeleteTeam');
    Route::post('delete-subordinate', 'TeamMemberController@postDeleteSubordinate');
    Route::resource('company', 'CompanyController');
    Route::resource('designation', 'DesignationController');
    Route::resource('department', 'DepartmentController');
    Route::resource('user', 'UserWebController');
    Route::resource('create-job', 'UserWebController@createJob');
    Route::resource('create-effective-action', 'UserWebController@createEffectiveAction');
    Route::resource('delete-effective-action', 'UserWebController@deleteEffectiveAction');
    Route::resource('edit-effective-action', 'UserWebController@editEffectiveAction');
    Route::resource('edit-job', 'UserWebController@editJob');
    Route::resource('create-department', 'UserWebController@createDepartment');
});
