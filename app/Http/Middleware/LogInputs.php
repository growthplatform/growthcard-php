<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;
use Illuminate\Routing\Router;

class LogInputs {

    protected $router;

    function __construct(Router $router) {
        $this->router = $router;
    }

    public function handle($request, Closure $next) {
        $this->postData = json_decode($request->getContent(), true);
        $action_name = $this->router->getRoutes()->match($request)->getActionName();
        Log::info('Input Details', array('context' => array('method' => $action_name, 'data' => $this->postData)));
        $response = $next($request);
        Log::info('Response Details', array('context' => array('method' => $action_name, 'data' => $response)));
        return $response;
    }

}
