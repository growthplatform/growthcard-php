<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Device;
use App\Models\Users;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Config;

class Apiauth {

    protected $user_id;
    protected $device_id;
    protected $user_token;
    protected $router;

    function __construct(Router $router) {
        $this->router = $router;
    }

    public function handle($request, Closure $next) {
        $this->postData = json_decode($request->getContent(), true);
        $user_id = $this->postData['userId'];
        $device_id = $this->postData['deviceId'];
        $user_token = $this->postData['userToken'];
        $response = array(
            'success' => 0,
            'message' => trans('messages.unauthorized'),
            'statusCode' => Config::get('codes.unauthorized'),
        );
        $response_del = array(
            'success' => 0,
            'message' => trans('messages.user_deleted'),
            'statusCode' => Config::get('codes.user_deleted'),
        );
        if ($user_id != '' && $device_id != '' && $user_token != '') {
            $ch_user = Users::withTrashed()->find($user_id);
            if (is_object($ch_user)) {
                if ($ch_user->trashed()) {
                    return response()->json($response_del, $response_del['statusCode']);
                }
            } else {
                return response()->json($response_del, $response_del['statusCode']);
            }
            $user = Device::where('user_id', $user_id)->where('device_id', $device_id)->where('user_token', $user_token)->first();
            if (is_object($user)) {
                return $next($request);
            } else {
                return response()->json($response, $response['statusCode']);
            }
        } else {
            return response()->json($response, $response['statusCode']);
        }
        return response()->json($response, $response['StatusCode']);
    }

}
