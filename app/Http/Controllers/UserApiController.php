<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Users;
use Config;
use Auth;
use App\Models\Device;
use App\Models\TeamMember;
use App\Models\UserWorkDay;
use Illuminate\Support\Facades\Hash;
use App\Models\PushNotification;
use App\Utility\CommonMethod;

class UserApiController extends UtilityController {

    const MANAGER_ROLE_ID = 1;
    const SUBORDINATE_ROLE_ID = 2;

    public function __construct() {
        parent:: __construct();
        $this->middleware('validatejson', ['except' => ['index', 'getTerm', 'postForgotPassword', 'postCronPushNotification']]);
        $this->middleware('apiauth', ['except' => ['postSignIn', 'getTerm', 'postForgotPassword', 'postCronPushNotification']]);
        $request = \Request::instance();
        $this->postData = json_decode($request->getContent(), true);
    }

    public function index() {
        try {
            $user = User::all();
            $this->result[] = $user;
            return $this->render();
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function show($id) {
        try {
            $user = User::findOrFail($id);
            $this->result[] = $user;
            return $this->render();
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function postSignIn() {
        try {
            $this->user = new Users();
            $validation_rules = array(
                'deviceId' => 'required',
                'deviceType' => 'required',
                'email' => 'required|email|exists:users,email,user_type,3',
                'password' => 'required'
            );
            $validator = Validator::make($this->postData, $validation_rules);
            if ($validator->fails()) {
                return $this->validationError($validator);
            }
            if (Auth::attempt(['email' => $this->postData['email'], 'password' => $this->postData['password']])) {
                $user_id = Auth::user()->id;
                $user = Users::withTrashed()->find($user_id);
                if (is_object($user)) {
                    if ($user->trashed()) {
                        $this->success = 0;
                        $this->message = trans('messages.user_deleted');
                        $this->statusCode = Config::get('codes.user_deleted');
                        Device::unRegister_all($user->id);
                    } else {
                        $deviceModel = new Device();
                        $user_token = $deviceModel->register_device($this->postData['deviceId'], $user->id, $this->postData['deviceToken'], $this->postData['deviceType']);
                        $user->role = '';
                        $role = $this->user->findUserRoleInTeam($user_id)->get();
                        $roleCount = count($role);
                        if ($roleCount == 2) {
                            $user = Users::where('id', $user_id)
                                    ->with(['member' => function($q) {
                                            $q->where('role', static::SUBORDINATE_ROLE_ID);
                                            $q->where('team_id', '!=', 'NULL');
                                        }, 'member.company', 'member.designation', 'member.effective', 'days', 'userdepartment.department'])
                                    ->first();
                            $role = $user->member['role'];
                            $switch = 1;
                        } elseif ($roleCount == 1) {
                            $teamId = TeamMember::select('team_id')->where('user_id', $user_id)->where('role', 2)->first();
                            if ($teamId->team_id == '') {
                                $user = Users::where('id', $user_id)
                                        ->with(['member' => function($q) {
                                                $q->where('role', static::MANAGER_ROLE_ID);
                                            }, 'member.company', 'member.designation', 'member.effective', 'days', 'userdepartment.department'])
                                        ->first();
                                $role = static::SUBORDINATE_ROLE_ID;
                                $switch = 1;
                            } else {
                                $user = Users::where('id', $user_id)
                                        ->with(['member' => function($q) {
                                                $q->where('team_id', '!=', 'NULL');
                                            }, 'member.company', 'member.designation', 'member.effective', 'days', 'userdepartment.department'])
                                        ->first();
                                $role = $user->member['role'];
                                $switch = 0;
                            }
                        } else {
                            $user = Users::getUserAllData()->where('id', $user_id)->first();
                            $role = static::SUBORDINATE_ROLE_ID;
                            $switch = 0;
                        }
                        $user->userToken = $user_token;
                        $user->role = $role;
                        $user->switch = $switch;
                        $this->result = $user;
                    }
                }
            } else {
                $this->renderFailure(trans('messages.invalid_credentials'), Config::get('codes.unauthorized'));
            }
            return $this->render();
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function getTerm() {
        return view('user/terms');
    }

    public function putTermsStatus() {
        try {
            $this->user = new Users();
            $user_id = $this->postData['userId'];
            $this->user->updateTermsStatus($user_id);
            return $this->render();
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function postWorkDays() {
        try {
            $this->user_work_day = new UserWorkDay();
            $validation_rules = array(
                'workDay' => 'required|size:7'
            );
            $validator = Validator::make($this->postData, $validation_rules);
            if ($validator->fails()) {
                return $this->validationError($validator);
            } else {
                $user_id = $this->postData['userId'];
                $workDay = str_split($this->postData['workDay']);
                $this->result = $this->user_work_day->saveUserWorkDays($user_id, $workDay);
            }
            return $this->render();
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function putUserProfile() {
        try {
            $this->user = new Users();
            $validation_rules = array(
                'email' => 'email|unique:users,email',
                'firstName' => 'min:3,max:20',
                'lastName' => 'min:3,max:20',
                'workDay' => 'size:7'
            );
            $validator = Validator::make($this->postData, $validation_rules);
            if ($validator->fails()) {
                return $this->validationError($validator);
            } else {
                $this->user->updateUserProfile($this->postData);
            }
            return $this->render();
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function deleteSignOut(Request $request) {
        try {
            Device::unRegister_all($request->input('userId'));
            $this->message = trans('messages.sign_out_successfull');
            return $this->render();
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function postGetSubordinates() {
        try {
            $validation_rules = array(
                'teamId' => 'required|exists:team,id'
            );
            $validator = Validator::make($this->postData, $validation_rules);
            if ($validator->fails()) {
                return $this->validationError($validator);
            } else {
                $this->result = $this->team_member->getSubordinates($this->postData);
            }
            return $this->render();
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function postGetSubordinateProfile() {
        try {
            $this->user = new Users();
            $validation_rules = array(
                'teamId' => 'required|exists:team,id',
                'subordinateId' => 'required|exists:users,id',
                'companyId' => 'required|exists:company,id',
            );
            $validator = Validator::make($this->postData, $validation_rules);
            if ($validator->fails()) {
                return $this->validationError($validator);
            } else {
                $this->result = $this->user->getSubordinateProfile($this->postData);
            }
            return $this->render();
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function postSearchUser() {
        try {
            $this->user = new Users();
            $validation_rules = array(
                'searchText' => 'required',
                'companyId' => 'required|exists:company,id',
                'pageNo' => 'required|min:1',
                'teamId' => 'required|exists:team,id',
            );
            $validator = Validator::make($this->postData, $validation_rules);
            if ($validator->fails()) {
                return $this->validationError($validator);
            } else {
                $response = $this->user->searchUser($this->postData);
                if ($response['status'] == 'true') {
                    $this->result = $response['result'];
                    $this->message = $response['message'];
                    return $this->render();
                } else {
                    return $this->renderFailure(trans('messages.user_not_manager'), Config::get('codes.unauthorized'));
                }
            }
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function postSwitchProfile() {
        try {
            $validation_rules = array(
                'flag' => 'required'
            );
            $validator = Validator::make($this->postData, $validation_rules);
            if ($validator->fails()) {
                return $this->validationError($validator);
            } else {
                $this->user = new Users();
                $this->result = $this->user->switchProfile($this->postData);
            }
            return $this->render();
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function postForgotPassword() {
        try {
            $validation_rules = array('email' => 'required|email|exists:users,email,user_type,3');
            $validator = Validator::make($this->postData, $validation_rules);
            if ($validator->fails()) {
                return $this->validationError($validator);
            }
            $this->user = new Users();
            $email = $this->postData['email'];
            $user = Users::where('email', $email)->firstOrFail();
            $length = 6;
            $alphabets = range('A', 'Z');
            $numbers = range('0', '9');
            $final_array = array_merge($alphabets, $numbers);
            $passKey = '';
            while ($length > 0) {
                $key = array_rand($final_array);
                $passKey .= $final_array[$key];
                $length--;
            }
            $user->password = bcrypt($passKey);
            $user->save();
            $this->user->forgotPasswordMail($email, $user, $passKey);
            $this->message = trans('messages.mail_send_successfully');
            return $this->render();
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function postChangePassword() {
        try {
            $this->user = new Users();
            $validation_rules = array(
                'oldPassword' => 'required',
                'newPassword' => 'required',
            );
            $validator = Validator::make($this->postData, $validation_rules);
            if ($validator->fails()) {
                return $this->validationError($validator);
            }
            $user_id = $this->postData['userId'];
            $user = Users::where('id', $user_id)->first();
            if (Hash::check($this->postData['newPassword'], $user->password)) {
                $this->renderFailure(trans('messages.match_pass'), Config::get('codes.unauthorized'));
            }
            if (Auth::attempt(['password' => $this->postData['oldPassword'], 'id' => $user_id])) {
                $this->user->updatePassword($this->postData['newPassword'], $user_id);
            } else {
                $this->renderFailure(trans('messages.incorrect_pass'), Config::get('codes.unauthorized'));
            }
            return $this->render();
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function postLogOut() {
        try {
            $userId = $this->postData['userId'];
            $userToken = $this->postData['userToken'];
            $deviceId = $this->postData['deviceId'];
            Device::where('user_id', $userId)->where('user_token', $userToken)->where('device_id', $deviceId)->delete();
            return $this->render();
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function postPushNotificationList() {
        try {
            $validation_rules = array(
                'pageNo' => 'required|min:1',
                'teamId' => 'required|exists:team,id'
            );
            $validator = Validator::make($this->postData, $validation_rules);
            if ($validator->fails()) {
                return $this->validationError($validator);
            } else {
                $response = PushNotification::notificationsList($this->postData);
                if ($response['status'] == 'false') {
                    return $this->renderFailure(trans('messages.user_not_manager'), Config::get('codes.unauthorized'));
                } else {
                    $this->result = $response['result'];
                    $this->message = $response['message'];
                }
            }
            return $this->render();
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function postCronPushNotification() {
        try {
            PushNotification::cronPush($this->postData);
            $this->message = 'Push Send';
            return $this->render();
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

}
