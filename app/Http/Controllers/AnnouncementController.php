<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Config;
use App\Models\Announcement;
use App\Utility\CommonMethod;

class AnnouncementController extends UtilityController {

    public function __construct() {
        parent:: __construct();
        $this->middleware('validatejson', ['except' => ['index']]);
        $this->middleware('apiauth', ['except' => ['']]);
        $request = \Request::instance();
        $this->postData = json_decode($request->getContent(), true);
    }

    public function show($id) {
        try {
            $user = User::findOrFail($id);
            $this->result[] = $user;
            return $this->render();
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function postGetAnnouncement() {
        try {
            $validation_rules = array(
                'teamId' => 'required|exists:team,id',
                'companyId' => 'required|exists:company,id',
                'pageNo' => 'required|min:1',
                'role' => 'required'
            );
            $validator = Validator::make($this->postData, $validation_rules);
            if ($validator->fails()) {
                return $this->validationError($validator);
            } else {
                $response = Announcement::getAnnouncement($this->postData);
                if ($response['status'] == 'false') {
                    return $this->renderFailure(trans('messages.user_not_manager'), Config::get('codes.unauthorized'));
                } else {
                    $this->result = $response['result'];
                    $this->message = $response['message'];
                }
            }
            return $this->render();
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function store() {
        try {
            $validation_rules = array(
                'subject' => 'required',
                'content' => 'required',
                'teamId' => 'required|exists:team,id',
                'flag' => 'required',
                'companyId' => 'required|exists:company,id',
            );
            $validator = Validator::make($this->postData, $validation_rules);
            if ($validator->fails()) {
                return $this->validationError($validator);
            } else {
                $response = Announcement::createAnnouncement($this->postData);
                if ($response == 0) {
                    return $this->renderFailure(trans('messages.user_not_manager'), Config::get('codes.unauthorized'));
                }
            }
            return $this->render();
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        } 
    }

    public function postDeleteAnnouncement() {
        try {
            $validation_rules = array(
                'announcementId' => 'required|exists:announcement,id',
                'teamId' => 'required|exists:team,id',
                'companyId' => 'required|exists:company,id'
            );
            $validator = Validator::make($this->postData, $validation_rules);
            if ($validator->fails()) {
                return $this->validationError($validator);
            } else {
                $response = Announcement::deleteAnnouncement($this->postData);
                if ($response == 'false') {
                    return $this->renderFailure(trans('messages.user_not_manager'), Config::get('codes.unauthorized'));
                }
            }
            return $this->render();
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        } 
    }

}
