<?php

namespace App\Http\Controllers;

use Config;
use Illuminate\Http\Response;

class UtilityController extends Controller {

    public function __construct() {
        $this->result = array();
        $this->success = 1;
        $this->message = '';
        $this->statusCode = 200;
    }

    /**
     * @description Api for rendering the json format for result.
     * @return json encodeded response
     */
    public function render() {
        try {
            $this->response = array(
                'success' => $this->success,
                'message' => $this->message,
                'statusCode' => $this->statusCode
            );
            if ($this->success == 1) {
                $this->response['result'] = $this->result;
            }
            $result = Response()->json($this->response, $this->response['statusCode'], [], JSON_NUMERIC_CHECK);
            return $result;
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function validationError($validator) {
        try {
            $this->success = 0;
            $this->message = $validator->errors()->first();
            $this->statusCode = Config::get('codes.parameter_missing');
            return $this->render();
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function modelNotFoundError($msg) {
        try {
            $this->success = 0;
            $this->message = $msg;
            $this->statusCode = Config::get('codes.not_found');
            return $this->render();
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function exceptionCatch($msg) {
        try {
            $this->success = 0;
            $this->message = $msg;
            $this->statusCode = Config::get('codes.tech_error');
            return $this->render();
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function renderFailure($msg, $code) {
        try {
            $this->success = 0;
            $this->message = $msg;
            $this->statusCode = $code;
            return $this->render();
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function ajaxResponse($sucess, $message = [], $result = []) {
        try {
            $this->response = array(
                'success' => $sucess,
                'message' => $message,
            );
            if ($sucess == 1) {
                $this->response['result'] = $result;
            }
            return json_encode($this->response);
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

}
