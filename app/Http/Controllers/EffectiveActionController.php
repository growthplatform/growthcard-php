<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Config;
use App\Models\EffectiveAction;
use App\Models\PastEffectiveAction;
use App\Models\Comment;
use App\Utility\CommonMethod;

class EffectiveActionController extends UtilityController {

    public function __construct() {
        parent:: __construct();
        $this->middleware('validatejson', ['except' => ['index']]);
        $this->middleware('apiauth', ['except' => ['postSignIn']]);
        $this->middleware('xss',['except' => ['postGetEffectiveAction','postCustomEffectiveAction','postGetPastEffectiveAction',
            'postGetCurrentEffectiveActionGraph','postCommentOnEffectiveAction','postGetCommentsOfEffectiveAction','postDeletePastEffectiveAction']]);
        $request = \Request::instance();
        $this->postData = json_decode($request->getContent(), true);
    }

    public function postGetEffectiveAction() {
        try {
            $this->effective_action = new EffectiveAction();
            $validation_rules = array(
                'designationId' => 'required|exists:designation,id'
            );
            $validator = Validator::make($this->postData, $validation_rules);
            if ($validator->fails()) {
                return $this->validationError($validator);
            } else {
                $response = $this->effective_action->getEffectiveAction($this->postData);
                if ($response['status'] == 'true') {
                    $this->result = $response['result'];
                    $this->message = $response['message'];
                    return $this->render();
                } else {
                    return $this->renderFailure(trans('messages.user_not_manager'), Config::get('codes.unauthorized'));
                }
            }
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function postCustomEffectiveAction() {
        try {
            $this->effective_action = new EffectiveAction();
            $validation_rules = array(
                'effectiveActionName' => 'required|max:140',
                'designationId' => 'required|exists:designation,id'
            );
            $validator = Validator::make($this->postData, $validation_rules);
            if ($validator->fails()) {
                return $this->validationError($validator);
            } else {
                $response = $this->effective_action->createCustomEffectiveAction($this->postData);
                if ($response['status'] == 'true') {
                    $this->result = $response['result'];
                    $this->message = $response['message'];
                    return $this->render();
                } else {
                    return $this->renderFailure(trans('messages.user_not_manager'), Config::get('codes.unauthorized'));
                }
            }
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function postGetPastEffectiveAction() {
        try {
            $this->past_effective_action = new PastEffectiveAction();
            $validation_rules = array(
                'teamId' => 'required|exists:team,id',
                'subordinateId' => 'required|exists:users,id',
                'pageNo' => 'required|min:1',
                'flag' => 'required|min:1'
            );
            $validator = Validator::make($this->postData, $validation_rules);
            if ($validator->fails()) {
                return $this->validationError($validator);
            } else {
                $response = $this->past_effective_action->getPastCustomEffectiveAction($this->postData);
                if ($response['status'] == 'true') {
                    $this->result = $response['result'];
                    $this->message = $response['message'];
                    return $this->render();
                }
            }
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function postGetCurrentEffectiveActionGraph() {
        try {
            $this->effective_action = new EffectiveAction();
            $validation_rules = array(
                'teamId' => 'required|exists:team,id',
                'effectiveActionId' => 'required|exists:effective_action,id',
                'subordinateId' => 'required|exists:users,id',
                'flag' => 'required'
            );
            if (!empty($this->postData['pastEffectiveActionId'])) {
                $validation_rules = array(
                    'pastEffectiveActionId' => 'required|exists:past_effective_action,id'
                );
            }
            $validator = Validator::make($this->postData, $validation_rules);
            if ($validator->fails()) {
                return $this->validationError($validator);
            } else {
                $response = $this->effective_action->curentEffectiveAction($this->postData);
                if ($response === 0) {
                    return $this->renderFailure(trans('messages.effective_action_not_found'), Config::get('codes.not_found'));
                }
                $this->result = $response;
                return $this->render();
            }
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function postCommentOnEffectiveAction() {
        try {
            $this->comment = new Comment();
            $validation_rules = array(
                'effectiveActionId' => 'required|exists:effective_action,id',
                'teamId' => 'required|exists:team,id',
                'subordinateId' => 'required|exists:users,id',
                'companyId' => 'required|exists:company,id',
            );
            $validator = Validator::make($this->postData, $validation_rules);
            if ($validator->fails()) {
                return $this->validationError($validator);
            } else {
                $this->result = $this->comment->commentOnEffectiveAction($this->postData);
                return $this->render();
            }
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function postGetCommentsOfEffectiveAction() {
        try {
            $this->comment = new Comment();
            $validation_rules = array(
                'effectiveActionId' => 'required|exists:effective_action,id',
                'teamId' => 'required|exists:team,id',
                'pageNo' => 'required|min:1',
                'subordinateId' => 'required|exists:users,id'
            );
            if (!empty($this->postData['pastEffectiveActionId'])) {
                $validation_rules = array(
                    'pastEffectiveActionId' => 'required|exists:past_effective_action,id'
                );
            }
            $validator = Validator::make($this->postData, $validation_rules);
            if ($validator->fails()) {
                return $this->validationError($validator);
            } else {
                $response = $this->comment->getCommentsOfEffectiveAction($this->postData);
                if ($response['status'] == 'true') {
                    $this->result = $response['result'];
                    $this->message = $response['message'];
                    return $this->render();
                }
                return $this->render();
            }
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function postDeletePastEffectiveAction() {
        try {
            $this->past_effective_action = new PastEffectiveAction();
            $validation_rules = array(
                'teamId' => 'required|exists:team,id',
                'subordinateId' => 'required|exists:users,id',
                'effectiveActionId' => 'required|exists:effective_action,id',
            );
            $validator = Validator::make($this->postData, $validation_rules);
            if ($validator->fails()) {
                return $this->validationError($validator);
            } else {
                $response = $this->past_effective_action->deletePastCustomEffectiveAction($this->postData);
                if ($response == 0) {
                    return $this->renderFailure(trans('messages.user_not_manager'), Config::get('codes.unauthorized'));
                }
                return $this->render();
            }
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

}
