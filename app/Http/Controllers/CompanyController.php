<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Models\Company;
use App\Utility\CommonMethod;

class CompanyController extends UtilityController {

    public function __construct() {
        parent:: __construct();
        $this->middleware('loginputs');
        $this->middleware('xss');
        $this->company = new Company();
    }

    public function store() {
        try {
            $inputs = Input::all();
            $validation_rules = array(
                'firstName' => 'required|max:50',
                'lastName' => 'required|max:50',
                'email' => 'required|email|unique:users,email',
                'password' => 'required|min:6|max:12',
                'companyName' => 'required|max:100'
            );
            $validator = Validator::make($inputs, $validation_rules);
            if ($validator->fails()) {
                return $this->validationError($validator);
            } else {
                $this->company->createCompnay($inputs);
            }
            return $this->render();
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function edit($id) {     //return prefill data for edit
        try {
            $data = $this->company->getCompaiesList()->where('id', $id)->first();
            $this->result = $data;
            return $this->render();
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function update($id) {
        try {
            $inputs = Input::all();
            $company = \App\Models\Company::findOrFail($id);
            $companyAdmin = $company->company_admin;
            $companyAdminEmail = \App\Models\Users::findOrFail($companyAdmin);
            $validation_rules = array(
                'firstName' => 'required|max:50',
                'lastName' => 'required|max:50',
                'companyName' => 'required|max:100',
                'companyId' => 'required|exists:company,id'
            );
            if ($companyAdminEmail->email != $inputs['email']) {
                $validation_rules['email'] = 'required|email|unique:users,email';
            }
            if ($inputs['password'] != '') {
                $validation_rules['password'] = 'required|min:6|max:12';
            }
            $validator = Validator::make($inputs, $validation_rules);
            if ($validator->fails()) {
                return $this->validationError($validator);
            } else {
                $this->company->updateCompnay($inputs);
            }
            return $this->render();
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function destroy($id) {
        try {
            $companyId['companyId'] = $id;
            $validation_rules = array(
                'companyId' => 'required|exists:company,id'
            );
            $validator = Validator::make($companyId, $validation_rules);
            if ($validator->fails()) {
                return $this->validationError($validator);
            } else {
                $this->company->destroyCompnay($id);
            }
            return $this->render();
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

}
