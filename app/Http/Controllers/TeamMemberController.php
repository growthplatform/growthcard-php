<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use App\Models\Users;
use Config;
use Auth;
use App\Models\TeamMember;
use App\Models\Team;
use App\Utility\CommonMethod;
use App\Models\TeamAssignmentHistory;
use Illuminate\Support\Facades\Input;

class TeamMemberController extends UtilityController {

    const MANAGER_ROLE_ID = 1;
    const SUBORDINATE_ROLE_ID = 2;

    public function __construct() {
        parent:: __construct();
        $this->middleware('validatejson', ['except' => ['index', 'postCreateTeam', 'postDeleteTeam', 'postDeleteSubordinate', 'postCreateTeamBulk', 'getDownload']]);
        $this->middleware('apiauth', ['except' => ['postSignIn', 'postCreateTeam', 'postDeleteTeam', 'postDeleteSubordinate', 'postCreateTeamBulk', 'getDownload']]);
        $this->middleware('xss',['except' => ['postGetSubordinates','postAssignEffectiveAction','postchangeEffectiveAction','postMarkAdhrence','postGetTeamMember','postManagerDashBoard']]);
        $request = \Request::instance();
        $this->postData = json_decode($request->getContent(), true);
    }

    public function postGetSubordinates() {
        try {
            $this->team_member = new TeamMember();
            $validation_rules = array(
                'teamId' => 'required|exists:team,id',
                'pageNo' => 'required|min:1'
            );
            $validator = Validator::make($this->postData, $validation_rules);
            if ($validator->fails()) {
                return $this->validationError($validator);
            } else {
                $response = $this->team_member->getSubordinates($this->postData);
                if ($response['status'] == 'true') {
                    $this->result = $response['result'];
                    $this->message = $response['message'];
                    return $this->render();
                } else {
                    return $this->renderFailure(trans('messages.user_not_manager'), Config::get('codes.unauthorized'));
                }
            }
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function postAssignEffectiveAction() {
        try {
            $this->team_member = new TeamMember();
            $validation_rules = array(
                'teamId' => 'required|exists:team,id',
                'effectiveActionDescription' => 'max:250',
                'subordinateId' => 'required|exists:users,id',
                'effectiveActionId' => 'required|exists:effective_action,id,deleted_at,NULL',
            );
            $validator = Validator::make($this->postData, $validation_rules);
            if ($validator->fails()) {
                return $this->validationError($validator);
            } else {
                $response = $this->team_member->assignEffectiveAction($this->postData);
                if ($response == 0) {
                    return $this->renderFailure(trans('messages.user_not_manager'), Config::get('codes.unauthorized'));
                }
                return $this->render();
            }
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function postchangeEffectiveAction() {
        try {
            $this->team_member = new TeamMember();
            $validation_rules = array(
                'teamId' => 'required|exists:team,id',
                'effectiveActionId' => 'required|exists:effective_action,id',
                'subordinateId' => 'required|exists:users,id'
            );
            $validator = Validator::make($this->postData, $validation_rules);
            if ($validator->fails()) {
                return $this->validationError($validator);
            } else {
                $response = $this->team_member->changeEffectiveAction($this->postData);
                if ($response == 0) {
                    return $this->renderFailure(trans('messages.user_not_manager'), Config::get('codes.unauthorized'));
                }
                return $this->render();
            }
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function postMarkAdhrence() {
        try {
            $this->team_member = new TeamMember();
            $validation_rules = array(
                'teamId' => 'required|exists:team,id',
                'effectiveActionId' => 'required|exists:effective_action,id',
                'userId' => 'required|exists:users,id',
                'companyId' => 'required|exists:company,id'
            );
            $validator = Validator::make($this->postData, $validation_rules);
            if ($validator->fails()) {
                return $this->validationError($validator);
            } else {
                $response = $this->team_member->markAdhrence($this->postData);
                $this->result = $response;
                $msg = '';
                if ($response == 2) {
                    $msg = trans('messages.not_assigned_effective_action');
                } else if ($response == 1) {
                    $msg = trans('messages.not_selected_work_day');
                } else if ($response == 3) {
                    $msg = trans('messages.work_days_not_opted');
                }
                if ($msg != '') {
                    return $this->renderFailure($msg, Config::get('codes.unauthorized'));
                } else {
                    return $this->render();
                }
            }
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function postGetTeamMember() {
        try {
            $this->team_member = new TeamMember();
            $validation_rules = array(
                'teamId' => 'required|exists:team,id',
                'subordinateId' => 'required|exists:users,id',
                'companyId' => 'required|exists:company,id',
                'pageNo' => 'required|min:1'
            );
            $validator = Validator::make($this->postData, $validation_rules);
            if ($validator->fails()) {
                return $this->validationError($validator);
            } else {
                $this->result = $this->team_member->getTeamMembers($this->postData);
                return $this->render();
            }
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    //drag n drop team creation
    public function postCreateTeam(Request $request) {
        try {
            $this->team = new Team();
            $companyId = \Session::get('companyId');
            $createdBy = Auth::User()->id;
            $dtTime = date('Y-m-d H:i:s');
            $teamData = array('company_id' => $companyId,
                'created_by' => $createdBy,
                'updated_by' => $createdBy,
                'created_at' => $dtTime,
                'updated_at' => $dtTime
            );
            $ajaxteamid = (int) $request->input('teamid');
            if ($ajaxteamid > 0) {
                $teamID = $ajaxteamid;  //teamid as received from ajax request
            } else {
                $teamID = $this->team->createTeam($teamData); //create new team
            }
            if ($teamID > 0) {
                $this->team_member = new TeamMember();
                $oldmanagerid = (int) trim($request->input('oldmanagerid'));
                $teammanagerid = (int) trim($request->input('teammanagerid'));
                if ($oldmanagerid > 0) {    //at the time of change manager
                    if ($oldmanagerid !== $teammanagerid) { //if manager is changed
                        $this->team_member->updateField(array('oldmanagerid' => $oldmanagerid, 'team_id' => $teamID, 'action' => 'changemanager'));
                    }
                    $oldManagerEa = TeamMember::select('effective_action_id')->where('user_id', $oldmanagerid)->first();
                    $oldManagerEaId = $oldManagerEa->effective_action_id;
                    $data = array('team_id' => $teamID, 'user_id' => $oldmanagerid, 'role' => 1, 'effective_action_id' => $oldManagerEaId);
                    TeamAssignmentHistory::insert($data);
                }
                $teamDetailArr = array();
                $designationArrStr = $this->team_member->getDesignationID(array($teammanagerid));  //getting user's designationId
                $teamDetailArr[$teamID][static::MANAGER_ROLE_ID]['company_id'] = $companyId;
                $teamDetailArr[$teamID][static::MANAGER_ROLE_ID]['user_id'] = $teammanagerid;
                $teamDetailArr[$teamID][static::MANAGER_ROLE_ID]['designation_id'] = $designationArrStr[$teammanagerid]['designationId'];
                $teamsubordinateid = trim($request->input('teamsubordinateid'));
                if ($teamsubordinateid != '') {
                    if ($ajaxteamid > 0) {
                        $teamsubordinateidArr = array_reverse(explode(',', trim($request->input('teamsubordinateid')))); //so that latest dragged used can be shown on top
                    } else {
                        $teamsubordinateidArr = explode(',', trim($request->input('teamsubordinateid')));
                    }

                    $designationArrStr2 = $this->team_member->getDesignationID($teamsubordinateidArr);

                    foreach ($teamsubordinateidArr as $key => $userID) {
                        $teamDetailArr[$teamID][static::SUBORDINATE_ROLE_ID][$key]['company_id'] = $companyId;
                        $teamDetailArr[$teamID][static::SUBORDINATE_ROLE_ID][$key]['user_id'] = $userID;
                        $teamDetailArr[$teamID][static::SUBORDINATE_ROLE_ID][$key]['designation_id'] = $designationArrStr2[$userID]['designationId'];
                    }
                }
                $this->team_member->createUpdateTeamMember($teamDetailArr);
            }
            return $teamID;
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function postCreateTeamBulk(Request $request) {       //create team from CSV file        
        try {
            $common_method = new CommonMethod();
            //**start of ajax upload**//
            $destination_path = env('LOCAL_STORAGE_PATH');
            if ($request->ajax()) {
                $file = Input::file('ajaxcsvoperation');
                
                $fileArray = array('attachedFile' => $file[0]);
                $rules = array(
                    'attachedFile' => 'required|mimes:csv,txt|max:5120'
                );
                $validator = Validator::make($fileArray, $rules);
                
                if ($validator->fails()) {
                    return Redirect::back()->withErrors($validator->errors()->toArray());
                } else {
                    if (Input::hasFile('ajaxcsvoperation')) {
                        $common_method->ajaxUpload($file[0], 'upload');
                    }
                }
            }
            //**end of ajax upload**//  
            $fileuploadmode = isset($request['fileuploadmode']) ? $request['fileuploadmode'] : '';
            if ($fileuploadmode == 'browseupload') {
                $file = $common_method->postCSVFileUpload('csvcreateteam', $request['ajaxfilenewname']);
            } else if ($fileuploadmode == 'ajaxupload') {
                $file = array('csvcreateteam' => array('name' => $request['ajaxfilename'], 'newname' => $request['ajaxfilenewname'], 'type' => $request['ajaxfilemimetype'], 'tmp_name' => $request['ajaxfilepath'], 'error' => 0, 'size' => $request['ajaxfilesize']));
            }
            $status = $common_method->checkFileExist($file['csvcreateteam']['newname'], 'upload');
            if($status === false){
                Session::flash('fileupload_failure', array('filenotexist' => trans('messages.file_not_uploaded')));
            } else {
                $data = array();
                $actioinStatus = $i = 0;
                $actualfilepath = $common_method->getFolderName(env('STORAGETYPE'), 'upload', 'read').$file['csvcreateteam']['newname'];
                if (($handle = fopen($actualfilepath, 'r')) !== FALSE) {
                    $readFileDataArr = $this->readFile($handle, $data, $i);
                    $data = $readFileDataArr['data'];
                    $i = $readFileDataArr['i'];
                    fclose($handle);
                }
                $outputcsvdata = array();
                if (count($data['data_error'] > 1)) {
                    $outputcsvdata['data_error'] = $data['data_error'];
                }
                if (isset($data['data_success']) && count($data['data_success']) > 0) { //now using correct data after validation
                    $tmpReturnData = $this->processFile($data['data_success'], $outputcsvdata['data_error'], $actioinStatus);
                    $data['data_success'] = $tmpReturnData['data_success'];
                    $outputcsvdata['data_error'] = $tmpReturnData['data_error'];
                    $actioinStatus = $tmpReturnData['actioinStatus'];
                }
                if ($i > 1) {
                    if (count($outputcsvdata['data_error']) > 1) {    //if error found after db validation
                        if (($fileuploadmode == 'browseupload') || ($fileuploadmode == 'ajaxupload')) {
                            $extension = pathinfo($file['csvcreateteam']['newname'], PATHINFO_EXTENSION);
                            $name = pathinfo($file['csvcreateteam']['newname'], PATHINFO_FILENAME); // getting image extension
                            $outputfile = $name . '_error' . '.' . $extension;
                        }
                        $common_method->createFile($outputcsvdata['data_error'], $outputfile); 
                        
                        $status2 = $common_method->checkFileExist($outputfile, 'upload');
                        if ($status2 === false) {  //if no error file created due to any reason
                            $outputfile = 'upload_team_error.csv';
                        }
                        $erorRecords = count($outputcsvdata['data_error']) - 1;
                        if ($erorRecords > 1) { //for error message
                            $errorline = $erorRecords . ' records.';
                        } else {
                            $errorline = $erorRecords . ' record.';
                        }
                        //failure message
                        Session::flash('uploadteambulk_failure', array('errorline' => $errorline, 'downloadurl' => 'download/uploadteambulk/' . $outputfile));
                    } else {
                        if ($actioinStatus === 2) {
                            Session::flash('uploadteambulk_success', trans('messages.team_updated_success')); //success update message
                        } else {
                            Session::flash('uploadteambulk_success', trans('messages.team_created_success'));  //success insert message
                        }
                    }
                } else {
                    Session::flash('uploadteambulk_failure_empty', trans('messages.csv_no_record'));  //if csv has no data
                }
            }
            return Redirect::to('/');
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function postDeleteTeam(Request $request) {      //delete team
        try {
            $this->team_member = new TeamMember();
            $this->team = new Team();
            $teamid = $request->input('teamid');
            return $this->team->deleteTeam($teamid);
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function postDeleteSubordinate(Request $request) {   //subordinate delete
        try {
            $this->team_member = new TeamMember();
            $userid = $request->input('userid');
            $role = $request->input('role');
            return $this->team_member->deleteSubordinate($userid, $role);
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function getDownload($requestedfile, $filename) {    //download file        
        try {
            $this->common_method = new CommonMethod();
            $contentType = 'text/csv';
            $srcFileName = $filename;
            $destFileName = 'error.csv';
            if ($requestedfile == 'uploadteambulksample') {
                $destFileName = 'team_sample.csv';
            } else if ($requestedfile == 'uploaduserbulksample') {
                $destFileName = 'user_sample.csv';
            }
            $this->common_method->getDownload($srcFileName, $destFileName, $contentType);
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function postManagerDashBoard() {
        try {
            $this->team_member = new TeamMember();
            $validation_rules = array(
                'teamId' => 'required|exists:team,id',
                'userId' => 'required|exists:users,id',
                'pageNo' => 'required|min:1'
            );
            $validator = Validator::make($this->postData, $validation_rules);
            if ($validator->fails()) {
                return $this->validationError($validator);
            } else {
                $response = $this->team_member->getManagerDashboard($this->postData);
                if ($response === 0) {
                    return $this->renderFailure(trans('messages.user_not_manager'), Config::get('codes.unauthorized'));
                } else {
                    if ($response['status'] == 'true') {
                        $this->result = $response['result'];
                        $this->message = $response['message'];
                        return $this->render();
                    }
                }
            }
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    private function readFile($handle, $data, $i) {
        $subordinateemailidarr = $manageremailidarr = array();
        while (($rowtmp = fgetcsv($handle, 1000, ';')) !== FALSE) {  //reading CSV file
            $row = $error = $errordata = array();
            $errorinfield = false;
            if ($i > 0) { //skiping header row
                $subordinateemailid = (!empty($rowtmp[0])) ? trim($rowtmp[0]) : '';
                $manageremailid = (!empty($rowtmp[1])) ? trim($rowtmp[1]) : '';
                $errordata[] = $subordinateemailid;
                $errordata[] = $manageremailid;
                if (trim($subordinateemailid) == '') {  //validating subordinate email id
                    $error[0] = "Subordinate_Email_ID : " . trans('messages.csv_field_empty');
                    $errorinfield = true;
                } else if (filter_var($subordinateemailid, FILTER_VALIDATE_EMAIL) === false) {
                    $error[0] = "Subordinate_Email_ID : " . trans('messages.csv_email_incorrect');
                    $errorinfield = true;
                } else {
                    if (in_array($subordinateemailid, $subordinateemailidarr)) {
                        $error[0] = "Subordinate_Email_ID : " . trans('messages.csv_email_exist');
                        $errorinfield = true;
                    } else {
                        $subordinateemailidarr[] = $subordinateemailid;
                        $row[0] = $subordinateemailid;
                    }
                }
                if (trim($manageremailid) == '') {  //validating manager email id
                    $error[1] = "Manager_Email_ID : " . trans('messages.csv_field_empty');
                    $errorinfield = true;
                } else if (filter_var($manageremailid, FILTER_VALIDATE_EMAIL) === false) {
                    $error[1] = "Manager_Email_ID : " . trans('messages.csv_email_incorrect');
                    $errorinfield = true;
                } else {
                    if ($subordinateemailid == $manageremailid) {
                        $error[1] = "Manager_Email_ID : " . trans('messages.csv_email_same');
                        $errorinfield = true;
                    } else {
                        $manageremailidarr[] = $manageremailid;
                        $row[1] = $manageremailid;
                    }
                }
                if ($errorinfield === true) {
                    $row = array();
                }
                if (count($error) > 0) {
                    $data['data_error'][$i]['data'] = $errordata;
                    $data['data_error'][$i]['data'][2] = implode(', ', $error);
                }
                if (count($row) > 0) {
                    $data['data_success'][$i]['data'] = $row;
                }
            } else { //header row for output file if error found
                $data['data_error'][$i]['data'][0] = (!empty($rowtmp[0])) ? trim($rowtmp[0]) : 'Subordinate_Email_ID';
                $data['data_error'][$i]['data'][1] = (!empty($rowtmp[1])) ? trim($rowtmp[1]) : 'Manager_Email_ID';
                $data['data_error'][$i]['data'][2] = 'Error Found';
            }
            $i++;
        }
        $returnArr['data'] = $data;
        $returnArr['i'] = $i;
        return $returnArr;
    }

    private function processFile($data_success, $data_error, $actioinStatus) {
        $createdBy = Auth::User()->id;
        $sessioncompanyId = \Session::get('companyId');
        $userObj = new Users();
        $teamObj = new Team();
        $teamMemberObj = new TeamMember();
        $dtTime = date('Y-m-d H:i:s');
        $toBeInsertUserIdArr = array();
        foreach ($data_success as $rowdataTmp) {
            $toBeInsertUserIdArr[] = $rowdataTmp['data'][0];
            $toBeInsertUserIdArr[] = $rowdataTmp['data'][1];
        }

        $allUserIdArr = $userObj->getUserIDArr($toBeInsertUserIdArr, 'email');
        $userIDArr = array_values($allUserIdArr);
        $companyIdArr = $teamMemberObj->getUserCompanyIDArr($userIDArr);

        foreach ($data_success as $rowid => $rowdata) {
            $teamID = $teamIDTmp = $subordinateteamMemberID = $managerteamMemberID = $manageruserid = $subordinateuserid = $companyid = 0;
            $error = $teamData = array();
            $validsubordinatetoupdateteam = $insertnewsubordinateteammember = $validmanagertoupdateteam = $insertnewmanagerteammember = false;  //for subordinate for updating if found valid
            $tobeinsertsubordinateid = $rowdata['data'][0]; //subordinate id
            $tobeinsertmanagerid = $rowdata['data'][1]; //manager id
            if (array_key_exists($tobeinsertmanagerid, $allUserIdArr)) {
                $manageruserid = $allUserIdArr[$tobeinsertmanagerid];
            }
            if ($manageruserid > 0) {
                $companyid = $companyIdArr[$manageruserid]['companyId'];
                if ($companyid === $sessioncompanyId) {
                    $returnArr = $teamMemberObj->getAssignedTeamID($manageruserid, static::MANAGER_ROLE_ID);
                    if (count($returnArr) > 0) {
                        $teamID = $returnArr['teamId'];
                        $managerteamMemberID = $returnArr['id'];
                    }
                    if ($teamID === NULL) {
                        $validmanagertoupdateteam = true;
                    } else if ($teamID > 0) {
                        $actioinStatus = 2;
                    } else {
                        $insertnewmanagerteammember = true;
                    }
                } else { //if manager relate to other company
                    $error[1] = "Manager_Email_ID : " . trans('messages.csv_email_othercompany');
                }
            } else {  //if manager is not registered
                $error[1] = "Manager_Email_ID : " . trans('messages.csv_email_notregistered');
            }
            if (array_key_exists($tobeinsertsubordinateid, $allUserIdArr)) {
                $subordinateuserid = $allUserIdArr[$tobeinsertsubordinateid];
            }
            if ($subordinateuserid > 0) {
                $companyid = $companyIdArr[$subordinateuserid]['companyId'];
                if ($companyid === $sessioncompanyId) {
                    $returnArr = $teamMemberObj->getAssignedTeamID($subordinateuserid, static::SUBORDINATE_ROLE_ID);
                    if (count($returnArr) > 0) {
                        $teamIDTmp = $returnArr['teamId'];
                        $subordinateteamMemberID = $returnArr['id'];
                    }
                    if ($teamIDTmp === NULL) {
                        $validsubordinatetoupdateteam = true;
                    } else if ($teamIDTmp > 0) {  //if subordinate already assigned to a team
                        $error[0] = "Subordinate_Email_ID : " . trans('messages.csv_email_alreadyassigned');
                    } else {
                        $insertnewsubordinateteammember = true;
                    }
                } else {  //if subordinate relates to other company
                    $error[0] = "Subordinate_Email_ID : " . trans('messages.csv_email_othercompany');
                }
            } else { //if subordinate is not registered
                $error[0] = "Subordinate_Email_ID : " . trans('messages.csv_email_notregistered');
            }
            if (count($error) > 0) {
                $data_error[$rowid] = $data_success[$rowid];
                $data_error[$rowid]['data'][2] = implode(', ', $error);
                unset($data_success[$rowid]);
            } else {
                $teamData = array('company_id' => $sessioncompanyId, 'created_by' => $createdBy, 'updated_by' => $createdBy, 'created_at' => $dtTime, 'updated_at' => $dtTime);
                if ($validmanagertoupdateteam === true) {
                    $teamID = $teamObj->createTeam($teamData);
                    $teamMemberObj->updateField(array('teamMemberID' => $managerteamMemberID, 'team_id' => $teamID));
                    $deleteLoginDeviceUser = $teamMemberObj->deleteLoginDeviceUser(array($manageruserid));
                } else if ($insertnewmanagerteammember === true) {
                    $teamID = $teamObj->createTeam($teamData);
                    $designationArr = $teamMemberObj->getDesignationID(array($manageruserid));
                    $designation_id = $designationArr[$manageruserid]['designationId'];
                    $teamMemberObj->insertField(array('user_id' => $manageruserid, 'team_id' => $teamID, 'designation_id' => $designation_id, 'company_id' => $sessioncompanyId, 'role' => static::MANAGER_ROLE_ID, 'created_at' => $dtTime, 'updated_at' => $dtTime));
                    $deleteLoginDeviceUser = $teamMemberObj->deleteLoginDeviceUser(array($manageruserid));
                }
                if ($validsubordinatetoupdateteam === true) {
                    $teamMemberObj->updateField(array('teamMemberID' => $subordinateteamMemberID, 'team_id' => $teamID));
                    $deleteLoginDeviceUser = $teamMemberObj->deleteLoginDeviceUser(array($subordinateuserid));
                } else if ($insertnewsubordinateteammember === true) {
                    $designationArr = $teamMemberObj->getDesignationID(array($subordinateuserid));
                    $designation_id = $designationArr[$subordinateuserid]['designationId'];
                    $teamMemberObj->insertField(array('user_id' => $subordinateuserid, 'team_id' => $teamID, 'designation_id' => $designation_id, 'company_id' => $sessioncompanyId, 'role' => static::SUBORDINATE_ROLE_ID, 'created_at' => $dtTime, 'updated_at' => $dtTime));
                    $deleteLoginDeviceUser = $teamMemberObj->deleteLoginDeviceUser(array($subordinateuserid));
                }
            }
        }
        $returnData['data_success'] = $data_success;
        $returnData['data_error'] = $data_error;
        $returnData['actioinStatus'] = $actioinStatus;
        return $returnData;
    }

}
