<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Utility\CommonMethod;

class DesignationController extends UtilityController {

    public function __construct() {
        parent:: __construct();
        $this->middleware('loginputs');
        $this->middleware('xss');
        $this->company = new Company();
    }

    public function index() {
        try {
            $companyId = \Session::get('companyId');
            $this->result = \App\Models\Designation::select('id', 'title', 'comapny_id')->where('comapny_id', $companyId)->get();
            return $this->render();
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

}
