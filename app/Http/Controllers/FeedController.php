<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use App\Models\Feed;
use App\Models\FeedLike;
use App\Utility\CommonMethod;

class FeedController extends UtilityController {

    public function __construct() {
        parent:: __construct();
        $this->middleware('validatejson', ['except' => ['index']]);
        $this->middleware('apiauth', ['except' => ['']]);
        $request = \Request::instance();
        $this->postData = json_decode($request->getContent(), true);
    }

    public function postGenerateFeed() {
        try {
            $this->feed = new Feed();
            $validation_rules = array(
                'userId' => 'required|exists:users,id',
                'teamId' => 'required|exists:team,id',
                'companyId' => 'required|exists:company,id',
                'pageNo' => 'required|min:1',
                'flag1' => 'required',
                'flag2' => 'required',
            );
            $messages = array('teamId.exists' => 'You are not currently member of a team.',);
            if ($this->postData['flag2'] != 0) {
                $validation_rules['flag2'] = 'required|exists:department,id';
            }
            $validator = Validator::make($this->postData, $validation_rules, $messages);
            if ($validator->fails()) {
                return $this->validationError($validator);
            } else {
                $response = $this->feed->generateFeed($this->postData);
                if ($response['status'] == 'true') {
                    $this->result = $response['result'];
                    $this->message = $response['message'];
                    return $this->render();
                }
                return $this->render();
            }
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function postLikeFeed() {
        try {
            $this->feedLike = new FeedLike();
            $validation_rules = array(
                'userId' => 'required|exists:users,id',
                'feedId' => 'required|exists:feed,id',
            );
            $validator = Validator::make($this->postData, $validation_rules);
            if ($validator->fails()) {
                return $this->validationError($validator);
            } else {
                $this->result = $this->feedLike->likeFeed($this->postData);
                return $this->render();
            }
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

}
