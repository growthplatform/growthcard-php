<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\Models\Company;
use App\Models\TeamMember;
use App\Models\Users;
use App\Models\Designation;
use App\Models\Department;
use App\Models\Images;
use App\Models\EffectiveAction;
use App\Utility\CommonMethod;
use Auth;
use Illuminate\Support\Facades\Hash;
use Config;
use Illuminate\Support\Facades\Input;

class UserWebController extends UtilityController {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    const MANAGER_ROLE_ID = 1;
    const SUBORDINATE_ROLE_ID = 2;

    public function __construct() {
        $this->middleware('auth');
        $this->middleware('xss');
        parent:: __construct();
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index() {
        try {
            $this->company = new Company();
            $this->user = new Users();
            $user_type = Auth::User()->user_type;
            if ($user_type == 1) {
                $data = $this->company->getCompaiesList()->get();
                return view('company')->with('data', $data);
            } elseif ($user_type == 3) {
                Session::flush();
                Auth::logout();
                return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/');
            } else {
                $companyId = Company::select('id')->where('company_admin', Auth::User()->id)->first();
                Session::put('companyId', $companyId->id);
                $response = $this->getDasboardData();
                if (is_object($response['data']) && is_array($response['teamData'])) {
                    return view('index')->with('data', $response['data'])->with('teamData', $response['teamData'])->with('userType', (int) $user_type);
                } elseif (is_array($response['data'])) {
                    return view('index')->with('data', $response['data'])->with('userType', (int) $user_type);
                } else {
                    return view('index')->with('userType', (int) $user_type);
                }
            }
        } catch (\Exception $e) {
            CommonMethod::logingException($e);            
            return view('606');
        }
    }

    public function getDasboardData() {
        try {
            $this->user = new Users();
            $companyId = \Session::get('companyId');
            $data = $this->user->getUserAllDataWeb()->where('user_type', 3)
                    ->whereHas('memberweb', function($q) use($companyId) {
                        // Query the name field in team_member table
                        $q->where('company_id', $companyId);
                    })
                    ->get();
            $this->team_member = new TeamMember();
            $tmpteamData = $this->team_member->getTeam($companyId);
            $teamData = array();
            if (count($tmpteamData) > 0) {
                foreach ($tmpteamData as $tmpData) {
                    if ((int) $tmpData->role === static::MANAGER_ROLE_ID) {
                        $teamData[$tmpData->team_id]['id'] = $tmpData->id;
                        $teamData[$tmpData->team_id]['user_id'] = $tmpData->user_id;
                        $teamData[$tmpData->team_id]['company_id'] = $tmpData->company_id;
                        $teamData[$tmpData->team_id]['designation_id'] = $tmpData->designation_id;
                        $teamData[$tmpData->team_id]['first_name'] = $tmpData->user->first_name;
                        $teamData[$tmpData->team_id]['last_name'] = $tmpData->user->last_name;
                        $teamData[$tmpData->team_id]['profile_image'] = $tmpData->user->profile_image;
                        $teamData[$tmpData->team_id]['email'] = $tmpData->user->email;
                        $teamData[$tmpData->team_id]['designation'] = ucwords(strtolower($tmpData->designation->title));
                        $teamData[$tmpData->team_id]['department'] = $tmpData->userdepartment->department->department_name;
                    } else if ((int) $tmpData->role === static::SUBORDINATE_ROLE_ID) {
                        $teamData[$tmpData->team_id]['subordinates'][$tmpData->user_id]['id'] = $tmpData->id;
                        $teamData[$tmpData->team_id]['subordinates'][$tmpData->user_id]['user_id'] = $tmpData->user_id;
                        $teamData[$tmpData->team_id]['subordinates'][$tmpData->user_id]['company_id'] = $tmpData->company_id;
                        $teamData[$tmpData->team_id]['subordinates'][$tmpData->user_id]['designation_id'] = $tmpData->designation_id;
                        $teamData[$tmpData->team_id]['subordinates'][$tmpData->user_id]['first_name'] = $tmpData->user->first_name;
                        $teamData[$tmpData->team_id]['subordinates'][$tmpData->user_id]['profile_image'] = $tmpData->user->profile_image;
                        $teamData[$tmpData->team_id]['subordinates'][$tmpData->user_id]['last_name'] = $tmpData->user->last_name;
                        $teamData[$tmpData->team_id]['subordinates'][$tmpData->user_id]['email'] = $tmpData->user->email;
                        $teamData[$tmpData->team_id]['subordinates'][$tmpData->user_id]['designation'] = ucwords(strtolower($tmpData->designation->title));
                        $teamData[$tmpData->team_id]['subordinates'][$tmpData->user_id]['department'] = $tmpData->userdepartment->department->department_name;
                    }
                }
            }
            if (!isset($data)) {
                $data = array();
            }
            return array('data' => $data, 'teamData' => $teamData);
        } catch (\Exception $e) {
            CommonMethod::logingException($e);            
            return view('606');
        }
    }

    public function storeCompanyId($id) {
        Session::put('companyId', $id);
        return;
    }

    public function store() {
        try {
            $this->user = new Users();
            $inputs = Input::all();
            $validation_rules = array(
                'firstName' => 'required|max:50',
                'lastName' => 'required|max:50',
                'email' => 'required|email|unique:users,email',
                'password' => 'required',
                'companyName' => 'required|max:100',
                'job' => 'required|exists:designation,id',
                'department' => 'required|exists:department,id'
            );
            $validator = Validator::make($inputs, $validation_rules);
            if ($validator->fails()) {
                return $this->validationError($validator);
            } else {
                $this->user->createUser($inputs);
            }
            return $this->render();
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function edit($id) { //return prefill data for edit
        try {
            $this->user = new Users();
            $data = $this->user->getUserAllData()->where('id', $id)->first();
            $this->result = $data;
            return $this->render();
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function update($id) {
        try {
            $this->user = new Users();
            $inputs = Input::all();
            $user = Users::findOrFail($id);
            $validation_rules = array(
                'firstName' => 'required|max:50',
                'lastName' => 'required|max:50',
                'password' => 'required',
                'companyName' => 'required|max:100',
                'userId' => 'required|exists:users,id',
                'job' => 'required|exists:designation,id',
                'department' => 'required|exists:department,id'
            );
            if ($user->email != $inputs['email']) {
                $validation_rules['email'] = 'required|email|unique:users,email';
            }
            $validator = Validator::make($inputs, $validation_rules);
            if ($validator->fails()) {
                return $this->validationError($validator);
            } else {
                $this->user->updateUser($inputs);
            }
            return $this->render();
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function getDashboard() {
        try {
            $user_type = Auth::User()->user_type;
            $response = $this->getDasboardData();
            $menuid = 1;
            Session::put('menuid', $menuid);
            if (is_object($response['data']) && is_array($response['teamData'])) {
                return view('index')->with('data', $response['data'])->with('teamData', $response['teamData'])->with('userType', (int) $user_type);
            } elseif (is_array($response['data'])) {
                return view('index')->with('data', $response['data'])->with('userType', (int) $user_type);
            } else {
                return view('index')->with('userType', (int) $user_type);
            }
        } catch (\Exception $e) {
            CommonMethod::logingException($e);            
            return view('606');
        }
    }

    public function getSetting() {
        try {
            $this->user = new Users();
            $menuid = 4;
            \Session::put('menuid', $menuid);
            $data = $this->user->getUserNamePassword();
            return view('setting')->with('data', $data);
        } catch (\Exception $e) {
            CommonMethod::logingException($e);
            return view('606');
        }
    }

    public function getUser() {
        try {
            $this->company = new Company();
            $user_type = Auth::User()->user_type;
            $companyId = Session::get('companyId');
            $menuid = 2;
            Session::put('menuid', $menuid);
            $data = TeamMember::with('company', 'designation', 'effective', 'userdepartment.department', 'user.days')
                    ->where('company_id', $companyId)
                    ->groupBy('user_id')
                    ->get();
            return view('users')->with('data', $data)->with('userType', (int) $user_type);
        } catch (\Exception $e) {
            CommonMethod::logingException($e);
            return view('606');
        }
    }

    public function getEffectiveAction() {
        try {
            $this->designation = new Designation();
            $companyId = Session::get('companyId');
            $menuid = 3;
            Session::put('menuid', $menuid);
            $data = $this->designation->where('comapny_id', $companyId)->with('effectiveAction')->get();
            return view('effectiveActions')->with('data', $data);
        } catch (\Exception $e) {
            CommonMethod::logingException($e);
            return view('606');
        }
    }

    public function postChangePassword() {
        try {
            $this->user = new Users();
            $inputs = Input::all();
            $validation_rules = array(
                'oldPassword' => 'required',
                'newPassword' => 'required',
            );
            $validator = Validator::make($inputs, $validation_rules);
            if ($validator->fails()) {
                return $this->validationError($validator);
            }
            $user_id = Auth::User()->id;
            $user = Users::where('id', $user_id)->first();
            if (Hash::check($inputs['newPassword'], $user->password)) {
                $this->renderFailure(trans('messages.match_pass'), Config::get('codes.unauthorized'));
            }
            if (Auth::attempt(['password' => $inputs['oldPassword'], 'id' => $user_id])) {
                $this->user->updatePassword($inputs['newPassword'], $user_id);
            } else {
                $this->renderFailure(trans('messages.incorrect_pass'), Config::get('codes.unauthorized'));
            }
            return $this->render();
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function destroy($id, Request $request) {
        try {
            $this->user = new Users();
            $companyId['userId'] = $id;
            $validation_rules = array(
                'userId' => 'required|exists:users,id'
            );
            $validator = Validator::make($companyId, $validation_rules);
            if ($validator->fails()) {
                return $this->validationError($validator);
            } else {
                $flag = $request->input('flag');
                $response = $this->user->destroyUser($id, $flag);
                if ($response == 1) {
                    $this->renderFailure(trans('messages.user_is_manager'), Config::get('codes.manager_delete_confirm'));
                }
            }
            return $this->render();
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function createJob() {
        try {
            $this->designation = new Designation();
            $inputs = Input::all();
            $validation_rules = array(
                'jobName' => 'required|max:40',
            );
            $validator = Validator::make($inputs, $validation_rules);
            if ($validator->fails()) {
                return $this->validationError($validator);
            } else {
                $this->designation->createJob($inputs);
            }
            return $this->render();
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function createEffectiveAction() {
        try {
            $this->effectiveaction = new EffectiveAction();
            $inputs = Input::all();
            $validation_rules = array(
                'effectiveAction' => 'required|max:140',
                'Jobtitle' => 'required|exists:designation,id',
            );
            $validator = Validator::make($inputs, $validation_rules);
            if ($validator->fails()) {
                return $this->validationError($validator);
            } else {
                $this->effectiveaction->createEffectiveAction($inputs);
            }
            return $this->render();
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function deleteEffectiveAction() {
        try {
            $this->effectiveaction = new EffectiveAction();
            $inputs = Input::all();
            $validation_rules = array(
                'id' => 'required'
            );
            $validator = Validator::make($inputs, $validation_rules);
            if ($validator->fails()) {
                return $this->validationError($validator);
            } else {
                $this->effectiveaction->destroyEffectiveAction($inputs);
            }
            return $this->render();
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function editEffectiveAction() {
        try {
            $this->effectiveaction = new EffectiveAction();
            $inputs = Input::all();
            $validation_rules = array(
                'id' => 'required|exists:effective_action,id',
                'jobid' => 'required|exists:designation,id'
            );
            $validator = Validator::make($inputs, $validation_rules);
            if ($validator->fails()) {
                return $this->validationError($validator);
            } else {
                $this->effectiveaction->editEffectiveAction($inputs);
            }
            return $this->render();
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function editJob() {
        try {
        $this->designation = new Designation();
        $inputs = Input::all();
        $validation_rules = array(
            'id' => 'required|exists:designation,id',
            'jobName' => 'required|max:40',
        );
        $validator = Validator::make($inputs, $validation_rules);
        if ($validator->fails()) {
            return $this->validationError($validator);
        } else {
            $this->designation->editJob($inputs);
        }
        return $this->render();
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function createDepartment() {
        try {
            $this->department = new Department();
            $inputs = Input::all();
            $validation_rules = array(
                'departmentName' => 'required|max:40',
            );
            $validator = Validator::make($inputs, $validation_rules);
            if ($validator->fails()) {
                return $this->validationError($validator);
            } else {
                $this->department->createDepartment($inputs);
            }
            return $this->render();
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    //creating user using csv upload
    public function postCreateUserBulk(Request $request) {        
            $common_method = new CommonMethod();
            //**start of ajax upload**//            
            if ($request->ajax()) {
                $file = Input::file('ajaxcsvoperation');
                $fileArray = array('attachedFile' => $file[0]);
                $rules = array(
                    'attachedFile' => 'required|mimes:csv,txt|max:5120'
                );
                $validator = Validator::make($fileArray, $rules);

                if ($validator->fails()) {
                    return Redirect::back()->withErrors($validator->errors()->toArray());
                } else {
                    if (Input::hasFile('ajaxcsvoperation')) {
                        $common_method->ajaxUpload($file[0], 'upload');
                    }
                }
            } //**end of ajax upload**//    
            // getting all of the post data
            $fileuploadmode = isset($request['fileuploadmode']) ? $request['fileuploadmode'] : '';
            if ($fileuploadmode == 'browseupload') {
                $file = $common_method->postCSVFileUpload('csvcreateteam', $request['ajaxfilenewname']);
            } else if ($fileuploadmode == 'ajaxupload') {
                $file = array('csvcreateteam' => array('name' => $request['ajaxfilename'], 'newname' => $request['ajaxfilenewname'], 'type' => $request['ajaxfilemimetype'], 'tmp_name' => $request['ajaxfilepath'], 'error' => 0, 'size' => $request['ajaxfilesize']));
            }            
            $status = $common_method->checkFileExist($file['csvcreateteam']['newname'], 'upload');
            if($status === false){
                // get the error messages from the validator
                // send back to the page with the input data and errors
                Session::flash('fileupload_failure', array('filenotexist' => trans('messages.file_not_uploaded')));
            } else {                
                $userObj = new Users();
                $data = array();
                $i = 0;
                $actualfilepath = $common_method->getFolderName(env('STORAGETYPE'), 'upload', 'read').$file['csvcreateteam']['newname'];
                if (($handle = fopen($actualfilepath, 'r')) !== FALSE) { //opening csv file                                    
                    $readFileDataArr = $this->readFile($handle, $data, $i);
                    $data = $readFileDataArr['data'];
                    $i = $readFileDataArr['i'];
                    fclose($handle);
                }                
                $outputcsvdata = array();
                if (count($data['data_error'] > 1)) {
                    $outputcsvdata['data_error'] = $data['data_error'];
                }
                if (isset($data['data_success']) && count($data['data_success']) > 0) { //if no error found in csv data
                    foreach ($data['data_success'] as $rowdata) {
                        $userObj->createUser(array('firstName' => $rowdata['data'][0], 'lastName' => $rowdata['data'][1], 'password' => $rowdata['data'][3], 'email' => $rowdata['data'][2], 'job' => $rowdata['data'][4], 'department' => $rowdata['data'][5]));
                    }
                }
                if ($i > 1) {
                    //creating error file
                    if (count($outputcsvdata['data_error']) > 1) {
                        if ($fileuploadmode == 'browseupload' || $fileuploadmode == 'ajaxupload') {
                            $extension = pathinfo($file['csvcreateteam']['newname'], PATHINFO_EXTENSION);
                            $name = pathinfo($file['csvcreateteam']['newname'], PATHINFO_FILENAME); // getting image extension
                            $outputfile = $name . '_error' . '.' . $extension;
                        }
                        $common_method->createFile($outputcsvdata['data_error'], $outputfile); 
                        
                        $status2 = $common_method->checkFileExist($outputfile, 'upload');
                        if ($status2 === false) {  //if no error file created due to any reason
                            $outputfile = 'create_user_error.csv';
                        }     
                        $erorRecords = count($outputcsvdata['data_error']) - 1;
                        if ($erorRecords > 1) {
                            $errorline = $erorRecords . ' records.';
                        } else {
                            $errorline = $erorRecords . ' record.';
                        }  
                        Session::flash('createuserbulk_failure', array('errorline' => $errorline, 'downloadurl' => 'download/createuserbulk/' . $outputfile));                        
                    } else {
                        Session::flash('createuserbulk_success', trans('messages.user_created_success'));
                    }
                } else {
                    Session::flash('createuserbulk_failure_empty', trans('messages.csv_no_record'));
                }
            }
            return Redirect::to('users');        
    }

    public function getManageImage() {
        try {
            $menuid = 5;
            $common_method = new CommonMethod();
            Session::put('menuid', $menuid);
            $user_id = Auth::User()->id;
            $user_type = (int) Auth::User()->user_type;
            if ($user_type === 1) {
                $companyId = 0;
            } else {
                $this->company = new Company();
                $companyId = $this->company->getUserCompanyID($user_id);
            }

            $this->images = new Images();
            $imagedataarr = $this->images->getAllImages($companyId, $user_id);
            //echo "imagedataarr<pre>";
            //print_r($imagedataarr);exit;

            $storageType = env('STORAGETYPE');
            $destination_path = $common_method->getAliasFolderName($storageType, 'image');

            return view('manageImage')->with('imagedataarr', $imagedataarr)->with('destination_path', $destination_path);
        } catch (\Exception $e) {
            CommonMethod::logingException($e);
            return view('606');
        }
    }

    //to upload images using ajax in manage image work
    public function postUploadImageBulk(Request $request) {
        try {            
            if ($request->ajax()) {
                $file = Input::file('ajaximageoperation');
                $fileArray = array('attachedFile' => $file[0]);
                $rules = array(
                    'attachedFile' => 'mimes:jpeg,jpg,png,gif|required|max:5120' // max 5120 i.e 5 mb
                );
                $validator = Validator::make($fileArray, $rules);

                if ($validator->fails()) {
                    return Redirect::back()->withErrors($validator->errors()->toArray());
                } else {
                    if (Input::hasFile('ajaximageoperation')) {
                        $common_method = new CommonMethod();
                        $common_method->ajaxUpload($file[0], 'image');
                    }
                }
            }
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    //to delete image
    public function postDeleteImage(Request $request) {
        try {
            $id = isset($request['id']) ? $request['id'] : 0;
            $this->images = new Images();
            $dataArr['deleted_at'] = date('Y-m-d H:i:s');
            echo $this->images->updateField($dataArr, $id);
            exit;
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    //to upload images in normal submission in manage image work
    public function postSaveUploadImage(Request $request) {
        try {            
            $commonMethod = new CommonMethod();
            $inputs = Input::all();
            $existingDataArr = array();
            $newDataArr = array();
            $captionArr = (isset($inputs['caption']) && count($inputs['caption']) > 0) ? $inputs['caption'] : array();
            $new_captionArr = (isset($inputs['new_caption']) && count($inputs['new_caption']) > 0) ? $inputs['new_caption'] : array();
            $messageArr = (isset($inputs['message']) && count($inputs['message']) > 0) ? $inputs['message'] : array();
            $new_messageArr = (isset($inputs['new_message']) && count($inputs['new_message']) > 0) ? $inputs['new_message'] : array();
            $oldfileArr = Input::hasFile('uploadedimage') ? Input::file('uploadedimage') : 'NO_FILE_UPLOADED';
            $newsubmitfileArr = Input::hasFile('new_uploadedimage') ? Input::file('new_uploadedimage') : 'NO_AJAX_SUBMIT_FILE_UPLOADED';
            
            $newfileArr = (isset($inputs['ajaxupload']) && count($inputs['ajaxupload']) > 0) ? $inputs['ajaxupload'] : 'NO_AJAX_FILE_UPLOADED';
            if (count($captionArr) > 0) {
                foreach ($captionArr as $id1 => $value1) {
                    $existingDataArr[$id1]['caption'] = $value1;
                }
            }

            if (count($messageArr) > 0) {
                foreach ($messageArr as $id2 => $value2) {
                    $existingDataArr[$id2]['message'] = $value2;
                }
            }

            if (count($new_captionArr) > 0) {
                foreach ($new_captionArr as $id3 => $value3) {
                    $newDataArr[$id3]['caption'] = $value3;
                }
            }

            if (count($new_messageArr) > 0) {
                foreach ($new_messageArr as $id4 => $value4) {
                    $newDataArr[$id4]['message'] = $value4;
                }
            }

            if ($oldfileArr != 'NO_FILE_UPLOADED') {  //if no already uploaded file found                
                foreach ($oldfileArr as $id5 => $value5) {
                    if($value5 !== NULL){
                        $newname = $commonMethod->upload($value5, 'image');                            
                        $status = $commonMethod->checkFileExist($newname, 'image');
                        if ($status === true) {
                            $existingDataArr[$id5]['name'] = $newname;
                        }
                    }
                }
            }                        
            foreach ($existingDataArr as $id => $dataArr) {
                $this->images = new Images();
                $updateStatus = $this->images->updateField($dataArr, $id);
            }
            
            if ($newsubmitfileArr != 'NO_AJAX_SUBMIT_FILE_UPLOADED') {  //if no already uploaded file found                
                foreach ($newsubmitfileArr as $id8 => $value8) {
                    if($value8 !== NULL){
                        $newname2 = $commonMethod->upload($value8, 'image');                            
                        $status = $commonMethod->checkFileExist($newname2, 'image');
                        if ($status === true) {                            
                            $newDataArr[$id8]['name'] = $newname2;
                            $newfileArr['name'][$id8] = '';
                            $newfileArr['newname'][$id8] = '';
                            $newfileArr['type'][$id8] = '';
                            $newfileArr['tmp_name'][$id8] = '';
                            $newfileArr['error'][$id8] = '';
                            $newfileArr['size'][$id8] = '';
                        }
                    }
                }
            }
            
            if ($newfileArr != 'NO_AJAX_FILE_UPLOADED') { //if new file uploaded using ajax
                foreach ($newfileArr['newname'] as $id6 => $value6) {
                    if ($value6 !== '') {
                        $status = $commonMethod->checkFileExist($value6, 'image');
                        if ($status === true) {
                            $newDataArr[$id6]['name'] = $value6;
                        }
                    }
                }
            }

            $user_id = Auth::User()->id;
            $user_type = (int) Auth::User()->user_type;
            $this->company = new Company();
            $this->images = new Images();
            $companyId = $this->company->getUserCompanyID($user_id);
            foreach ($newDataArr as $dataArr7) {
                if ($user_type === 1) {
                    $companyId = 0;
                }
                $dataArr7['created_by'] = $user_id;
                $dataArr7['updated_by'] = $user_id;
                $dataArr7['company_id'] = $companyId;
                $dataArr7['created_at'] = date('Y-m-d H:i:s');
                $updateStatus = $this->images->insertField($dataArr7);
            }

            Session::flash('imageupload_success', trans('messages.image_upload_success'));
            return Redirect::to('manage-image');
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    private function readFile($handle, $data, $i) {        
        $userObj = new Users();
        $sessioncompanyId = \Session::get('companyId');
        while (($rowtmp = fgetcsv($handle, 1000, ';')) !== FALSE) { //reading csv file
            $row = $error = $errordata = array();
            $errorinfield = false;
            if ($i > 0) { //skiping first record
                $designationID = $departmentID = 0;
                $firstName = (!empty($rowtmp[0])) ? trim($rowtmp[0]) : '';
                $lastName = (!empty($rowtmp[1])) ? trim($rowtmp[1]) : '';
                $email = (!empty($rowtmp[2])) ? trim($rowtmp[2]) : '';
                $password = (!empty($rowtmp[3])) ? trim($rowtmp[3]) : '';
                $jobTitle = (!empty($rowtmp[4])) ? trim($rowtmp[4]) : '';
                $department = (!empty($rowtmp[5])) ? trim($rowtmp[5]) : '';
                $errordata[] = $firstName;    //to collect first name related error data so that we can print it in error file
                $errordata[] = $lastName; //to collect last name related error data so that we can print it in error file
                $errordata[] = $email; //to collect email related error data so that we can print it in error file
                $errordata[] = $password; //to collect password related error data so that we can print it in error file
                $errordata[] = $jobTitle; //to collect jobTitle related error data so that we can print it in error file
                $errordata[] = $department; //to collect department related error data so that we can print it in error file
                //validate first name
                if (trim($firstName) == '') {
                    $error[0] = "First_Name : " . trans('messages.csv_field_empty');
                    $errorinfield = true;
                } else {
                    $firstnamevalidator = Validator::make(array('firstName' => $firstName), array('firstName' => 'required|max:50'));
                    if ($firstnamevalidator->fails()) {
                        $error[0] = "First_Name : '" . $firstnamevalidator->errors()->first() . "'";
                        $errorinfield = true;
                    } else {
                        $row[0] = $firstName;
                    }
                }
                //validate last name
                if (trim($lastName) == '') {
                    $error[1] = "Last_Name : " . trans('messages.csv_field_empty');
                    $errorinfield = true;
                } else {
                    $lastnamevalidator = Validator::make(array('lastName' => $lastName), array('lastName' => 'required|max:50'));
                    if ($lastnamevalidator->fails()) {
                        $error[1] = "Last_Name : '" . $lastnamevalidator->errors()->first() . "'";
                        $errorinfield = true;
                    } else {
                        $row[1] = $lastName;
                    }
                }

                //validate email
                if (trim($email) == '') {
                    $error[2] = "Email_ID : " . trans('messages.csv_field_empty');
                    $errorinfield = true;
                } else if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
                    $error[2] = "Email_ID : " . trans('messages.csv_email_incorrect');
                    $errorinfield = true;
                } else {
                    $userID = $userObj->getUserID($email);                    
                    if ($userID > 0) {
                        $error[2] = "Email_ID : " . trans('messages.csv_email_alreadyused');
                        $errorinfield = true;
                    } else {
                        $row[2] = $email;
                    }
                }                
                //validate password
                if (trim($password) == '') {
                    $error[3] = "Password : " . trans('messages.csv_field_empty');
                    $errorinfield = true;
                } else {
                    $passwordvalidator = Validator::make(array('password' => $password), array('password' => 'required|min:6|max:12'));
                    if ($passwordvalidator->fails()) {
                        $error[3] = "Password : '" . $passwordvalidator->errors()->first() . "'";
                        $errorinfield = true;
                    } else {
                        $row[3] = $password;
                    }
                }

                //validate job title
                if (trim($jobTitle) == '') {
                    $error[4] = "Job_Title : " . trans('messages.csv_field_empty');
                    $errorinfield = true;
                } else {
                    $designationObj = new Designation();
                    $designationID = $designationObj->getDesignationID($jobTitle, $sessioncompanyId);
                    if ($designationID > 0) {
                        $row[4] = $designationID;
                    } else {
                        $error[4] = "Job_Title : " . trans('messages.csv_no_exist');
                        $errorinfield = true;
                    }
                }

                //validate department
                if (trim($department) == '') {
                    $error[5] = "Department : " . trans('messages.csv_field_empty');
                    $errorinfield = true;
                } else {
                    $departmentObj = new Department();
                    $departmentID = $departmentObj->getDepartmentID($department, $sessioncompanyId);
                    if ($departmentID > 0) {
                        $row[5] = $departmentID;
                    } else {
                        $error[5] = "Department : " . trans('messages.csv_no_exist');
                        $errorinfield = true;
                    }
                }
                if ($errorinfield === true) {
                    $row = array();
                }
                if (count($error) > 0) {
                    $data['data_error'][$i]['data'] = $errordata;
                    $data['data_error'][$i]['data'][6] = implode(', ', $error);
                }
                if (count($row) > 0) {
                    $data['data_success'][$i]['data'] = $row;
                }
            } else { //setting header row
                $data['data_error'][$i]['data'][0] = (!empty($rowtmp[0])) ? trim($rowtmp[0]) : 'First_Name';
                $data['data_error'][$i]['data'][1] = (!empty($rowtmp[1])) ? trim($rowtmp[1]) : 'Last_Name';
                $data['data_error'][$i]['data'][2] = (!empty($rowtmp[2])) ? trim($rowtmp[2]) : 'Email_ID';
                $data['data_error'][$i]['data'][3] = (!empty($rowtmp[3])) ? trim($rowtmp[3]) : 'Password';
                $data['data_error'][$i]['data'][4] = (!empty($rowtmp[4])) ? trim($rowtmp[4]) : 'Job_Title';
                $data['data_error'][$i]['data'][5] = (!empty($rowtmp[5])) ? trim($rowtmp[5]) : 'Department';
                $data['data_error'][$i]['data'][6] = 'Error Found';
            }
            $i++;
        }
        $returnArr['data'] = $data;
        $returnArr['i'] = $i;
        return $returnArr;
    }

}
