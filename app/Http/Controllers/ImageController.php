<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Users;
use App\Repositories\File\FileRepositoryLocal;
use App\Utility\CommonMethod;

class ImageController extends UtilityController {

    public function __construct(FileRepositoryLocal $file) {
        parent:: __construct();
        $this->file = $file;
        $this->common_method = new CommonMethod();
        $this->user = new Users();
    }

    public function postSaveImage(Request $request, $dataarr = array()) {
        try {
            $file = $request->file('image');
            $user_id = $request->input('userId');
            $type = 1;
            if (count($dataarr) > 0) {
                $action = isset($dataarr['action']) ? $dataarr['action'] : '';
                if ($action == 'appimages') {
                    $type = 2;
                }
            }
            $image = $this->file->upload($file, $type);
            $user = Users::find($user_id);
            $user->profile_image = $image['image'];
            $user->save();
            $this->result = $this->user->getUserAllData()->where('id', $user_id)->first();
            return $this->render();
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

}
