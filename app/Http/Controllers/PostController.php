<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Config;
use App\Models\Post;
use App\Utility\CommonMethod;

class PostController extends UtilityController {

    public function __construct() {
        parent:: __construct();
        $this->middleware('validatejson', ['except' => ['index']]);
        $this->middleware('apiauth', ['except' => ['']]);
        $request = \Request::instance();
        $this->postData = json_decode($request->getContent(), true);
    }

    public function postGetPost() {
        try {
            $validation_rules = array(
                'companyId' => 'required|exists:company,id',
                'pageNo' => 'required|min:1',
                'flag' => 'required',
                'subordinateId' => 'required'
            );
            $validator = Validator::make($this->postData, $validation_rules);
            if ($validator->fails()) {
                return $this->validationError($validator);
            } else {
                $response = Post::getPost($this->postData);
                $this->result = $response['result'];
                $this->message = $response['message'];
            }
            return $this->render();
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function store() {
        try {
            $validation_rules = array(
                'content' => 'required',
                'teamId' => 'required|exists:team,id',
                'flag' => 'required',
                'companyId' => 'required|exists:company,id',
                'departmentId' => 'required|exists:department,id'
            );
            $validator = Validator::make($this->postData, $validation_rules);
            if ($validator->fails()) {
                return $this->validationError($validator);
            } else {
                $response = Post::createPost($this->postData);
                if ($response == 'false') {
                    return $this->renderFailure(trans('messages.user_not_subordinate'), Config::get('codes.unauthorized'));
                }
            }
            return $this->render();
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function postDeletePost() {
        try {
            $validation_rules = array('postId' => 'required|exists:post,id');
            $validator = Validator::make($this->postData, $validation_rules);
            if ($validator->fails()) {
                return $this->validationError($validator);
            } else {
                $response = Post::deletePost($this->postData);
                if ($response == 'false') {
                    return $this->renderFailure(trans('messages.user_not_manager'), Config::get('codes.unauthorized'));
                }
            }
            return $this->render();
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

}
