<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use App\Models\Department;
use Illuminate\Support\Facades\Session;
use App\Utility\CommonMethod;

class DepartmentController extends UtilityController {

    public function __construct() {
        parent:: __construct();
        $this->middleware('validatejson', ['except' => ['index']]);
        $this->middleware('apiauth', ['except' => ['index']]);
        $this->middleware('xss',['except' => ['postGetDepartment']]);
        $request = \Request::instance();
        $this->postData = json_decode($request->getContent(), true);
    }

    public function index() {
        try {
            $companyId = Session::get('companyId');
            $this->result = Department::select('id', 'department_name', 'company_id')->where('company_id', $companyId)->get();
            return $this->render();
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

    public function postGetDepartment() {
        try {
            $validation_rules = array(
                'companyId' => 'required|exists:company,id'
            );
            $validator = Validator::make($this->postData, $validation_rules);
            if ($validator->fails()) {
                return $this->validationError($validator);
            } else {
                $this->result = Department::where('company_id', $this->postData['companyId'])->get();
                return $this->render();
            }
        } catch (\Exception $e) {
            $msg = CommonMethod::logingException($e);
            return $this->exceptionCatch($msg);
        }
    }

}
