<?php

namespace App\Utility;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Aws\Credentials\CredentialProvider;
use Aws\S3\S3Client;;
use Aws\S3\Exception\S3Exception;
use Illuminate\Support\Facades\Redirect;

class CommonMethod {


    //logging the error in laravel log file
    public static function logException($method, $file, $line, $message) {

        Log::error(['method' => $method, 'error' => ['file' => $file, 'line' => $line, 'message' => $message], 'created_at' => date("Y-m-d H:i:s")]);
    }
    
    public static function downloadS3File($destination_path, $srcFileName, $destFileName, $contentType, $type){         
        $folderName = env('AWS_IMAGE_STORAGE_PATH');
        if($type == 'upload'){
            $folderName = env('AWS_FILE_UPLOAD_STORAGE_PATH');
        } else if($type == 'profile'){
            $folderName = env('AWS_PROFILE_STORAGE_PATH');
        }
        header("Expires: 0");
        header("Content-Description: File Transfer");
        header("Content-Type:".$contentType);
        header("Content-Disposition:attachment;filename=".$destFileName);
        header("Content-Transfer-Encoding: binary");
        echo file_get_contents($destination_path.$srcFileName);
    }

    //This function will be used to download file(CSV).
    function getDownload($srcFileName, $destFileName, $contentType) {       
        $storageType = env('STORAGETYPE');
        $destination_path = self::getFolderName($storageType, 'upload', 'read');
        if($storageType === 'AWS'){
            self::downloadS3File($destination_path, $srcFileName, $destFileName, $contentType, 'upload');
        } else {
            set_time_limit(0);
            $fullPath = $destination_path . $srcFileName;  // change the path to fit your websites document structure
            if (($fd = fopen($fullPath, "r")) !== FALSE) {
                $fsize = filesize($fullPath);
                $path_parts = pathinfo($fullPath);
                $ext = strtolower($path_parts["extension"]);
                ob_end_clean();
                if ($ext === 'csv' || $ext === 'zip') {
                    header("Expires: 0");
                    header("Content-Description: File Transfer");
                    header("Content-type: " . $contentType); // add here more headers for diff. extensions
                    header("Content-Disposition: attachment; filename=\"" . $destFileName . "\""); // use 'attachment' to force a download
                    header("Content-Transfer-Encoding: binary");
                } else {
                    header("Expires: 0");
                    header("Content-Description: File Transfer");
                    header("Content-Type: application/force-download");
                    header("Content-Disposition: attachment; filename=\"" . $destFileName . "\""); // use 'attachment' to force a download
                    header("Content-Transfer-Encoding: binary");
                }
                header("Content-length: $fsize");
                header("Pragma: ");
                header("Cache-Control: ");
                while (!feof($fd)) {
                    $buffer = fread($fd, 2048);
                    echo $buffer;
                }
            }
            fclose($fd);
        }
    }

    public function getImageFullPath($imagefolder, $imagename) {
        if ($imagename != '') {
            return env('SERVER_ROOT') . $imagefolder . $imagename;
        }
        return '';
    }
    
    public function fileProperty($file){
        $returnFile['fileName'] = $file->getClientOriginalName();
        $returnFile['fiteType'] = $file->getClientMimeType();
        $returnFile['fileTmpName'] = $file->getRealPath();
        $returnFile['error'] = $file->getError();
        $returnFile['size'] = $file->getClientSize();      
        return $returnFile;
    }
    
    public function upload($file, $uploadType){
        $extension = $file->getClientOriginalExtension();
        $fileName = $file->getClientOriginalName();
        $name = pathinfo($fileName, PATHINFO_FILENAME); // getting image extension
        $newnametmp = str_replace(" ", "_", $name . '_' . date('YmdHis') . '_' . time()); // renameing image
        $newname = $newnametmp . '.' . $extension; // renameing image
        $storageType = env('STORAGETYPE');
        $destination_path = $this->getFolderName($storageType, $uploadType);
        if($storageType === 'AWS'){
            $newfileName = $destination_path.$newname;            
            if(self::uploadImageToS3($file, $newfileName) !== true){
                $newname = '';
            }
        } else {
            $file->move($destination_path, $newname);
        }
        return $newname;
    }

    public function ajaxUpload($file, $uploadType) {
        $returnFile = $this->fileProperty($file);
        $fileName = $returnFile['fileName'];
        $newname = $this->upload($file, $uploadType);
        $fileArr['name'][] = $fileName;
        $fileArr['type'][] = $returnFile['fiteType'];
        $fileArr['tmp_name'][] = $returnFile['fileTmpName'];
        $fileArr['error'][] = $returnFile['error'];
        $fileArr['size'][] = $returnFile['size'];
        $fileArr['newname'][0] = $newname;
        echo json_encode($fileArr);
        exit;
    }
    
    public function postCSVFileUpload($fName, $ajaxfilenewname) {
        $uploadedFile = Input::file($fName);
        $fileArray = array('attachedFile' => $uploadedFile);
        $rules = array(
          'attachedFile' => 'required|mimes:csv,txt|max:5120'
        );
        $validator = Validator::make($fileArray, $rules);

        if ($validator->fails()){
            return Redirect::back()->withErrors($validator->errors()->toArray());
        } else {
            
           if (Input::hasFile($fName)){
                $returnFile = $this->fileProperty($uploadedFile);
                $newFile[$fName]['name'] = $returnFile['fileName'];
                $newFile[$fName]['type'] = $returnFile['fiteType'];
                $newFile[$fName]['tmp_name'] = $returnFile['fileTmpName'];
                $newFile[$fName]['error'] = $returnFile['error'];
                $newFile[$fName]['size'] = $returnFile['size'];
                $newFile[$fName]['newname'] = $ajaxfilenewname;
                return $newFile;
           }
        }
    }

    public static function getMicroDateTime() {
        $t = microtime(true);
        $micro = sprintf("%06d", ($t - floor($t)) * 1000000);
        return date('Y-m-d H:i:s.' . $micro, $t);
    }
    
    public static function getFolderName($storageType, $type, $mode = 'write') {
        if($storageType === 'AWS'){
            if($mode == 'write'){
                $folderName = env('AWS_IMAGE_STORAGE_PATH');
                if($type == 'upload'){
                    $folderName = env('AWS_FILE_UPLOAD_STORAGE_PATH');
                } else if($type == 'profile'){
                    $folderName = env('AWS_PROFILE_STORAGE_PATH');
                }
            } else {                
                $bucketUrl = env('AWS_URL').env('AWS_BUCKET_NAME').'/';
                $folderName = $bucketUrl.env('AWS_IMAGE_STORAGE_PATH');
                if($type == 'upload'){
                    $folderName = $bucketUrl.env('AWS_FILE_UPLOAD_STORAGE_PATH');
                } else if($type == 'profile'){
                    $folderName = $bucketUrl.env('AWS_PROFILE_STORAGE_PATH');
                }
            }
        } else {
            $folderName = env('IMAGE_LOCAL_STORAGE_PATH');
            if($type == 'upload'){
                $folderName = env('LOCAL_STORAGE_PATH');
            } else if($type == 'profile'){
                $folderName = env('USER_IMAGE_PATH');
            }
        }
        return $folderName;
    }
    
    public static function getAliasFolderName($storageType, $type){        
        if($storageType === 'AWS'){
            $bucketUrl = env('AWS_URL').env('AWS_BUCKET_NAME').'/';
            $folderName = $bucketUrl.env('AWS_IMAGE_STORAGE_PATH');
            if($type == 'upload'){
                $folderName = $bucketUrl.env('AWS_FILE_UPLOAD_STORAGE_PATH');
            } else if($type == 'profile'){
                $folderName = $bucketUrl.env('AWS_PROFILE_STORAGE_PATH');
            }
        } else {
            $folderName = env('APP_IMAGE');
            if($type == 'upload'){
                $folderName = env('FILE_UPLOAD');
            } else if($type == 'profile'){
                $folderName = env('PROFILE_IMAGE');
            }
        }
        return $folderName;
    }
    
    public static function get($fileName, $type) {
        $fullFilePath = '';
        if ($fileName) {
            return self::getAliasFolderName(env('STORAGETYPE'), $type).$fileName;
        }
        return $fullFilePath;
    }
    
    public static function getAliasPath($type){
        $storageType = env('STORAGETYPE');
        if($storageType === 'AWS'){
            $folderName = env('AWS_IMAGE_STORAGE_PATH');
            if($type == 'upload'){
                $folderName = env('AWS_FILE_UPLOAD_STORAGE_PATH');
            } else if($type == 'profile'){
                $folderName = env('AWS_PROFILE_STORAGE_PATH');
            }
        } else {
            $folderName = env('APP_IMAGE');
            if($type == 'upload'){
                $folderName = env('FILE_UPLOAD');
            } else if($type == 'profile'){
                $folderName = env('PROFILE_IMAGE');
            }
        }
        return $folderName;
    }
    
    public function checkFileExist($fileName, $type) {
        $storageType = env('STORAGETYPE');
        $fullFilePath = $this->getFolderName($storageType, $type).$fileName;
        if($storageType === 'AWS'){
            $client = new S3Client([
                'key' => env('AWS_ACCESS_KEY_ID'),
                'secret' => env('AWS_SECRET_ACCESS_KEY'),
                'region' => env('AWS_REGION'),
                'version' => env('AWS_VERSION')
            ]);
            return $client->doesObjectExist( env('AWS_BUCKET_NAME'), $fullFilePath );
        } else {
            return file_exists($fullFilePath);
        }
    }
    
    public static function uploadImageToS3($sourceFile, $newfileName) {
        try {
            $provider = CredentialProvider::env();
            $s3Client = new S3Client(array('version' => 'latest',
                'region' => env('AWS_REGION'),
                'credentials' => $provider));
            $data = array(
                'Bucket' => env('AWS_BUCKET_NAME'),
                'Key' => $newfileName,
                'ACL' => 'public-read',
                'SourceFile' => $sourceFile);
            $res = $s3Client->putObject($data);
            if (!empty($res['ObjectURL'])) {
                return true;
            }
            return false;
        } catch (\Exception $e) {
            Log::error(__METHOD__ . ' ' . $e->getMessage());
            return false;
        }
    }
    
    public static function logingException($e){
        Log::error(['method' => __METHOD__, 'error' => ['file' => $e->getFile(), 'line' => $e->getLine(), 'message' => $e->getMessage()], 'created_at' => date("Y-m-d H:i:s")]);
        return trans('messages.tech_error');
    }
    
    public static function createFile($data_error, $outputfile){
        try {
            $str = '';
            $storageType = env('STORAGETYPE');
            $destination_path = static::getFolderName($storageType, 'upload');
            if($storageType === 'AWS'){   
                foreach($data_error as $key=>$dataArr){
                    foreach($dataArr as $key2=>$dataArr2){
                        $str .= implode(';', $dataArr2)."\r\n"; 
                    }
                }
                $newfileName = $destination_path.$outputfile;
                $provider = CredentialProvider::env();
                $s3Client = new S3Client(array('version' => 'latest',
                    'region' => env('AWS_REGION'),
                    'credentials' => $provider));
                $data = array(
                    'Bucket' => env('AWS_BUCKET_NAME'),
                    'Key' => $newfileName,
                    'ACL' => 'public-read',
                    'Body'   => $str);
                $res = $s3Client->putObject($data);
                if (!empty($res['ObjectURL'])) {
                    return true;
                }
            } else {         
                
                $outputfilewithpath = $destination_path . $outputfile;
                $fp = fopen($outputfilewithpath, 'w+');
                foreach ($data_error as $rowdata) {
                    foreach ($rowdata as $dataarr) {
                        fputcsv($fp, $dataarr, ';');
                    }
                }
                
            }
            return false;
        } catch (\Exception $e) {
            Log::error(__METHOD__ . ' ' . $e->getMessage());
            return false;
        }
    }

}
