Laravel Dummy Projects includes the following setup:

1. Basic Laravel Authentication : login,user register, forgot Password ,logout
Just embed your design to get it ready for you

2. Datable Integration : Demo example of users list

3. Mapping library integarion for camelcase  representation

4. Basic Api for each project -  sign-in, forgot-password, change-password, logout with authentication middleware
5. Restfull Controllers  set up -  dummy routes available with controller methods 
6. Sofdelete Trait implemeneted
7. API authnetication with user token in the middleware
8. Laravel Cron setup - one dummy cron is ready to use
9. File Upload Repository with local storage and S3 storage
10. Logging all the input and outputs for debugging purpose
11. Push Notification Setup for IOS and Android



